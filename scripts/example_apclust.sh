#!/bin/bash 

# Example shell script performing affinity propagation clustering.
#
# References:
#
# Frey, Brendan J., and Delbert Dueck. "Clustering by passing messages between
#   data points." science 315, no. 5814 (2007): 972-976.
#
# Bodenhofer, Ulrich, Andreas Kothmeier, and Sepp Hochreiter. "APCluster: an R
#   package for affinity propagation clustering." Bioinformatics 27, no. 17
#   (2011): 2463-2464.
#
# https://en.wikipedia.org/wiki/Affinity_propagation
#
# http://scikit-learn.org/stable/modules/clustering.html
#
# http://www.psi.toronto.edu/affinitypropagation/faq.html
#
# Affinity propagation is a clustering algorithm that identifies the "natural"
# clusters within a data set. In other words, the user does not have to specify
# the target number of clusters as in k-means clustering. There is technically a
# way to specify more or less clusters using a q-value (for quantile similarity)
# in [0,1] where lower q-values imply less clusters and higher q-values imply
# more clusters (q=0 => 1 cluster, q=1 => 'N' clusters where N is number of
# inputs). However, currently, setting the q-value is not exposed to the user,
# though it would be trivial to add this option if desired. Instead, we default
# to a "no prior" q-value of 0.5, which imposes no prior beliefs on the number
# of found clusters.
#
# Required arguments to SCOBY app:
#   - Path to input binary matrix. Samples are assumed to be in each column
#       (with <n_dims>-columns per sample). For performance, it is recommended
#       that samples be PCA dimensionally reduced prior to running clustering.
#       See: <script_dir>/example_pca_reduce.sh.
#   - n_dims - The number of consecutive columns defining a single sample
#   - Path to output sample to cluster assignments. Each sample is assigned a
#       cluster identifier. The i-th line in file contains the cluster identifier
#       assigned to the i-th sample according to input column ordering.
#   - Path to output cluster to set of associated samples. Each row lists all
#       sample identifiers mapped to that cluster.
#   - Path to output exemplars. Each row is the exemplar sample identifier for
#       that cluster. Rows are in same order as 'Path to output cluster to set
#       of associated samples'. The exemplar sample is the sample that all
#       other samples within the same cluster are most similar to.
#
# Prerequisite script(s) assumed to have already been called/understood:
#   - <script_dir>/example_convert_matrix_csv_to_bin.sh
#
# See also:
#   - <script_dir>/example_shwap.sh

##############################################################################
# Determines paths to executable and example data sets

# Script directory
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Base project directory
PROJ_DIR="$SCRIPT_DIR/.."

# Directory containing example data sets
DATA_DIR="$PROJ_DIR/data"

# Path to executable binary
# Make sure to compile using cmake_init.sh and cmake_build.sh scripts
EXE_PATH="$PROJ_DIR/CMakeBuild/Release/scoby"

# Name of utility (app) to run
APP_NAME="apclust"

##############################################################################
# Arguments expected by app

# Path to input binary matrix. Samples are assumed to be in each column (with
#   <n_dims>-columns per sample). For performance, it is recommended that
#   samples be PCA dimensionally reduced prior to running clustering.
#   See: <script_dir>/example_pca_reduce.sh.
IN_BIN_MATRIX_PATH="$DATA_DIR/example_data2d.bin"

# The number of consecutive columns defining a single sample
N_DIMS="1"

# Path to write sample identifier to cluster identifier mapping in CSV format
OUT_S2C_PATH="$DATA_DIR/example_data2d.ap.s2c.csv"

# Path to write cluster identifier to associated set of sample identifiers in
# CSV format
OUT_C2S_PATH="$DATA_DIR/example_data2d.ap.c2s.csv"

# Path to write the sample identifiers which have been identified as the
# exemplar for the corresponding cluster. Each row is in the same cluster order
# as the cluster-to-samples (c2s) CSV file.
OUT_EX_PATH="$DATA_DIR/example_data2d.ap.ex.csv"

##############################################################################
# Run app

# Build commandline
cmd="$EXE_PATH $APP_NAME $IN_BIN_MATRIX_PATH $N_DIMS"
cmd="$cmd $OUT_S2C_PATH $OUT_C2S_PATH $OUT_EX_PATH"

echo "Executing:"
echo $cmd
echo "-------------------"

# Run commandline
$cmd

echo "-------------------"
echo "Finished."
