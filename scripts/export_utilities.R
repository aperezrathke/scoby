###################################################################
# Export utilities
###################################################################

# Load an existing rdata file
# @rdata_path - path on disk to rdata file, data is assumed to
# have been saved by 'save_rdata'
# @return loaded data
load_rdata <- function(rdata_path) {
  # Loads into variable named 'data'
  print(paste0("Loading ", rdata_path))
  load(file = rdata_path)
  return(data)
}

# Save to an rdata file - data saved by this method can be
# loaded using load_rdata(<path>)
# @param data - the data to save to rdata archive on disk
# @param rdata_path - path to write rdata archive
# @param b_create_subdirs - TRUE if subdirectories should be created
save_rdata <- function(data, rdata_path, b_create_subdirs = TRUE) {
  print(paste0("Saving ", rdata_path))
  d = dirname(rdata_path)
  if (b_create_subdirs && !dir.exists(d)) {
    dir.create(d, recursive = TRUE)
  }
  save(data, file = rdata_path)
}

# @return file name without path information or last .csv extension
get_fid_from_csv <- function(csv_path) {
  return(gsub("\\.csv$", "", basename(csv_path)))
}

# @return file name without path information or last .rdata extension
get_fid_from_rdata <- function(rdata_path) {
  return(gsub("\\.rdata$", "", basename(rdata_path)))
}

# Determines binary RDATA path from input CSV path
# @return rdata path
get_rdata_path_from_csv <- function(csv_path) {
  return(gsub("\\.csv$", ".rdata", csv_path))
}

# Determines ASCII CSV path from RDATA path
# @return csv path
get_csv_path_from_rdata <- function(rdata_path) {
  return(gsub("\\.rdata$", ".csv", rdata_path))
}

# @return modified file path of form:
#   <directory>/<basename of file>.<attrib>.rdata
insert_attrib_in_rdata <- function(rdata_path, attrib) {
  d = dirname(rdata_path)
  fid = get_fid_from_rdata(rdata_path)
  return(file.path(d, paste0(fid, ".", attrib, ".rdata")))
}

# Determines derived from rdata path for file with new extension
get_ext_path_from_rdata <- function(rdata_path, new_ext_with_dot) {
  return(gsub("\\.rdata$", new_ext_with_dot, rdata_path))
}

