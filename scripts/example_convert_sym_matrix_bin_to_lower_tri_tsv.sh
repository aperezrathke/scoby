#!/bin/bash 

# Example shell script for converting a symmetric (memory-efficient) binary
#   formatted matrix into a human-readable, plain-text, tab-separated (tsv)
#   format. The app will export the lower triangle, the upper triangle will
#   be written as all 0's.
#
# Required arguments to SCOBY app:
#   - Input path to read symmetric, binary-formatted matrix
#   - Output path to write square matrix in plain-text, tab-separated format
#
# Additional related script(s):
#   - <script_dir>/example_convert_sym_matrix_lower_tri_tsv_to_bin.sh

##############################################################################
# Determines paths to executable and example data sets

# Script directory
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Base project directory
PROJ_DIR="$SCRIPT_DIR/.."

# Directory containing example data sets
DATA_DIR="$PROJ_DIR/data"

# Path to executable binary
# Make sure to compile using cmake_init.sh and cmake_build.sh scripts
EXE_PATH="$PROJ_DIR/CMakeBuild/Release/scoby"

# Name of utility (app) to run
APP_NAME="convert_sym_matrix_bin_to_lower_tri_tsv"

##############################################################################
# Arguments expected by app

# Output path to write symmetric, binary-formatted matrix
IN_SYMMETRIC_BIN_MATRIX_PATH="$DATA_DIR/example_data2d.pdist.lowertri.sym.bin"

# Input path to square matrix in tab-separated format
# Only lower triangle (including diagonal) is stored
OUT_SQUARE_TSV_MATRIX_PATH="$DATA_DIR/example_data2d.pdist.lowertri.2.tsv"

##############################################################################
# Run app

# Build commandline
cmd="$EXE_PATH $APP_NAME $IN_SYMMETRIC_BIN_MATRIX_PATH $OUT_SQUARE_TSV_MATRIX_PATH"

echo "Executing:"
echo $cmd
echo "-------------------"

# Run commandline
$cmd

echo "-------------------"
echo "Finished."
