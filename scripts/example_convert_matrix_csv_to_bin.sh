#!/bin/bash 

# Example shell script for converting a matrix in CSV format to a binary,
#   non-symmetric matrix format. This binary format is used for storing
#   non-symmetric matrices that represent sample position data where each
#   sample is assumed to occupy a fixed number of consecutive columns
#   (typically 1). A sample is assumed to occupy all rows in its corresponding
#   columns.
#
# This fast matrix can serve as input to many other SCOBY apps:
#
#   - convert_matrix_bin_to_csv (convert binary back to CSV)
#   - crop (remove tail rows and columns)
#   - pca_reduce (dimensionality reduction)
#   - compute_rmsd | compute_rmsd_mt (pairwise RMSD)
#   - k_means_silh (k-means clustering using silhouettes)
#   - apclust (affinity propagation clustering)
#   - shwap (online affinity propagation)
#
# Required arguments to SCOBY app:
#   - Input path to plain-text, comma-separated, column-major matrix
#   - Output path of resulting binary matrix
#   - "1" if CSV input has header row, else "0"
#
# # Additional related script(s):
#   - <script_dir>/example_convert_matrix_bin_to_csv.sh
#   - <script_dir>/example_crop.sh
#   - <script_dir>/example_pca_reduce.sh
#   - <script_dir>/example_compute_rmsd.sh
#   - <script_dir>/example_compute_rmsd_mt.sh
#   - <script_dir>/example_k_means_silh.sh
#   - <script_dir>/example_apclust.sh
#   - <script_dir>/example_shwap.sh

##############################################################################
# Determines paths to executable and example data sets

# Script directory
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Base project directory
PROJ_DIR="$SCRIPT_DIR/.."

# Directory containing example data sets
DATA_DIR="$PROJ_DIR/data"

# Path to executable binary
# Make sure to compile using cmake_init.sh and cmake_build.sh scripts
EXE_PATH="$PROJ_DIR/CMakeBuild/Release/scoby"

# Name of utility (app) to run
APP_NAME="convert_matrix_csv_to_bin"

##############################################################################
# Arguments expected by app

# Input path to plain-text, comma-separated, column-major matrix
IN_CSV_MATRIX_PATH="$DATA_DIR/example_data2d.csv"

# Output path of resulting "fast" binary matrix
OUT_BIN_MATRIX_PATH="$DATA_DIR/example_data2d.bin"

# 1 -> CSV has header row, 0 -> CSV has no header row
IN_HAS_HEADER_ROW="0"

##############################################################################
# Run app

# Build commandline
cmd="$EXE_PATH $APP_NAME $IN_CSV_MATRIX_PATH $OUT_BIN_MATRIX_PATH $IN_HAS_HEADER_ROW"

echo "Executing:"
echo $cmd
echo "-------------------"

# Run commandline
$cmd

echo "-------------------"
echo "Finished."
