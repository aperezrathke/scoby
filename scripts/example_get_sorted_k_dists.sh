#!/bin/bash 

# Example shell script for DBSCAN parameter estimation of 'Epsilon' parameter.
#   See pseudocode here: https://en.wikipedia.org/wiki/DBSCAN
#   (variable may also be named 'eps')
#
# Epsilon is basically the distance threshold for adding points to a cluster.
#   This definition is not technically correct but it provides a naive
#   intuition. For the proper definition, see DBSCAN paper:
#
# Ester, Martin, et al. "A density-based algorithm for discovering clusters in
#   large spatial databases with noise." Kdd. Vol. 96. No. 34. 1996.
#
# According to Ester et al, a simple heuristic for estimating Epsilon when
#   MinPts = k (see DBSCAN paper for MinPts definition) is to plot the
#   k-distances and select Epsilon = index of elbow. The k-distance is defined
#   as the distance to the kth nearest neighbor. If we sort the points in
#   descending order by their associated k-distance values and then create a
#   plot where the y-axis is the k-disance and the x-axis the index of the 
#   x-th greatest k-distance, we will observe a graph with an elbow (see Ester
#   et al). The k-distance at the elbow junction is though to be a reasonably
#   good Epsilon value for when MinPts is set to k.
# 
# See $DATA_DIR/example_data2d.kdist4.png for example k-distances plot which
#   can be used for determining elbow index.
#
# Prerequisite script(s) assumed to have already been called/understood:
#   - <script_dir>/example_convert_sym_l_tsv_to_bin_matrix.sh
#
# Additional related script(s):
#   - <script_dir>/example_convert_sym_matrix_lower_tri_tsv_to_bin.sh
#   - <script_dir>/example_get_dbs_cluster_freq_counts.sh

##############################################################################
# Determines paths to executable and example data sets

# Script directory
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Base project directory
PROJ_DIR="$SCRIPT_DIR/.."

# Directory containing example data sets
DATA_DIR="$PROJ_DIR/data"

# Path to executable binary
# Make sure to compile using cmake_init.sh and cmake_build.sh scripts
EXE_PATH="$PROJ_DIR/CMakeBuild/Release/scoby"

# Name of utility (app) to run
APP_NAME="get_sorted_k_dists"

##############################################################################
# Arguments expected by app

# Input path to symmetric, binary-formatted matrix of pairwise distances
IN_SYMMETRIC_BIN_MATRIX_PATH="$DATA_DIR/example_data2d.pdist.lowertri.sym.bin"

# Input 'k' value for computing k-distances. The k-distance is defined as the
# distance to the kth nearest neighbor. If we sort the points in descending
# order by their associated k-distance values and then create a plot where the
# y-axis is the k-distance and the x-axis is the index of the xth greatest
# k-distance, we will observe a graph with an elbow (see Ester et al). The
# k-distance at the elbow junction is thought to be a reasonably good Epsilon
# value for when MinPts is set to k.
IN_K="4"

# Output path to write newline-separated k-distances vector
OUT_KDISTS_VECTOR_CSV_PATH="$DATA_DIR/example_data2d.kdist"$IN_K".csv"

##############################################################################
# Run app

# Build commandline
cmd="$EXE_PATH $APP_NAME $IN_SYMMETRIC_BIN_MATRIX_PATH $IN_K $OUT_KDISTS_VECTOR_CSV_PATH"

echo "Executing:"
echo $cmd
echo "-------------------"

# Run commandline
$cmd

echo "-------------------"
echo "Finished."
