#!/bin/bash 

# Example shell script for DBSCAN clustering of example data set.
#   See pseudocode here: https://en.wikipedia.org/wiki/DBSCAN
#
# See original paper:
#
# Ester, Martin, et al. "A density-based algorithm for discovering clusters in
#   large spatial databases with noise." Kdd. Vol. 96. No. 34. 1996.
#
# Required arguments to SCOBY app:
#   - Input path to a symmetric, pairwise distance matrix in binary format
#       (see example_convert_sym_matrix_lower_tri_tsv_to_bin.sh)
#   - Epsilon is the distance threshold (see description below)
#   - MinPts is the minimum number of points within distance epsilon for a
#       point to be considered a core point (see description below)
#   - Output path to clusters file
#   
# The canonical DBSCAN algorithm has 2 main parameters: Epsilon and MinPts
#
#   From Wikipedia:
#
#       Consider a set of points in some space to be clustered. For the
#       purpose of DBSCAN clustering, the points are classified as core
#       points, (density-)reachable points and outliers, as follows:
#           - A point is a core point if at least MinPts points are within
#               distance Epsilon of it, and those points are said to be
#               directly reachable from p. No points are reachable from a
#               non-core point.
#           - A point q is reachable from p if there is a path p1, ..., pn
#               with p1 = p and pn = q, where each pi+1 is directly reachable
#               from pi (so all the points on the path must be core points,
#               with the possible exception of q).
#           - All points not reachable from any other point are outliers.
#
#       Now if p is a core point, then it forms a +cluster+ together with all
#       points (core or non-core) that are reachable from it.
#
# From Ester et al, section 4.2 (DBSCAN KDD paper):
#
#   MinPts = 4 is the recommended parameter setting for any set of points > 4
#
#   Epsilon may be estimated heuristically using an "elbow"-plot method. See
#      <script_dir>/example_get_sorted_k_dists_for_bin_matrix.sh 
#
# Output format:
#
#   The output file is simply a list of cluster identifiers for each point in
#       the input matrix. The first line is the cluster identifier for the
#       first point, the second line is the cluster identifier for the second
#       point, etc. NOTE: THERE ARE TWO RESERVED CLUSTER IDENTIFIERS:
#           - 0: This point is noise; it is not a core point or density
#                reachable.
#           - 1: This point is unlabeled and was not visited by algorithm. It
#                is an error to see this output.
#       Therefore, all *actual* cluster identifiers start at >= 2.
#
# Prerequisite script(s) assumed to have already been called/understood:
#   - <script_dir>/example_convert_sym_matrix_lower_tri_tsv_to_bin.sh
#   - [optional] <script_dir>/example_get_sorted_k_dists.sh
#
# Additional related script(s):
#   - <script_dir>/example_get_dbs_cluster_freq_counts.sh
#       This script provides summary statistics for the clusters found.
   
##############################################################################
# Determines paths to executable and example data sets

# Script directory
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Base project directory
PROJ_DIR="$SCRIPT_DIR/.."

# Directory containing example data sets
DATA_DIR="$PROJ_DIR/data"

# Path to executable binary
# Make sure to compile using cmake_init.sh and cmake_build.sh scripts
EXE_PATH="$PROJ_DIR/CMakeBuild/Release/scoby"

# Name of utility (app) to run
APP_NAME="dbscan"

##############################################################################
# Arguments expected by app

# Input path to symmetric, binary-formatted matrix of pairwise distances
IN_SYMMETRIC_BIN_MATRIX_PATH="$DATA_DIR/example_data2d.pdist.lowertri.sym.bin"

# Distance threshold for point association. This parameter may be estimated
# using an elbow junction plot. See:
#   <script_dir>/example_get_sorted_k_dists_for_bin_matrix.sh
# For an example elbow plot (based on example inputs), see also:
#   <data_dir>/example_data2d.kdist4.png
# Note: this value is actual standard deviation of each cluster
IN_EPSILON="0.316"

# Minimum number of points within distance Epsilon for a point to be a "core
# point". Recommended setting is 4 for probably most data sets.
IN_MIN_PTS="4"

# Output path to write plain-text cluster assignments
#   Note:
#       - cluster identifier 0 -> noise
#       - cluster identifier 1 -> unclassified (error)
#   All actual clusters have identifiers >= 2
OUT_DBSCAN_CLUSTERS_PATH="$DATA_DIR/example_data2d.dbscan.csv"

##############################################################################
# Run app

# Build commandline
cmd="$EXE_PATH $APP_NAME $IN_SYMMETRIC_BIN_MATRIX_PATH $IN_EPSILON $IN_MIN_PTS $OUT_DBSCAN_CLUSTERS_PATH"

echo "Executing:"
echo $cmd
echo "-------------------"

# Run commandline
$cmd

echo "-------------------"
echo "Finished."
