#!/bin/bash 
# *Nix shell script which creates debug and release build
# directories using CMake
# Takes the following optional arguments:
# -debugdir: Path to debug build directory
# -releasedir: Path to release build directory
# -useintel: toggle usage of intel compiler

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
ROOT_DIR=$SCRIPT_DIR/..
DEBUG_DIR=$ROOT_DIR/CMakeBuild/Debug
RELEASE_DIR=$ROOT_DIR/CMakeBuild/Release
USE_INTEL=0

# Function to perform an out-of-source cmake build
function doOutOfSourceBuild {
    BUILD_DIR=$1
    BUILD_TYPE=$2
    USE_INTEL_COMPILER=$3
    echo Generating out-of-source $BUILD_TYPE build at $BUILD_DIR using $ROOT_DIR
    # Clear out any previous build directory
    rm -r $BUILD_DIR
    # Navigate to build directory
    mkdir -p $BUILD_DIR
    pushd .
    cd $BUILD_DIR
    if [ $USE_INTEL_COMPILER -eq 1 ]; then
        echo Using intel compiler
        cmake -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCMAKE_CXX_COMPILER=icpc -DCMAKE_C_COMPILER=icc $ROOT_DIR
    else
        echo Using default compiler
        cmake -DCMAKE_BUILD_TYPE=$BUILD_TYPE $ROOT_DIR
    fi
    popd
}

# Check if there are any parameter overrides
while [ $# -gt 0 ]
do
    case "$1" in
    -debugdir)
        DEBUG_DIR=$2
        shift
        ;;
    -releasedir)
        RELEASE_DIR=$2
        shift
        ;;
    -useintel)
        USE_INTEL=1
        ;;
    esac
    shift
done

# Create debug build
doOutOfSourceBuild $DEBUG_DIR Debug $USE_INTEL

# Create release build
doOutOfSourceBuild $RELEASE_DIR Release $USE_INTEL
