#!/bin/bash 

# Example shell script performing streaming, hierarchical, weighted affinity
#   propagation (SHWAP) clustering. Based on StrAP algorithm of Zhang et al.
#
# References:
#
# Zhang, Xiangliang, Cyril Furtlehner, and Michele Sebag. "Data streaming with
#   affinity propagation." Machine learning and knowledge discovery in
#   databases (2008): 628-643.
#
# Zhang, Xiangliang, Cyril Furtlehner, Cecile Germain-Renaud, and Michele
#   Sebag. "Data stream clustering with affinity propagation." IEEE
#   Transactions on Knowledge and Data Engineering 26, no. 7 (2014): 1644-1656.
#
# Affinity propagation is a clustering algorithm that identifies the "natural"
# clusters within a data set. In other words, the user does not have to specify
# the target number of clusters as in k-means clustering.
#
# However, the algorithm is O(N^2) in memory where N is number of samples to
# be clustered. In part to address this memory barrier, a streaming variant
# known as the 'StrAP' algorithm has been developed by Zhang et al which
# applies affinity propagation to an initial set of data points and then
# attempts to associate new data points to the existing clusters based on
# their similarity to the existing cluster exemplars. Only once enough samples
# have failed to be associated to existing clusters is a new weighted affinity
# propagation pass performed.
#
# Required arguments to SCOBY app:
#   - Path to input binary matrix. Samples are assumed to be in each column
#       (with <n_dims>-columns per sample). For performance, it is recommended
#       that samples be PCA dimensionally reduced prior to running clustering.
#       See: <script_dir>/example_pca_reduce.sh.
#   - n_dims - The number of consecutive columns defining a single sample
#   - Path to output sample to cluster assignments. Each sample is assigned a
#       cluster identifier. The i-th line in file contains the cluster identifier
#       assigned to the i-th sample according to input column ordering.
#   - Path to output cluster to set of associated samples. Each row lists all
#       sample identifiers mapped to that cluster.
#   - Path to output exemplars. Each row is the exemplar sample identifier for
#       that cluster. Rows are in same order as 'Path to output cluster to set
#       of associated samples'. The exemplar sample is the sample that all
#       other samples within the same cluster are most similar to.
#
# Optional arguments to SCOBY app (Note, if an argument is specified, then all
#   preceding arguments must also be specified)
#   - init_size - (Integer) The first init_size elements are clustered using
#       affinity propagation to find the initial exemplar and cluster
#       assignments. The default init_size is 2000.
#   - ex_assoc_heur - The type of heuristic to use for attempting to associate
#       an unlabeled sample to an existing exemplar. This used by the StrAP
#       algorithm to avoid an affinity propagation pass if the sample is
#       'similar enough' an existing exemplar. To define 'similar enough', the
#       following heuristics are available:
#           '0' -> average distance to exemplar : Unlabeled samples may be
#               assigned to a cluster using the average distance of all
#               associated samples to their exemplar. An unlabeled sample is
#               added to a cluster with least distance from the exemplar such
#               that this distance is less than the average distance cutoff,
#               else it is added to a reservoir for future weighted affinity
#               propagation.
#           '1' -> (default) median average distance to exemplar : Similar to
#               'average distance to exemplar' heuristic, but uses median
#               distance to exemplar as association criteria rather than
#               average distance.
#   - ex_assoc_heur_scale - The association heuristic cutoff distance is
#       scaled by this factor. Values less than 1.0 make it harder to
#       associate an unlabeled sample to an exemplar. Values greater than 1.0
#       make it easier to associate an unlabeled sample to an exemplar. The
#       default value is 0.25.
#   - max_unlabeled_thresh - Integer threshold for triggering a weighted,
#       local affinity propagation on samples which failed to be associated to
#       an exemplar (i.e. reservoir size). Default value is 1500.
#   - max_exemplars_thresh - Integer threshold for triggering a weighted,
#       hierarchical affinity propagation run on only the exemplars. This is
#       to attempt to merge exemplars and reduce the total count in order for
#       the local affinity propagation runs to be performant. Effectively an
#       upper bound on the number of allowed clusters. Default value is 1250.
#   - decay_window - Each exemplar has an integer window length defined as
#       the number of samples that have been processed since the last time a
#       sample was associated to that exemplar. If the window length exceeds
#       the decay value, then the exemplar is removed. Furthermore, the
#       multiplicity weights associated to that exemplar are reduced as a
#       function of this decay length. Default value is 10000.
#   - q - Real value in [0,1] that specifies which quantile similarity to
#       assign as the prior cluster association preference. This essentially
#       is a knob which can be used to tune the number of found clusters.
#       Values of q that are close to 0 will lead to less clusters, values of
#       q that are close to 1 will lead to more clusters. Further, q=0 will
#       give a single cluster, and q=1 will give N clusters where N is number
#       of input samples. Default value of q is 0.5 and corresponds to a
#       'no-prior' preference on the number of obtained clusters. This value
#       may be manipulated internally by SHWAP if convergence problems are
#       encountered (see 'p_conv_decay_fact' and 'p_conv_max_attempts').
#   - maxits - (Integer) Maximum number of iterations allowed for each
#       affinity propagation run. Default value is 1000.
#   - convits - (Integer)  A single run of affinity propagation terminates if
#       the exemplars have not changed for convits iterations. Default is 100.
#   - lam - Real-valued affinity propagation damping factor in range [0.5, 1);
#       higher values correspond to heavy damping which may be needed if
#       oscillations occur (lower values may lead to faster affinity
#       propagation convergence). Default value is 0.9.
#   - nonoise - If '0', affinity propagation runs add a small amount of noise
#       to all similarity scores to prevent degenerate cases. If '1', this
#       step is avoided (not recommended!). Default value is '0'.
#   - p_conv_decay_fact - Real-valued scalar. If stock affinity propagation
#       fails to convergence, we can fiddle with the preferences in a
#       reasonable way such that the subsequent run will likely converge. This
#       scales the 'q-value' used for exemplar associations. Values in [0,1)
#       will lead to less clusters, values in (1,2) will lead to more
#       clusters. This heuristic is exclusive to this implementation and is
#       only used when an affinity propagation run fails to converge. Only
#       applies to non-initialization, weighted affinity propagation runs.
#       Default value is 1.05.
#   - p_conv_max_attempts - (Integer) The maximum number of times to fiddle
#       with the input preferences in attempt to get affinity propagation to
#       converge before giving up. Only applies to non-initialization,
#       weighted affinity propagation. Default value is 5.
#   - init_swiz_max_attempts - (Integer) The maximum number of times to
#       swizzle the input samples in an attempt to obtain an ensemble that
#       converges during the initial affinity propagation run. Default value
#       is 5.
#
# Prerequisite script(s) assumed to have already been called/understood:
#   - <script_dir>/example_convert_matrix_csv_to_bin.sh
#
# See also:
#   - <script_dir>/example_apclust.sh

##############################################################################
# Determines paths to executable and example data sets

# Script directory
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Base project directory
PROJ_DIR="$SCRIPT_DIR/.."

# Directory containing example data sets
DATA_DIR="$PROJ_DIR/data"

# Path to executable binary
# Make sure to compile using cmake_init.sh and cmake_build.sh scripts
EXE_PATH="$PROJ_DIR/CMakeBuild/Release/scoby"

# Name of utility (app) to run
APP_NAME="shwap"

##############################################################################
# Arguments expected by app (required)

# Path to input binary matrix. Samples are assumed to be in each column (with
#   <n_dims>-columns per sample). For performance, it is recommended that
#   samples be PCA dimensionally reduced prior to running clustering.
#   See: <script_dir>/example_pca_reduce.sh.
IN_BIN_MATRIX_PATH="$DATA_DIR/example_data2d.bin"

# The number of consecutive columns defining a single sample
N_DIMS="1"

# Path to write sample identifier to cluster identifier mapping in CSV format
OUT_S2C_PATH="$DATA_DIR/example_data2d.shwap.s2c.csv"

# Path to write cluster identifier to associated set of sample identifiers in
# CSV format
OUT_C2S_PATH="$DATA_DIR/example_data2d.shwap.c2s.csv"

# Path to write the sample identifiers which have been identified as the
# exemplar for the corresponding cluster. Each row is in the same cluster order
# as the cluster-to-samples (c2s) CSV file.
OUT_EX_PATH="$DATA_DIR/example_data2d.shwap.ex.csv"

##############################################################################
# Arguments expected by app (optional)

# (Integer) The first init_size elements are clustered using affinity
# propagation to find the initial exemplar and cluster assignments. Default
# value is 2000.
INIT_SIZE=100

# The type of heuristic to use for attempting to associate an unlabeled sample
# to an existing exemplar. This used by the StrAP algorithm to avoid an
# affinity propagation pass if the sample is 'similar enough' an existing
# exemplar. To define 'similar enough', the following heuristics are available:
#   '0' -> average distance to exemplar : Unlabeled samples may be assigned to
#          a cluster using the average distance of all associated samples to
#          their exemplar. An unlabeled sample is added to a cluster with
#          least distance from the exemplar such that this distance is less
#          than the average distance cutoff, else it is added to a reservoir
#          for future weighted affinity propagation.
#   '1' -> (default) median average distance to exemplar : Similar to 'average
#       distance to exemplar' heuristic, but uses median distance to exemplar
#       as association criteria rather than average distance.
EX_ASSOC_HEUR=1

# The association heuristic cutoff distance is scaled by this factor. Values
# less than 1.0 make it harder to associate an unlabeled sample to an
# exemplar. Values greater than 1.0 make it easier to associate an unlabeled
# sample to an exemplar. Default value is 0.25.
EX_ASSOC_HEUR_SCALE=0.25

# Integer threshold for triggering a weighted, local affinity propagation on
# samples which failed to be associated to an exemplar (i.e. reservoir size).
# Default value is 1500.
MAX_UNLABELED_THRESH=25

# Integer threshold for triggering a weighted, hierarchical affinity
# propagation run on only the exemplars. This is to attempt to merge exemplars
# and reduce the total count in order for the local affinity propagation runs
# to be performant. Effectively an upper bound on the number of allowed
# clusters. Default value is 1250.
MAX_EXEMPLARS_THRESH=200

# Each exemplar has an integer window length defined as the number of samples
# that have been processed since the last time a sample was associated to that
# exemplar. If the window length exceeds the decay value, then the exemplar is
# removed. Furthermore, the multiplicity weights associated to that exemplar
# are reduced as a function of this decay length. Default value is 10000.
DECAY_WINDOW=10000

# Real value in [0,1] that species which quantile similarity to assign as the
# prior cluster association preference. This essentially is a knob which can
# be used to tune the number of found clusters. Values of 'q' that are close
# to 0 will lead to less clusters, values of 'q' that are close to 1 will lead
# to more clusters. Further, q=0 will give a single cluster, and q=1 will give
# N clusters where N is number of input samples. Default value of q is 0.5 and
# corresponds to a 'no-prior' preference on the number of obtained clusters.
# This value may be manipulated internally by SHWAP if convergence problems
# are encountered (see 'p_conv_decay_fact' and 'p_conv_max_attempts').
Q=0.5

#(Integer) Maximum number of iterations allowed for each affinity propagation
# run. Default value is 1000.
MAXITS=1000

# (Integer)  A single run of affinity propagation terminates if the exemplars
# have not changed for convits iterations. Default is 100.
CONVITS=100

# Real-valued affinity propagation damping factor in range [0.5, 1); higher
# values correspond to heavy damping which may be needed if oscillations occur
# (lower values may lead to faster affinity propagation convergence). Default
# value is 0.9.
LAM=0.9

# If '0', affinity propagation runs add a small amount of noise to all
# similarity scores to prevent degenerate cases. If '1', this step is avoided
# (not recommended!). Default value is '0'.
NONOISE=0

# Real-valued scalar. If stock affinity propagation fails to convergence, we
# can fiddle with the preferences in a reasonable way such that the subsequent
# run will likely converge. This scales the 'q-value' used for exemplar
# associations. Values in [0,1) will lead to less clusters, values in (1,2)
# will lead to more clusters. This heuristic is exclusive to this
# implementation and is only used when an affinity propagation run fails to
# converge. Only applies to non-initialization, weighted affinity propagation
# runs. Default value is 1.05.
P_CONV_DECAY_FACT=1.05

# (Integer) The maximum number of times to fiddle with the input preferences
# in attempt to get affinity propagation to converge before giving up. Only
# applies to non-initialization, weighted affinity propagation. Default is 5.
P_CONV_MAX_ATTEMPTS=5

# (Integer) The maximum number of times to swizzle the input samples in an
# attempt to obtain an ensemble that converges during the initial affinity
# propagation run. Default value is 5.
INIT_SWIZ_MAX_ATTEMPTS=5

##############################################################################
# Run app

# Build commandline
cmd="$EXE_PATH $APP_NAME $IN_BIN_MATRIX_PATH $N_DIMS"
cmd="$cmd $OUT_S2C_PATH $OUT_C2S_PATH $OUT_EX_PATH"
cmd="$cmd $INIT_SIZE $EX_ASSOC_HEUR $EX_ASSOC_HEUR_SCALE"
cmd="$cmd $MAX_UNLABELED_THRESH $MAX_EXEMPLARS_THRESH"
cmd="$cmd $DECAY_WINDOW $Q $MAXITS $CONVITS $LAM $NONOISE"
cmd="$cmd $P_CONV_DECAY_FACT $P_CONV_MAX_ATTEMPTS $INIT_SWIZ_MAX_ATTEMPTS"

echo "Executing:"
echo $cmd
echo "-------------------"

# Run commandline
$cmd

echo "-------------------"
echo "Finished."
