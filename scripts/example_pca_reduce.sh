#!/bin/bash 

# Example shell script for performing PCA dimensionality reduction on a matrix
#   in binary format. The output matrix is also in binary matrix format and can
#   serve as input to additional SCOBY apps such as:
#
#   - convert_matrix_bin_to_csv (convert binary back to CSV)
#   - crop (remove tail rows and columns)
#   - pca_reduce (dimensionality reduction)
#   - compute_rmsd | compute_rmsd_mt (pairwise RMSD)
#   - k_means_silh (k-means clustering using silhouettes)
#   - apclust (affinity propagation clustering)
#   - shwap (online affinity propagation)

# NOTE: SAMPLE DIMENSIONS ARE ASSUMED TO BE IN SAME UNIT SCALE. Currently,
#   no scaling (e.g. by standard deviation) is performed.
#
# NOTE: OUTPUT MATRIX WILL ALWAYS BE IN COLUMN-MAJOR FORMAT WITH EACH SAMPLE
#   spanning a single column. The original formatting of the input matrix is
#   not preserved.
#
# For an overview of principal component analysis (PCA), see:
#   https://en.wikipedia.org/wiki/Principal_component_analysis
#
# The idea is to preserve as much information as possible from the original
#   dataset but in a lower dimensional space. The amount of information
#   preserved is controlled through the percent variance retained parameter.
#   The dimensionality reduction is distance preserving, therefore the
#   resulting data set may be used for more efficient clustering.
#
# Required arguments to SCOBY app:
#   - The percentage of variance to retain in the output data set. This is
#       based on heuristic: sum(eigenvalues 1 to k) / sum(eigenvalues 1 to n)
#       where k is smallest integer such that proportion is greater than or
#       equal to input variance retention percentage. The eigenvalues are of
#       the covariance matrix of the samples.
#   - A flag (1|0) indicating if samples are stored in in rows (row-major). If
#       "1", then samples are assumed to be in rows and they will be
#       transposed prior to computation. "0" indicates column major.
#   - The number of consecutive columns (or rows if row-major) each sample
#       occupies. Note: the final output matrix will be such that each sample
#       only occupies a single column.
#   - Path to input matrix binary
#   - Path to output PCA dimensionally reduced matrix binary. Note, this
#       matrix is column-major: each sample occupies a single column.
#
# Prerequisite script(s) assumed to have already been called/understood:
#   - <script_dir>/example_convert_matrix_csv_to_bin.8d.sh
#
# # Additional related script(s):
#   - <script_dir>/example_convert_matrix_bin_to_csv.sh
#   - <script_dir>/example_crop.sh
#   - <script_dir>/example_compute_rmsd.sh
#   - <script_dir>/example_compute_rmsd_mt.sh
#   - <script_dir>/example_k_means_silh.sh
#   - <script_dir>/example_apclust.sh
#   - <script_dir>/example_shwap.sh

##############################################################################
# Determines paths to executable and example data sets

# Script directory
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Base project directory
PROJ_DIR="$SCRIPT_DIR/.."

# Directory containing example data sets
DATA_DIR="$PROJ_DIR/data"

# Path to executable binary
# Make sure to compile using cmake_init.sh and cmake_build.sh scripts
EXE_PATH="$PROJ_DIR/CMakeBuild/Release/scoby"

# Name of utility (app) to run
APP_NAME="pca_reduce"

##############################################################################
# Arguments expected by app

# The percentage of variance among the samples to retain in [0,1]
IN_PERC_KEEP="0.50"

# If "1", input matrix is in row-major format with each sample occupying
#   consecutive rows. If "0", then samples are column-major and occupy
#   consecutive columns instead. Note: regardless of this flag, the output
#   matrix will always be column-major.
IN_OBS_IN_ROWS="0"

# The number of consecutive columns (if column-major) or rows (if row-major)
#   each sample occupies. Note: regardless of this flag, the output matrix
#   will be column-major with samples occupying only a single column.
IN_N_DIMS="1"

# Input path to fast, binary formatted matrix
IN_BIN_MATRIX_PATH="$DATA_DIR/example_data8d.bin"

# Output path to PCA dimensionally reduced fast matrix binary. Note, this
#   matrix is column-major: each sample occupies a single column.
OUT_PCA_BIN_MATRIX_PATH="$DATA_DIR/example_data8d.pca.bin"

##############################################################################
# Run app

# Build commandline
cmd="$EXE_PATH $APP_NAME $IN_PERC_KEEP $IN_OBS_IN_ROWS $IN_N_DIMS $IN_BIN_MATRIX_PATH $OUT_PCA_BIN_MATRIX_PATH"

echo "Executing:"
echo $cmd
echo "-------------------"

# Run commandline
$cmd

echo "-------------------"
echo "Finished."
