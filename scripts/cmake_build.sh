# *Nix shell script compiles creates debug and release build directories
# Takes the following optional arguments:
# -debugdir: Path to debug build directory
# -releasedir: Path to release build directory
# -builddir: Path to custom build directory (something other than debug|release)
# <target>: The name of the build target 

# Initialize to default paths
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
DEBUG_DIR=$SCRIPT_DIR/../CMakeBuild/Debug
RELEASE_DIR=$SCRIPT_DIR/../CMakeBuild/Release
# If build target is not 'all', 'debug', or 'release'
BUILD_DIR=
BUILD_TARGET=all

# Function to run make in parameter build directory
function doMake {
    MAKE_BUILD_DIR=$1
    echo Running make for target dir $MAKE_BUILD_DIR
    # Navigate to target build directory
    pushd .
    cd $MAKE_BUILD_DIR
    # Run make in target build directory
    make
    popd
}

# Check if there are any parameter overrides
while [ $# -gt 0 ]
do
    case "$1" in
    -debugdir)
        DEBUG_DIR=$2
        shift
        ;;
    -releasedir)
        RELEASE_DIR=$2
        shift
        ;;
    -builddir)
        BUILD_DIR=$2
        shift
        ;;
    *)
        BUILD_TARGET=$1
        ;;
    esac
    shift
done

# Determine build directories
if [ -n "$BUILD_DIR" ]
then
    echo Building custom target at $BUILD_DIR
    doMake $BUILD_DIR
elif [ "$BUILD_TARGET" == "all" ]
then
    echo Building all targets
    doMake $DEBUG_DIR
    doMake $RELEASE_DIR
elif [ "$BUILD_TARGET" == "debug" ]
then
    echo Building debug target at $DEBUG_DIR
    doMake $DEBUG_DIR
elif [ "$BUILD_TARGET" == "release" ]
then
    echo Building release target at $RELEASE_DIR
    doMake $RELASE_DIR
else
    echo No build found
fi

