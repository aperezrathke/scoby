#!/bin/bash 

# Example shell script for converting a square, tab-separated (tsv) matrix
#   into a symmetric (memory-efficient), binary-formatted matrix. The app
#   will only save the lower triangle (including diagonal) when coverting to
#   symmetric format.
#
# Required arguments to SCOBY app:
#   - Input path to square matrix in plain-text, tab-separated format
#   - Output path to write symmetric, binary-formatted matrix
#
# This matrix may now serve as input to any app utilizing a symmetric
#   (usually distance) matrix such as DBSCAN.
#
# Additional related script(s):
#   - <script_dir>/example_dbscan.sh
#   - <script_dir>/example_convert_sym_matrix_bin_to_lower_tri_tsv.sh

##############################################################################
# Determines paths to executable and example data sets

# Script directory
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Base project directory
PROJ_DIR="$SCRIPT_DIR/.."

# Directory containing example data sets
DATA_DIR="$PROJ_DIR/data"

# Path to executable binary
# Make sure to compile using cmake_init.sh and cmake_build.sh scripts
EXE_PATH="$PROJ_DIR/CMakeBuild/Release/scoby"

# Name of utility (app) to run
APP_NAME="convert_sym_matrix_lower_tri_tsv_to_bin"

##############################################################################
# Arguments expected by app

# Input path to square matrix in tab-separated format
# Only lower triangle (including diagonal) is stored
IN_SQUARE_TSV_MATRIX_PATH="$DATA_DIR/example_data2d.pdist.lowertri.tsv"

# Output path to write symmetric, binary-formatted matrix
OUT_SYMMETRIC_BIN_MATRIX_PATH="$DATA_DIR/example_data2d.pdist.lowertri.sym.bin"

##############################################################################
# Run app

# Build commandline
cmd="$EXE_PATH $APP_NAME $IN_SQUARE_TSV_MATRIX_PATH $OUT_SYMMETRIC_BIN_MATRIX_PATH"

echo "Executing:"
echo $cmd
echo "-------------------"

# Run commandline
$cmd

echo "-------------------"
echo "Finished."
