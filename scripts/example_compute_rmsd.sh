#!/bin/bash 

# Example shell script for computing pairwise root-mean-square distance (RMSD)
#
# See:
# https://en.wikipedia.org/wiki/Root-mean-square_deviation_of_atomic_positions
#
# Required arguments to SCOBY app:
#   - Input path to binary sample positions matrix. Samples are column-major
#   - Number of columns per sample. Each sample is assumed to occupy this many
#       consecutive columns. Typically this value is 1. Each sample is assumed
#       to occupy all rows for each of its columns.
#   - Output symmetric, binary matrix path or "NULL" must be input
#       (non-optional). This output matrix can serve as input to DBSCAN.
#   - [optional] Output CSV path for pairwise RMSD matrix (writes lower and
#       upper tris)
#
# Prerequisite script(s) assumed to have already been called/understood:
#   - <script_dir>/example_convert_matrix_csv_to_bin.sh
#
# Additional related script(s):
#   - <script_dir>/example_compute_rmsd_mt.sh
#   - <script_dir>/example_dbscan.sh
#       (The output RMSD binary matrix can serve as input to DBSCAN)

##############################################################################
# Determines paths to executable and example data sets

# Script directory
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Base project directory
PROJ_DIR="$SCRIPT_DIR/.."

# Directory containing example data sets
DATA_DIR="$PROJ_DIR/data"

# Path to executable binary
# Make sure to compile using cmake_init.sh and cmake_build.sh scripts
EXE_PATH="$PROJ_DIR/CMakeBuild/Release/scoby"

# Name of utility (app) to run
APP_NAME="compute_rmsd"

##############################################################################
# Arguments expected by app

# Input path to binary-formatted matrix of sample positions. Each column
#   defines a single sample. It is possible for a sample to span multiple
#   consecutive columns, but our example data is single column.
IN_BIN_MATRIX_PATH="$DATA_DIR/example_data2d.bin"

# The number of consecutive columns spanned by each sample. A sample is
# assumed to span all rows belonging to each of its columns.
IN_N_COLS="1"

# Output path for resulting symmetric (memory-efficient), binary matrix of
#   RMSD distances. This format can serve as input to DBSCAN. This path must
#   be specified or else "NULL" must be entered on commandline.
OUT_RMSD_SYMMETRIC_BIN_MATRIX_PATH="$DATA_DIR/example_data2d.rmsd.bin"

# [Optional] Output path for plain-text CSV version of pairwise RMSD matrix.
#   Both upper and lower triangles are output. "NULL" is also acceptable input
#   in which case this path will be ignored.
OUT_RMSD_CSV_MATRIX_PATH="$DATA_DIR/example_data2d.rmsd.csv"

##############################################################################
# Run app

# Build commandline
cmd="$EXE_PATH $APP_NAME $IN_BIN_MATRIX_PATH $IN_N_COLS $OUT_RMSD_SYMMETRIC_BIN_MATRIX_PATH $OUT_RMSD_CSV_MATRIX_PATH"

echo "Executing:"
echo $cmd
echo "-------------------"

# Run commandline
$cmd

echo "-------------------"
echo "Finished."
