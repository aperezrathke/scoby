#!/bin/bash 

# Example shell script for converting a matrix in binary format to a
#   non-symmetric, plain-text CSV matrix format.
#
# This CSV matrix is human-readable and can be opened in any text editor, R,
#   Matlab, python, spreadsheet software (eg. excel), etc.
#
# This matrix can serve as input to:
#   - convert_matrix_csv_to_bin (convert CSV to binary)
#
# Required arguments to SCOBY app:
#   - Input path to a full, binary matrix
#   - Output path to resulting plain-text, comma-separated matrix
#
# # Additional related script(s):
#   - <script_dir>/example_convert_matrix_bin_to_csv.sh

##############################################################################
# Determines paths to executable and example data sets

# Script directory
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Base project directory
PROJ_DIR="$SCRIPT_DIR/.."

# Directory containing example data sets
DATA_DIR="$PROJ_DIR/data"

# Path to executable binary
# Make sure to compile using cmake_init.sh and cmake_build.sh scripts
EXE_PATH="$PROJ_DIR/CMakeBuild/Release/scoby"

# Name of utility (app) to run
APP_NAME="convert_matrix_bin_to_csv"

##############################################################################
# Arguments expected by app

# Input path to a "fast" binary matrix
IN_BIN_MATRIX_PATH="$DATA_DIR/example_data2d.bin"

# Input path to plain-text, comma-separated matrix
OUT_CSV_MATRIX_PATH="$DATA_DIR/example_data2d.from_bin.csv"

##############################################################################
# Run app

# Build commandline
cmd="$EXE_PATH $APP_NAME $IN_BIN_MATRIX_PATH $OUT_CSV_MATRIX_PATH"

echo "Executing:"
echo $cmd
echo "-------------------"

# Run commandline
$cmd

echo "-------------------"
echo "Finished."
