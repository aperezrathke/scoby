#!/bin/bash 

# Example shell script for removing tail rows and/or columns from a matrix in
#   binary format. The resulting cropped matrix is still in binary format and
#   can serve as input to many other SCOBY apps such as:
#
#   - convert_matrix_bin_to_csv (convert binary back to CSV)
#   - crop (remove [even more] tail rows and columns)
#   - pca_reduce (dimensionality reduction)
#   - compute_rmsd | compute_rmsd_mt (pairwise RMSD)
#   - k_means_silh (k-means clustering using silhouettes)
#   - apclust (affinity propagation clustering)
#   - shwap (online affinity propagation)
#
# The utility of the crop app is to create a small subset
#   of the original data set for testing purposes. See
#   convert_matrx_bin_to_csv app (and example script) for how to convert
#   matrix binaries to plain-text CSV for human readable inspection.
#
# Required arguments to SCOBY app:
#   - The number of rows to keep starting from first row. If < 0, then all
#       rows will be kept, else all rows past this number are removed.
#   - The number of columns to keep starting from first column. If < 0, then
#       all columns will be kepts, else all columns past this number are
#       removed.
#   - Path to input matrix binary
#   - Path to output cropped matrix binary.
#
# Prerequisite script(s) assumed to have already been called/understood:
#   - <script_dir>/example_convert_matrix_csv_to_bin.sh
#
# # Additional related script(s):
#   - <script_dir>/example_convert_matrix_bin_to_csv.sh
#   - <script_dir>/example_pca_reduce.sh

##############################################################################
# Determines paths to executable and example data sets

# Script directory
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Base project directory
PROJ_DIR="$SCRIPT_DIR/.."

# Directory containing example data sets
DATA_DIR="$PROJ_DIR/data"

# Path to executable binary
# Make sure to compile using cmake_init.sh and cmake_build.sh scripts
EXE_PATH="$PROJ_DIR/CMakeBuild/Release/scoby"

# Name of utility (app) to run
APP_NAME="crop"

##############################################################################
# Arguments expected by app

# The first <IN_NUM_KEEP_ROWS> rows are retained in output matrix. If < 0,
#   then all rows are retained.
IN_NUM_KEEP_ROWS="-1"

# The first <IN_NUM_KEEP_COLS> columns are retained in output matrix. If < 0,
#   then all columns are retained.
IN_NUM_KEEP_COLS="128"

# Input path to fast, binary formatted matrix
IN_BIN_MATRIX_PATH="$DATA_DIR/example_data2d.bin"

# Output path of resulting "fast" binary matrix
OUT_CROPPED_BIN_MATRIX_PATH="$DATA_DIR/example_data2d.crop.bin"

##############################################################################
# Run app

# Build commandline
cmd="$EXE_PATH $APP_NAME $IN_NUM_KEEP_ROWS $IN_NUM_KEEP_COLS $IN_BIN_MATRIX_PATH $OUT_CROPPED_BIN_MATRIX_PATH"

echo "Executing:"
echo $cmd
echo "-------------------"

# Run commandline
$cmd

echo "-------------------"
echo "Finished."
