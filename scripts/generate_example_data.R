###################################################################
# generate_example_data.R
#
# Utilities for generating example data sets
###################################################################

###################################################################
# Libraries
###################################################################

# Needed for sampling from multivariate normal distribution
library(MASS)

###################################################################
# Common path utilities
###################################################################

# Cached directory to script
# https://stackoverflow.com/questions/3452086/getting-path-of-an-r-script
SCRIPT_DIR = getSrcDirectory(function(x) {
  x
})

# @return full path to directory containing this script
get_script_dir <- function() {
  return(SCRIPT_DIR)
}

# @return base directory of project
get_proj_dir <- function() {
  file.path(get_script_dir(), '..')
}

# @return path to store exported data
get_data_dir <- function() {
  file.path(get_proj_dir(), "data")
}

###################################################################
# External utilities
###################################################################

source(file.path(get_script_dir(), "export_utilities.R"))

###################################################################
# Globals
###################################################################

# Significant digits to keep
N_EXPORT_DIGITS = 3

###################################################################
# Create, visualize, export data
###################################################################

# Generates data set with <2^n_dim>*<n_samples_per_clust> total
#   samples where each sample has <n_dim> dimensions. Data set will
#   contain obvious clusters at each of corners defined by the
#   [-1,...,1]x[-1,...,1] hypercube. Note that there are 2^n_dim
#   corners in a hypercube embedded in n_dim dimensions. Order of
#   samples is scrambled.
#
# Example, for n_dim = 2, n_samples_per_clust = 64
#   - Output is a 2 x 256 matrix with each column as a sample.
#   - There will be 4 clusters.
#   - Each cluster has 64 samples
#   - The 4 clusters will be centered at [x,y] positions
#       [1,1], [-1,1], [-1,-1], and [1,-1] respectively
#
# @return <n_dim> x <n_dim*n_dim>*<n_samples_per_clust> matrix
#   where each column is a <n_dim>-dimensional sample
generate_samples <- function(n_dim = 2,
                             n_samples_per_clust = 64) {
  # Cap at 8 dimensions!
  if (n_dim > 8) {
    print("Too many dimensions!")
    return()
  }
  
  # Fix seed to make data generation deterministic
  set.seed(7331)
  
  # Determine number of clusters = number of corners of hypercube
  n_clust = 2^n_dim
  
  # Create covariance matrix
  Sigma = matrix(data = 0,
                 nrow = n_dim,
                 ncol = n_dim)
  for (i in 1:n_dim) {
    Sigma[i, i] = 0.1
  }
  
  # Initial means
  mu = rep(0, n_dim)
  
  # Map cluster index to hypercube corner
  get_mu <- function(i_clust) {
    i_clust_zero = i_clust - 1
    # http://stackoverflow.com/questions/12088080/how-to-convert-integer-number-into-binary-vector
    bits = intToBits(i_clust_zero)
    is_zero_bit = (bits[1:n_dim] == 00)
    mu[is_zero_bit] = -1
    mu[!is_zero_bit] = 1
    return(mu)
  }
  
  # Sample from multi-variate normal
  n_samples = n_clust * n_samples_per_clust
  samples = matrix(0, nrow = n_dim, ncol = n_samples)
  for (i in 1:n_clust) {
    mu = get_mu(i)
    print(paste0("Hypercube corner ", i, ": "))
    print(mu)
    beg = ((i - 1) * n_samples_per_clust) + 1
    end = beg + n_samples_per_clust - 1
    samples[, beg:end] = t(mvrnorm(n = n_samples_per_clust, mu = mu, Sigma = Sigma))
  }
  
  # Round each row
  for (i in 1:n_dim) {
    samples[i, ] = round(x = samples[i, ], digits = N_EXPORT_DIGITS)
  }
  
  # Scrample samples
  perm = sample(x = 1:n_samples,
                size = n_samples,
                replace = FALSE)
  samples = samples[, perm]
  
  # Restore randomness
  set.seed(Sys.time())
  
  return(samples)
}

# Export <n_dim>-dimensional samples
# @param n_dim - number of dimensions per sample
# @param pdist_lower_tri = logical, if TRUE, will export lower
#   triangle of tab-separated pairwise euclidean distance matrix
export_samples <- function(n_dim = 2,
                           pdist_lower_tri = FALSE) {
  samples = generate_samples(n_dim = n_dim)
  rdata_path = file.path(get_data_dir(), paste0("example_data", n_dim, "d.rdata"))
  csv_path = get_csv_path_from_rdata(rdata_path = rdata_path)
  save_rdata(data = samples, rdata_path = rdata_path)
  
  # Writes unquoted table data with no row or column names
  # (write.csv appears to force column names)
  export_vanilla_table <- function(x, path, sep = ",", ...) {
    print(paste0("Saving ", path))
    # http://stackoverflow.com/questions/14260646/how-to-control-number-of-decimal-digits-in-write-table-output
    write.table(
      x = format(
        x,
        trim = TRUE,
        digits = N_EXPORT_DIGITS,
        justify = "none"
      ),
      file = path,
      quote = FALSE,
      sep = sep,
      row.names = FALSE,
      col.names = FALSE,
      ...
    )
  }
  
  export_vanilla_table(x = samples, path = csv_path, sep = ",")
  
  # Should we also export pairwise distances?
  if (pdist_lower_tri) {
    pdist = as.matrix(dist(x = t(samples)))
    pdist[upper.tri(pdist)] = 0
    rdata_pdist_path = insert_attrib_in_rdata(rdata_path = rdata_path,
                                              attrib = "pdist.lowertri")
    tsv_pdist_path = get_ext_path_from_rdata(rdata_path = rdata_pdist_path,
                                             new_ext_with_dot = ".tsv")
    save_rdata(data = pdist, rdata_pdist_path)
    export_vanilla_table(x = pdist, path = tsv_pdist_path, sep = "\t")
  }
}

# Visualize 2-dimensional clusters
scatter_plot_2d_samples <- function() {
  samples = generate_samples(n_dim = 2)
  x = samples[1, ]
  y = samples[2, ]
  plot(x = x,
       y = y,
       main = paste0("n = ", ncol(samples)))
}
