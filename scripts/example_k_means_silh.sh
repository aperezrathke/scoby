#!/bin/bash 

# Example shell script performing k-means clustering with silhouette scoring.
#   The app will attempt a user-defined range [k_min, k_max] of k-values. The
#   clustering with the highest average silhouette score will be exported.
#
# NOTE: k must be >= 2, else silhouette scoring will divide-by-zero
#
# References:
#
#   https://en.wikipedia.org/wiki/K-means_clustering
#
#   Rousseeuw, Peter J. "Silhouettes: a graphical aid to the interpretation
#   and validation of cluster analysis." Journal of computational and applied
#   mathematics 20 (1987): 53-65.
#
# K-means clustering attempts to iteratively partition all samples into 'k'
#   disjoint sets sets such that each sample is closer to the sample mean of
#   its set than to the sample mean of any other set.
#
# Silhouette scoring is a criterion for judging the quality of the resulting
#   clusters. From wikipedia: "[It] is a measure of how similar an object is
#   its own cluser (cohesion) compared to other clusters (separation)." The
#   score is computed for every sample and will always be in range [-1, 1]
#   with higher scores considered better quality. The overall quality of the
#   clustering is reported as the average of all individual scores. One can
#   use silhouette scoring to decide the value of 'k' - the number of clusters
#   as the "better" clustering will have the highest average silhouette score.
#
# Required arguments to SCOBY app:
#   - min_k: The minimum integer k-cluster value to attempt (>= 2)
#   - max_k: The maximum integer k-cluster value to attempt (>= min_k)
#   - k_inc: The interval between subsequent k-cluster attempts. App will
#       try succesive k-cluster values (must be integer):
#           max_k, max_k - k_inc, max_k - 2 * k_inc, ... min_k
#   - k_report_interv: The interval for reporting status of current top
#       cluster found (e.g. heartbeat) - must be integer
#   - n_rand_restarts - K-means finds local minima sensitive to initial
#       starting centroids. To alleviate this, for a given k, the algorithm
#       can be rerun from different random initial conditions. The best
#       clustering will be kept (must be integer).
#   - n_dims - The number of consecutive columns defining a single sample
#   - delta - The real-valued stop threshold. For a single k-value run, if all
#       centroids change by less than or equal to this amount, then algorithm
#       stops and proceeds to next k-value or next random restart.
#   - max_iters_per_run: The maximum number of iterations for a single k-value
#       run. If clustering still has not reached delta threshold by this
#       number of iterations, the algorithm will stop, capture results, and
#       proceed to next k-value or next random restart (must be integer > 0).
#   - Path to input binary matrix. Samples are assumed to be in each column
#       (with <n_dims>-columns per sample). For performance, it is recommended
#       that samples be PCA dimensionally reduced prior to running clustering.
#       See: <script_dir>/example_pca_reduce.sh.
#   - Path to output cluster assignments. Each sample is assigned a cluster
#       identifier. The i-th line in file contains the cluster identifier assigned
#       to the i-th sample according to input column ordering.
#
# Optional arguments to SCOBY app (note, SCOBY does not use command switches
#   and therefore arguments are mapped by their location on the command line.
#   Therefore, all optional arguments must have any preceeding optional
#   arguments on the commandline. If a preceeding optional argument is not
#   desired, simply enter "null" or "NULL".
#   - [optional] - Output path to write centroids (csv format)
#   - [optional] - Output path of counts for each cluster (csv format)
#   - [optional] - Output path of silhouette scores for each sample in final
#                   clustering (csv format)
#   - [optional] - Output path of average silhouette score at each attempted
#                   k-value (csv format)
#
# Prerequisite script(s) assumed to have already been called/understood:
#   - <script_dir>/example_convert_matrix_csv_to_bin.sh

##############################################################################
# Determines paths to executable and example data sets

# Script directory
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Base project directory
PROJ_DIR="$SCRIPT_DIR/.."

# Directory containing example data sets
DATA_DIR="$PROJ_DIR/data"

# Path to executable binary
# Make sure to compile using cmake_init.sh and cmake_build.sh scripts
EXE_PATH="$PROJ_DIR/CMakeBuild/Release/scoby"

# Name of utility (app) to run
APP_NAME="k_means_silh"

##############################################################################
# Arguments expected by app

# The minimum integer k-cluster value to attempt (>= 2)
MIN_K="2"

# The maximum integer k-cluster value to attempt (>= min_k)
MAX_K="10"

# The interval between subsequent k-cluster attempts. App will try succesive
#   k_cluster values (must be integer):
#       max_k, max_k - k_inc, max_k - 2 * k_inc, ... min_k
K_INC="1"

# The interval for reporting status of current top cluster found (e.g.
#   heartbeat) - must be integer
K_REPORT_INTERV="1"

# K-means finds local minima sensitive to initial starting centroids. To
#   alleviate this, for a given k, the algorithm can be rerun from different
#   random initial conditions. The best clustering will be kept (must be
#   integer).
N_RAND_RESTARTS="2"

# The number of consecutive columns defining a single sample
N_DIMS="1"

# The real-valued stop threshold. For a single k-value run, if all centroids
#   change by less than or equal to this amount, then algorithm stops and
#   proceeds to next k-value or next random restart.
DELTA="0.0001"

#   - max_iters_per_run: The maximum number of iterations for a single k-value
#       run. If clustering still has not reached delta threshold by this
#       number of iterations, the algorithm will stop, capture results, and
#       proceed to next k-value or next random restart (must be integer).
MAX_ITERS_PER_RUN="10000"

# Path to input binary matrix. Samples are assumed to be in each column (with
#   <n_dims>-columns per sample). For performance, it is recommended that
#   samples be PCA dimensionally reduced prior to running clustering.
#   See: <script_dir>/example_pca_reduce.sh.
IN_BIN_MATRIX_PATH="$DATA_DIR/example_data2d.bin"

# Path to output cluster assignments. Each sample is assigned a cluster
#   identifier. The i-th line in file contains the cluster identifier assigned
#   to the i-th sample according to input column ordering.
OUT_CLUSTERS_PATH="$DATA_DIR/example_data2d.kmeans_silh.s2c.csv"

# Optional arguments to SCOBY app (note, SCOBY does not use command switches
#   and therefore arguments are mapped by their location on the command line.
#   Therefore, all optional arguments must have any preceeding optional
#   arguments on the commandline. If a preceeding optional argument is not
#   desired, simply enter "null" or "NULL".

# [optional] Output path to write centroids (csv format)
OUT_CENTROIDS_PATH="$DATA_DIR/example_data2d.kmeans_silh.centroids.csv"

# [optional] Output path of counts for each cluster (csv format)
OUT_CLUSTER_COUNTS_PATH="$DATA/example_data2d.kmeans_silh.counts.csv"

# [optional] Output path of silhouette scores for each sample in final
#   clustering (csv format)
OUT_SILH_SCORES_PATH="$DATA_DIR/example_data2d.kmeans_silh.scores.csv"

# [optional] Output path of average silhouette score at each attempted k-value
#   (csv format)
OUT_SILH_K_SCORES_PATH="$DATA_DIR/example_data2d.kmeans_silh.k_scores.csv"

##############################################################################
# Run app

# Build commandline
cmd="$EXE_PATH $APP_NAME $MIN_K $MAX_K $K_INC $K_REPORT_INTERV $N_RAND_RESTARTS"
cmd="$cmd $N_DIMS $DELTA $MAX_ITERS_PER_RUN $IN_BIN_MATRIX_PATH"
cmd="$cmd $OUT_CLUSTERS_PATH $OUT_CENTROIDS_PATH $OUT_CLUSTER_COUNTS_PATH"
cmd="$cmd $OUT_SILH_SCORES_PATH $OUT_SILH_K_SCORES_PATH"

echo "Executing:"
echo $cmd
echo "-------------------"

# Run commandline
$cmd

echo "-------------------"
echo "Finished."
