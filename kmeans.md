C++ implementation of k-means clustering with silhouette scoring

# K-means Clustering

This is tailored for atomic/protein structures and therefore uses [RMSD](https://en.wikipedia.org/wiki/Root-mean-square_deviation_of_atomic_positions) as a distance metric.
The distance metric may be configurable in the future, but for now RMSD is the only option available.

The number of clusters is selected using the [silhouette method](https://en.wikipedia.org/wiki/Silhouette_&#40;clustering&#41;).

## Input format

### Atom coordinates

The format expected by the k-means algorithm is a column-major list of x, y, z atom positions

For *n* protein structures with *p* atoms each, each column represents a single dimension of a protein structure.

first row:

```
structure_1.atom_1_x, structure_1.atom_1_y, structure_1.atom_1_z, ... , structure_n.atom_1_x, structure_n.atom_1_y, structure_n.atom_1_z
```
...

last row:

```
structure_1.atom_p_x, structure_1.atom_p_y, structure_1.atom_p_z, ... , structure_n.atom_p_x, structure_n.atom_p_y, structure_n.atom_p_z
```

The application also expects this format in binary and not plain-text csv.

However, the application provides a utility to convert a csv formatted list into the binary format expected by k-means.

To run the converter:

```
./scoby convert_matrix_csv_to_bin $InCsvPath $OutBinPath $HasHeaderRow
```

See also [<scripts>/example_convert_matrix_csv_to_bin.sh](scripts/example_convert_matrix_csv_to_bin.sh)

Where:

* `$InCsvPath` - Path to input csv matrix in format described in Atom coordinates section
* `$OutBinPath` - Destination file name to write output binary matrix which can be read by k-means utility
* `$HasHeaderRow` - Must be either 'true' or 'false' string. Set to true if input csv file's 1st row is simply column labels to be ignored.

### RMSD

In addition, the k-means application expects a symmetric binary matrix containing the RMSD between all pairs of structures.
Again, a utility is provided to compute and export this matrix from the Atom coordinates file.

To run the RMSD computer:

```
./scoby compute_rmsd $InBinPath $NumCols $OutBinPath [$OutCsvPath]
```

See also [<scripts>/example_compute_rmsd.sh](scripts/example_compute_rmsd.sh)

Where:

* `$InBinPath` - Path to input binary matrix in format described in Atom coordinates section (see `convert_matrix_csv_to_bin` to convert from plain-text)
* `$NumDim` - The integer number of consecutive columns spanned by each sample.
* `$OutBinPath` - Path to write the binary symmetric matrix for use in k-means clustering
* `$OutCsvPath` - Optional path to write plain text RMSD output file

## Clustering

For an example of k-means clustering with documentation on parameters, please refer to:

[<scripts>/example_k_means_silh.sh](scripts/example_k_means_silh.sh)
