Simple C++ implementation of DBSCAN clustering algorithm.

Based on pseudocode found [here](http://en.wikipedia.org/wiki/DBSCAN).

# Workflow

## Suggested Workflow

1. Compile source code
2. Convert plain text matrix to binary format
3. Perform parameter estimation to determine _Epsilon_ and _MinPts_ (see Utilities section)
4. Cluster data points
5. Analyze cluster output (see Utilities section)

## Compiling

### Unix

In Unix, navigate to the *scripts* folder and run

```
./cmake_init.sh
```

followed by

```
./cmake_buid.sh.
```

The scoby executable binary will be located in the CMakeBuild folder.

### Windows

Open the solution (.sln) in Visual Studio 2017 (or later) and then press *ctrl+shift+b* to compile.

## Generating the binary matrix format

The dbscan implementation currently only works with a binary distance matrix format.

To convert a plain-text, tab separated, lower-symmetric distance matrix to the binary format expected by dbscan, run the following command:

```
./scoby convert_sym_matrix_lower_tri_tsv_to_bin $InTsvPath $OutBinPath
```

This will convert the plain-text, tab-separated matrix located at *$InTsvPath* to a binary matrix which will be output to *$OutBinPath*

See also [scripts/example_convert_sym_matrix_lower_tri_tsv_to_bin.sh](scripts/example_convert_sym_matrix_lower_tri_tsv_to_bin.sh)

## Clustering

### Parameters

The canonical DBSCAN algorithm has 2 main parameters: _Epsilon_ and _MinPts_

From Wikipedia:

Consider a set of points in some space to be clustered. For the purpose of DBSCAN clustering, the points are classified as core points, (density-)reachable points and outliers, as follows:

* A point is a core point if at least _MinPts_ points are within distance _Epsilon_ of it, and those points are said to be directly reachable from p. No points are reachable from a non-core point.
* A point q is reachable from p if there is a path p1, ..., pn with p1 = p and pn = q, where each pi+1 is directly reachable from pi (so all the points on the path must be core points, with the possible exception of q).
* All points not reachable from any other point are outliers.

Now if p is a core point, then it forms a +cluster+ together with all points (core or non-core) that are reachable from it.

From Ester et al, section 4.2 (DBSCAN KDD paper):

* _MinPts_ = 4 is the recommended parameter setting for any set of points > 4

### Running

Assuming an existing binary matrix, the dbscan algorithm may be executed with the following command line:

```
./scoby dbscan $InBinPath $OutClustersPath $Epsilon $MinPts
```


Where:

* `$InBinPath` is the path to the input binary matrix
* `$OutClustersPath` is the path to the output clusters file
* `$Epsilon` is the distance threshold
* `$MinPts` is the minimum number of points with distance epsilon for a point to be considered a core point

See also [scripts/example_dbscan.sh](scripts/example_dbscan.sh)

### Output format

The output file is simply a list of cluster identifiers for each point in the input matrix. The first line is the cluster identifier for the first point, the second line is the cluster identifier for the second point, etc.

## Utilities

### Parameter Estimation

A heuristic for determining reasonable values for _Epsilon_ and _MinPts_ is described in [Ester et al](http://www.it.uu.se/edu/course/homepage/infoutv/ht11/literature_material/KDD-96.final.frame.pdf), section 4.2:

To do so, one computes the k-distance for each point in the set. The k-distance is defined as the distance to the kth nearest neighbor. If we sort the points in descending order by their associated k-distance values and then create a plot where the y-axis is the k-distance and the x-axis is the index of the xth greatest k-distance, we will observe a graph with an elbow (see Ester et al). The k-distance at the elbow junction is thought to be a reasonably good _Epsilon_ value for when _MinPts_ is set to k.

DBSCANPP contains a utility to generate a csv of the x and y values necessary for creating this elbow plot. This csv file can then be opened in any spreadsheet software, R or Matlab and plotted to visualize which k-distance corresponds to the elbow junction.

To run this utility, enter the following command line:

```
./scoby get_sorted_k_dists $InBinPath $K $OutKDistsCsvPath
```

Where:

* `$InBinPath` is the path to the input binary matrix
* `$K` determines the kth neighbor to compute distance for
* `$OutKDistsCsvPath` is the path to the output csv file of sorted k-distances


See also [scripts/example_get_sorted_k_dists.sh](scripts/example_get_sorted_k_dists.sh)

### Cluster Analysis

We are likely interested in a few summary statistics for each output cluster. Scoby includes a utility which will output a csv file where each row has the following format:

* Column 1: The cluster identifier
* Column 2: The number of points belonging to this cluster
* Column 3: The proportion of total points belonging to this cluster
* Columns 4 through n: A variable number of columns where each column is the identifier (i.e - index) of the points belonging this cluster

To run this utility, enter the following command line:

```
./scoby get_dbs_cluster_freq_counts $InClustersPath $OutFreqCountsCsvPath $bAsc
```

Where:

* `$InClustersPath` is the path to the clusters file generated from running dbscan
* `$OutFreqCountsCsvPath` is the path to the output csv file contained statistics for each cluster
* `$bAsc` if 1, then output is sorted ascending by cluster size; if 0, output will be in descending order

See also [scripts/example_get_dbs_cluster_freq_counts.sh](scripts/example_get_dbs_cluster_freq_counts.sh)
