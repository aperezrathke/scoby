**SCOBY** is a set of C++ clustering and data manipulation utilities originally developed for clustering protein data sets. However, these utilities can work with any data set for which a distance metric is available.

If you wish to use these utilities in publication, please cite:

* Perez-Rathke, Alan\*, Monifa A. Fahie\*, Christina Chisholm, Jie Liang, and Min Chen. "Mechanism of OmpG pH-dependent gating from loop ensemble and single channel studies." *Journal of the American Chemical Society* 140, no. 3 (2018): 1105-1115. doi: [10.1021/jacs.7b11979](https://doi.org/10.1021/jacs.7b11979) *\*Authors contributed equally*

## Features

* Affinity propagation clustering
* Streaming, hierarchical, weighted affinity propagation (SHWAP) clustering based on StrAP algorithm
* K-means clustering with silhouette scoring
* DBSCAN clustering
* Compute root-mean-squared distance (RMSD) between samples
* Principal component analysis (PCA) dimensionality reduction

## Compiling

### Linux

In linux, navigate to the *scripts* folder and run

```
./cmake_init.sh
```

followed by

```
./cmake_build.sh.
```

The scoby executable binary will be located in the CMakeBuild folder.

### Windows

Open the solution (.sln) in Visual Studio 2017 (or later) and then press *ctrl+shift+b* to compile.

## Usage

For bash scripts demonstrating usage and detailed parameter descriptions, please refer to following examples in *scripts* folder:

* Affinity propagation clustering: [example_apclust.sh](scripts/example_apclust.sh)
* SHWAP clustering based on StrAP algorithm: [example_shwap.sh](scripts/example_shwap.sh)
* K-means clustering with silhouette scoring: [example_k_means_silh.sh](scripts/example_k_means_silh.sh), see also [kmeans.md](kmeans.md)
* DBSCAN clustering: [example_dbscan.sh](scripts/example_dbscan.sh), see also [dbscan.md](dbscan.md)
* PCA dimensionality reduction: [example_pca_reduce.sh](scripts/example_pca_reduce.sh)

There are also additional example scripts for:

* [cropping data](scripts/example_crop.sh)
* [converting from CSV to binary matrix format](scripts/example_convert_matrix_csv_to_bin.sh)
* [converting from binary to CSV matrix format](scripts/example_convert_matrix_bin_to_csv.sh)
* [converting symmetric tsv matrix (lower tri) to binary](scripts/example_convert_sym_matrix_lower_tri_tsv_to_bin.sh)
* [converting symmetric binary matrix to tsv matrix (lower tri)](scripts/example_convert_sym_matrix_bin_to_lower_tri_tsv.sh)
* [single-threaded RMSD](scripts/example_compute_rmsd.sh)
* [multi-threaded RMSD](scripts/example_compute_rmsd_mt.sh)

## Additional references

For affinity propagation:

* Frey, Brendan J., and Delbert Dueck. "Clustering by passing messages between data points." science 315, no. 5814 (2007): 972-976.
* Bodenhofer, Ulrich, Andreas Kothmeier, and Sepp Hochreiter. "APCluster: an R package for affinity propagation clustering." Bioinformatics 27, no. 17 (2011): 2463-2464.
* [Affinity propagation FAQ](http://www.psi.toronto.edu/affinitypropagation/faq.html)

For streaming affinity propagation using the StrAP algorithm:

* Zhang, Xiangliang, Cyril Furtlehner, and Michele Sebag. "Data streaming with affinity propagation." Machine learning and knowledge discovery in databases (2008): 628-643.
* Zhang, Xiangliang, Cyril Furtlehner, Cecile Germain-Renaud, and Michele Sebag. "Data stream clustering with affinity propagation." IEEE Transactions on Knowledge and Data Engineering 26, no. 7 (2014): 1644-1656.

For DBSCAN:

* Ester, Martin, et al. "A density-based algorithm for discovering clusters in large spatial databases with noise." Kdd. Vol. 96. No. 34. 1996.
* [DBSCAN wikipedia](https://en.wikipedia.org/wiki/DBSCAN)

For k-means silhouette scoring:

* Rousseeuw, Peter J. "Silhouettes: a graphical aid to the interpretation and validation of cluster analysis." Journal of computational and applied mathematics 20 (1987): 53-65.

For more on kombucha and SCOBY: 

* [kombucha](https://en.wikipedia.org/wiki/Kombucha)
* [SCOBY](https://en.wikipedia.org/wiki/SCOBY)

