/**
 * @author Alan Perez-Rathke
 * @date July 25, 2014
 */

#include "scbybuild.h"
#include "scbysymmatrix_io.h"

#include <fstream>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/lexical_cast.hpp>

// needed for hack serialization
#include <stdio.h>

namespace {
    // reads lower tri by loading as full matrix first
    int scby_sym_read_lower_tri(
          scby_sym_matrix &outm
        , const char *path
        , const scby_matrix_utils::file_type ft
    ) {
        scby_assert(NULL != path);

        // read in a full matrix
        scby_matrix tmp;
        int result = scby_read_matrix(
              tmp
            , false /*has_header_row*/
            , path
            , ft
            ) ? 1 : 0;

        // convert full matrix to a symmetric matrix
        if (result) {
            if (tmp.n_rows == tmp.n_cols)
            {
                typedef scby_sym_matrix::size_type sz_t;
                const sz_t sz = static_cast<sz_t>(tmp.n_rows);
                // not sure why this has to be manually assigned
                outm.resize(sz, sz);
                for (sz_t irow = 0; irow < sz; ++irow) {
                    for (sz_t icol = 0; icol <= irow; ++icol) {
                        outm(irow, icol) = tmp(
                              as_scby_mat_sz_t(irow)
                            , as_scby_mat_sz_t(icol)
                            );
                    }
                }
            }
            else {
                // matrix is not square!
                result = 0;
            }
        }

        return result;
    }

    // writes text representation of lower, symmetric matrix to disk
    // @param m - reference to scby_sym_l_matrix that will be written to plain text
    // @param path - character string with path to output file
    // @param delim - string delimiter between all entries (e.g. ',' or '\t')
    // note: all columns above diagonal are written as 0's
    void scby_sym_matrix_write_lower_tri_delim(
          const scby_sym_matrix &m
        , const char *path
        , const char *delim
        )
    {
        scby_assert(path);
        scby_assert(delim);
        std::ofstream out;
        out.open(path);

        const size_t n_rows = m.size1();
        scby_assert(m.size1() == m.size2());

        // early out if matrix is empty
        if (n_rows < 1) {
            goto cleanup;
        }

        // write delimited
        for (size_t i = 0; i < n_rows; ++i)
        {
            // write first element
            out << m(i, 0);

            for (size_t j = 1; j < n_rows; ++j)
            {
                out << delim;

                if (j > i)
                {
                    out << "0.0";
                }
                else
                {
                    out << m(i, j);
                }
            }

            out << std::endl;
        }

    cleanup:
        out.close();
    }

    // writes text representation of symmetric matrix to disk (both upper and lower tri)
    // @param m - reference to scby_sym_matrix that will be written to plain text
    // @param path - character string with path to output file
    // @param delim - string delimiter between all entries (e.g. ',' or '\t')
    void scby_sym_matrix_write_delim(
          const scby_sym_matrix &m
        , const char *path
        , const char *delim
        )
    {
        std::ofstream out;
        out.open(path);

        const size_t n_rows = m.size1();
        scby_assert(m.size1() == m.size2());

        // early out if matrix is empty
        if (n_rows < 1) {
            goto cleanup;
        }

        // write text delimited matrix
        for (size_t i = 0; i < n_rows; ++i)
        {
            // write first element
            out << m(i, 0);

            for (size_t j = 1; j < n_rows; ++j)
            {
                out << delim << m(i, j);
            }

            out << std::endl;
        }

    cleanup:
        out.close();
    }
} // end of helper namespace

// reads a lower, symmetric matrix in tsv format from disk
// @param out reference to scby_sym_matrix that will be read from file
// @param path - character string with path to file
// @return 1 if matrix successfully loaded into 'out', 0 otherwise
int scby_sym_matrix_read_lower_tri_tsv(scby_sym_matrix &outm, const char *path) {
    return scby_sym_read_lower_tri(outm, path, scby_matrix_utils::raw_ascii);
}

// writes tsv representation of lower, symmetric matrix to disk
// @param m - reference to scby_sym_l_matrix that will be written to plain text
// @param path - character string with path to output file
// note: all columns above diagonal are written as 0's
void scby_sym_matrix_write_lower_tri_tsv(const scby_sym_matrix &m, const char *path) {
    scby_sym_matrix_write_lower_tri_delim(m, path, "\t");
}

// writes tsv representation of symmetric matrix to disk (both upper and lower tri)
// @param m - reference to scby_sym_matrix that will be written to plain text
// @param path character string with path to output file
void scby_sym_matrix_write_tsv(const scby_sym_matrix &m, const char *path) {
    scby_sym_matrix_write_delim(m, path, "\t");
}

// reads a lower, symmetric matrix in csv format from disk
// @param out reference to scby_sym_matrix that will be read from file
// @param path - character string with path to file
// @return 1 if matrix successfully loaded into 'out', 0 otherwise
int scby_sym_matrix_read_lower_tri_csv(scby_sym_matrix &outm, const char *path) {
    return scby_sym_read_lower_tri(outm, path, scby_matrix_utils::csv_ascii);
}

// writes csv representation of lower, symmetric matrix to disk
// @param m - reference to scby_sym_l_matrix that will be written to plain text
// @param path character string with path to output file
// note: all columns above diagonal are written as 0's
void scby_sym_matrix_write_lower_tri_csv(const scby_sym_matrix &m, const char *path) {
    scby_sym_matrix_write_lower_tri_delim(m, path, ",");
}

// writes csv representation of symmetric matrix to disk (both upper and lower tri)
// @param m - reference to scby_sym_matrix that will be written to plain text
// @param path - character string with path to output file
void scby_sym_matrix_write_csv(const scby_sym_matrix &m, const char *path) {
    scby_sym_matrix_write_delim(m, path, ",");
}

// writes binary representation of symmetric matrix to disk
// HACK - casts first element to scbyReal * (b/c can't compile boost serialization lib on visual studio 2013)
void scby_sym_matrix_write_bin(const scby_sym_matrix &m, const char *path) {
    scby_assert(NULL != path);
    const scby_sym_matrix::value_type* d_buffer = &m(0, 0);
    const scby_sym_matrix::size_type sz_buffer[2] = { m.size1(), m.size2() };
    FILE * pFile;
    pFile = fopen(path, "wb");
    scby_verify(fwrite(sz_buffer,
                      sizeof(scby_sym_matrix::size_type),
                      2, // count
                      pFile) == 2);
    scby_assert(sz_buffer[0] == sz_buffer[1]);
    const scby_sym_matrix::size_type num_elems = ((sz_buffer[0] * sz_buffer[0]) + sz_buffer[0]) / 2;
    scby_verify(fwrite(d_buffer,
                      sizeof(scby_sym_matrix::value_type),
                      num_elems,
                      pFile) == num_elems);
    fclose(pFile);
}

// reads binary representation of symmetric matrix from disk
// HACK - casts first element to scbyReal * (b/c can't compile boost serialization lib on visual studio 2013)
void scby_sym_matrix_read_bin(scby_sym_matrix &out_m, const char *path) {
    scby_assert(NULL != path);
    scby_sym_matrix::size_type sz_buffer[2] = { 0, 0 };
    FILE * pFile;
    pFile = fopen(path, "rb");
    scby_verify(fread(sz_buffer,
                     sizeof(scby_sym_matrix::size_type),
                     2, // count
                     pFile) == 2);
    out_m.resize(sz_buffer[0], sz_buffer[1], false /*preserve*/);
    scby_assert(sz_buffer[0] == sz_buffer[1]);
    const scby_sym_matrix::size_type num_elems = ((sz_buffer[0] * sz_buffer[0]) + sz_buffer[0]) / 2;
    scby_verify(fread(&(out_m(0, 0)),
                     sizeof(scby_sym_matrix::value_type),
                     num_elems, // count
                     pFile) == num_elems);
    fclose(pFile);
}

