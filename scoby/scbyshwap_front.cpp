/**
 * Front-facing utilities for streaming, hierarchical, weighted affinity propagation
 *
 * Hierarchical, weighted affinity propagation - a C++ port of the Matlab StrAP algorithm.
 * Uses reservoir size and exemplar count thresholds as criterion for re-running affinity propagation.
 *
 * Zhang, Xiangliang, Cyril Furtlehner, and Michele Sebag. "Data streaming with affinity propagation."
 * In Machine Learning and Knowledge Discovery in Databases, pp. 628-643. Springer Berlin Heidelberg, 2008.
 *
 * Zhang, Xiangliang, Cyril Furtlehner, Cecile Germain-Renaud, and Michele Sebag.
 * "Data stream clustering with affinity propagation." Knowledge and Data Engineering,
 * IEEE Transactions on 26, no. 7 (2014): 1644-1656.
 */

//****************************************************************************
// Includes
//****************************************************************************

#include "scbybuild.h"
#include "scbyassert.h"
#include "scbyshwap_front.h"
#include "scbyapcluster_front.h"

//****************************************************************************
// Helper namespace
//****************************************************************************

namespace {

    /**
     * Prints arguments
     */
    void print_args(const scby_shwap_pdist2_bin_args &args) {
        std::cout << "scby_shwap_pdist2_bin parameters:\n";
        std::cout << "\tin_matrix_path: " << args.in_matrix_path << std::endl;
        std::cout << "\tn_dims: " << args.n_dims << std::endl;
        std::cout << "\tout_samples_to_clusters_path: " << args.out_samples_to_clusters_path << std::endl;
        std::cout << "\tout_clusters_to_samples_path: " << args.out_clusters_to_samples_path << std::endl;
        std::cout << "\tout_exemplars_path: " << args.out_exemplars_path << std::endl;
        std::cout << "\tinit_size: " << args.init_size << std::endl;
        std::cout << "\tex_assoc_heur: " << args.ex_assoc_heur << std::endl;
        std::cout << "\tex_assoc_heur_scale: " << args.ex_assoc_heur_scale << std::endl;
        std::cout << "\tmax_unlabeled_thresh: " << args.max_unlabeled_thresh << std::endl;
        std::cout << "\tmax_exemplars_thresh: " << args.max_exemplars_thresh << std::endl;
        std::cout << "\tdecay_window: " << args.decay_window << std::endl;
        std::cout << "\tq: " << args.q << std::endl;
        std::cout << "\tmaxits: " << args.maxits << std::endl;
        std::cout << "\tconvits: " << args.convits << std::endl;
        std::cout << "\tlam: " << args.lam << std::endl;
        std::cout << "\tnonoise: " << args.nonoise << std::endl;
        std::cout << "\tp_conv_decay_fact: " << args.p_conv_decay_fact << std::endl;
        std::cout << "\tp_conv_max_attempts: " << args.p_conv_max_attempts << std::endl;
        std::cout << "\tinit_swiz_max_attempts: " << args.init_swiz_max_attempts << std::endl;
    }

} // end of helper namespace

//****************************************************************************
// SHWAP
//****************************************************************************

/**
 * Given input samples with coordinates in columns, will perform
 * streaming, hierarchical affinity propagation
 */
void scby_shwap_pdist2_bin(const scby_shwap_pdist2_bin_args &args) {
    print_args(args);

    // Load input matrix
    scby_matrix positions_mat;
    scby_load_mat_bin_and_reshape_col_maj(positions_mat, args.in_matrix_path, args.n_dims);

    // Cluster
    std::cout << "SHWAP clustering...\n";
    scby_index_vec_t samples_to_clusters;
    scby_ap_c2s_map_t clusters_to_samples;
    scby_index_vec_t exemplars;
    scby_ap_timers timers;
    scby_ap_timers::begin_capture(&timers, e_scby_ap_timer_all);
    if (!scby_shwap_pdist2(samples_to_clusters,
                          clusters_to_samples,
                          exemplars,
                          positions_mat,
                          &timers,
                          args.init_size,
                          args.ex_assoc_heur,
                          args.ex_assoc_heur_scale,
                          args.max_unlabeled_thresh,
                          args.max_exemplars_thresh,
                          args.decay_window,
                          args.q,
                          args.maxits,
                          args.convits,
                          args.lam,
                          args.nonoise,
                          args.p_conv_decay_fact,
                          args.p_conv_max_attempts,
                          args.init_swiz_max_attempts)) {
        std::cout << "\tclustering failed to converge\n";
        return;
    }
    scby_ap_timers::end_capture(&timers, e_scby_ap_timer_all);
    std::cout << "\tclustering successfully converged\n";
    scby_ap_timers::report();

    scby_apcluster_export_results(
        samples_to_clusters,
        clusters_to_samples,
        exemplars,
        args.out_samples_to_clusters_path,
        args.out_clusters_to_samples_path,
        args.out_exemplars_path
    );
}
