/**
 * C++ port of affinity propagation utility within APCluster R package
 *
 * Frey, Brendan J., and Delbert Dueck. "Clustering by passing messages between data points."
 * science 315, no. 5814 (2007): 972-976.
 *
 * Bodenhofer, Ulrich, Andreas Kothmeier, and Sepp Hochreiter.
 * "APCluster: an R package for affinity propagation clustering." Bioinformatics 27, no. 17 (2011): 2463-2464.
 */

#ifndef SCBYAPCLUSTER_H
#define SCBYAPCLUSTER_H

//****************************************************************************
// Includes
//****************************************************************************

#include "scbybuild.h"
#include "scbyassert.h"
#include "scbytypes.h"

#include <vector>

//****************************************************************************
// Defines
//****************************************************************************

/**
 * To have consistent defaults for all declarations of cluster method, defer
 * to these macro values
 */

/**
 * Default maximum iterations
 */
#define SCBY_AP_DEF_MAXITS 1000

/**
 * Default number of convergence iterations
 */
#define SCBY_AP_DEF_CONVITS 100

/**
 * Default damping factor
 */
#define SCBY_AP_DEF_LAM as_scby_real(0.9)

/**
 * Default value of whether or not to add noise
 */
#define SCBY_AP_DEF_NONOISE false

/**
 * Default similarity quantile for use as preferences
 */
#define SCBY_AP_DEF_PREF_QUANTILE as_scby_real(0.5)

/**
 * Type used for storing indices
 */
typedef scby_matrix_utils::Col<scby_matrix_utils::uword> scby_index_vec_t;

/**
 * Type used for storing mapping from cluster id to set of sample ids
 */
typedef std::vector< std::vector<scby_matrix_utils::uword> > scby_ap_c2s_map_t;

//****************************************************************************
// AP timers
//****************************************************************************

/**
 * Enumerated timers
 */
enum escby_ap_timers {
    e_scby_ap_timer_pdist=0,
    e_scby_ap_timer_find_exemplars,
    e_scby_ap_timer_refine_clusters,
    e_scby_ap_timer_all,
    e_scby_ap_timers_num
};

/**
 * Utility for tracking operation times
 */
class scby_ap_timers {
public:

    /**
     * If valid pointer, begins timing
     */
    static void begin_capture(scby_ap_timers *t, const escby_ap_timers et);

    /**
     * If valid pointer, ends timing
     */
    static void end_capture(scby_ap_timers *t, const escby_ap_timers et);

    /**
     * @return total number of seconds between timer capture calls
     */
    static scby_real get_timer_secs(const escby_ap_timers et);

    /**
     * outputs times taken by tracked operations
     */
    static void report();

private:

    /**
     * Total time accrued
     */
    static std::clock_t accrued_timers[e_scby_ap_timers_num];

    /**
     * The time between begin/end capture calls
     */
    std::clock_t transient_timers[e_scby_ap_timers_num];
};

//****************************************************************************
// scbyapcluster
//****************************************************************************

// Implementation detail: the noise initialization reference code (matlab, Rcpp, etc)
// expects a full-sized matrix and after initialization s(i,j) is almost certainly not symmetric
// hence, *not* using a memory efficient symmetric matrix (e.g. one which only stores upper tri)

/**
 * @param samples_to_clusters - stores cluster assignment for each sample
 *  - output is size n where n is number of samples
 * @param clusters_to_samples - stores sample assignments for each cluster
 * @param exemplars - stores index of exemplar sample for each cluster
 *  - output is size k where k is number of clusters found
 * @param s - a similarity matrix - typically taken to be negative of the
 *  Euclidean squared distance between data points. preferences will be stored
 *  along diagonal and matrix also may be modified by Gaussian noise
 * @param p - vector of preferences for each input sample. higher preferences means sample
 *  is more likely to be found as an exemplar. if no prior preference, science paper recommends
 *  to simply use the median similarity value
 * @param timers - (optional) used for timing various operations
 * @param maxits - maximum number of iterations
 * @param convits - the algorithm terminates if the exemplars have not changed for convits
 *  iterations
 * @param lam - damping factor; should be a value in the range [0.5, 1); higher values correspond
 *  to heavy damping which may be needed if oscillations occur
 * @param nonoise - if false, adds a small amount of noise to similarities to prevent degenerate cases
 * @return TRUE if converged, FALSE o/w
 */
extern bool scby_apcluster(scby_index_vec_t &samples_to_clusters,
                           scby_ap_c2s_map_t &clusters_to_samples,
                           scby_index_vec_t &exemplars,
                           scby_matrix &s,
                           const scby_vec_col &p,
                           scby_ap_timers *timers=NULL,
                           const size_t maxits=SCBY_AP_DEF_MAXITS,
                           const size_t convits=SCBY_AP_DEF_CONVITS,
                           const scby_real lam=SCBY_AP_DEF_LAM,
                           const bool nonoise=SCBY_AP_DEF_NONOISE);

/**
 * This stock version internally computes default pairwise similarities and preferences
 * based on input positions_mat and q parameters
 * @param samples_to_clusters - stores cluster assignment for each sample
 *  - output is size n where n is number of samples
 * @param clusters_to_samples - stores sample assignments for each cluster
 * @param exemplars - stores index of exemplar sample for each cluster
 *  - output is size k where k is number of clusters found
 * @param positions_mat - matrix of sample coordinates, each sample is assumed to take a single column
 * @param timers - (optional) used for timing various operations
 * @param q - specifies which quantile similarity to assign as the no-prior preference
 * @param maxits - maximum number of iterations
 * @param convits - the algorithm terminates if the exemplars have not changed for convits
 *  iterations
 * @param lam - damping factor; should be a value in the range [0.5, 1); higher values correspond
 *  to heavy damping which may be needed if oscillations occur
 * @param nonoise - if false, adds a small amount of noise to similarities to prevent degenerate cases
 * @return TRUE if converged, FALSE o/w
 */
extern bool scby_apcluster_pdist2(scby_index_vec_t &samples_to_clusters,
                                  scby_ap_c2s_map_t &clusters_to_samples,
                                  scby_index_vec_t &exemplars,
                                  const scby_matrix &positions_mat,
                                  scby_ap_timers *timers=NULL,
                                  const scby_real q=SCBY_AP_DEF_PREF_QUANTILE,
                                  const size_t maxits=SCBY_AP_DEF_MAXITS,
                                  const size_t convits=SCBY_AP_DEF_CONVITS,
                                  const scby_real lam=SCBY_AP_DEF_LAM,
                                  const bool nonoise=SCBY_AP_DEF_NONOISE);

/**
 * Utility for determining preferences based on the q'th quantile similarity score
 * present within the lower triangle of the input similarity matrix
 * @param p - output vector of preferences based on the similarity score
 *  at the q-th quantile - each element is set to same value
 * @param s - initialized symmetric pairwise similarity matrix
 * @param q - specifies the quantile of the similarity score in [0, 1.0],
 *  the score corresponding to this quantile is replicated to each element of p
 */
extern void scby_apcluster_util_qs_prefs_lower_tri(
        scby_vec_col &p,
        const scby_matrix &s,
        const scby_real q=SCBY_AP_DEF_PREF_QUANTILE);

/**
 * Utility for determining preferences based on the q'th quantile similarity score
 * based on both the upper and lower tri of the similarity matrix. Note, the
 * diagonal is excluded as the input preferences are to be placed along it.
 * @param p - output vector of preferences based on the similarity score
 *  at the q-th quantile - each element is set to same value
 * @param s - initialized pairwise similarity matrix - square, can be non-symmetric
 * @param q - specifies the quantile of the similarity score in [0, 1.0],
 *  the score corresponding to this quantile is replicated to each element of p
 */
extern void scby_apcluster_util_qs_prefs_no_diag(
        scby_vec_col &p,
        const scby_matrix &s,
        const scby_real q = SCBY_AP_DEF_PREF_QUANTILE);

#endif // SCBYAPCLUSTER_H
