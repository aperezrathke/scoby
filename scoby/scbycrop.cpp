/**
 * @author Alan Perez-Rathke
 * @date December 1st, 2015
 *
 * Simple utility to crop ends of a matrix.
 * Useful for generating manageable test data
 */

#include "scbybuild.h"
#include "scbycrop.h"
#include "scbyarmadillo.h"

// Utilities
namespace {

    void print_args(const scby_crop_args_t &args) {
        std::cout << "Crop matrix util args:\n";
        // The max number of rows to keep
        // if <= 0, then all rows are kept
        std::cout << "\tNum keep rows: " << args.num_keep_rows << std::endl;
        // The max number of columns to keep
        // if <= 0, then all columns are kept
        std::cout << "\tNum keep cols: " << args.num_keep_cols << std::endl;
        // Path to binary input matrix
        std::cout << "\tInput matrix path: " << args.in_matrix_path << std::endl;
        // Path to write cropped matrix
        std::cout << "\tOutput matrix path: " << args.out_matrix_path << std::endl;
    }

} // end of helper namespace

// Reduces size of input matrix - useful for generating test data
void scby_crop_bin(const scby_crop_args_t &args) {
    // Output arguments
    print_args(args);
    // Load input matrix
    scby_matrix mat;
    mat.load(args.in_matrix_path);
    std::cout << "Read " << mat.n_rows << " by " << mat.n_cols << " input matrix\n";
    // Determine final dimensions
    const size_t final_n_rows = (args.num_keep_rows > 0) ? ((size_t)args.num_keep_rows) : mat.n_rows;
    const size_t final_n_cols = (args.num_keep_cols > 0) ? ((size_t)args.num_keep_cols) : mat.n_cols;
    if ((final_n_rows == mat.n_rows) && (final_n_cols == mat.n_cols)) {
        std::cout << "Input matrix is unchanged -> exiting\n";
    } else {
        // Crop matrix
        mat = scby_matrix_utils::resize(mat, final_n_rows, final_n_cols);
        // Write cropped matrix
        scby_write_matrix_bin(mat, args.out_matrix_path);
    }
    std::cout << "Finished.\n";
}
