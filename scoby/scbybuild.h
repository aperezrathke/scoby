/**
 * @author Alan Perez-Rathke
 * @date July 25, 2014
 */

#ifndef SCBYBUILD_H
#define SCBYBUILD_H

/**
 * OS detection
 */
#ifdef _WIN32
#   define SCBY_BUILD_OS_WINDOWS
#else
#   define SCBY_BUILD_OS_UNIX
#endif // _WIN32

/**
 * Build types
 */
// _DEBUG is defined by Visual Studio (non-ANSI compliant)
// NDEBUG is defined by CMake for GCC release builds
// NDEBUG is part of C++ ANSI standard for disabling assertions
#if defined(_DEBUG) || (defined(SCBY_BUILD_OS_UNIX) && !defined(NDEBUG))
#   define SCBY_BUILD_TYPE_DEBUG
#endif

/**
 * Compiler detection
 */
#ifdef _MSC_VER
#   define SCBY_BUILD_COMPILER_MSVC
#elif (defined(__ICC) || defined(__ICL) || defined(__INTEL_COMPILER))
#   define SCBY_BUILD_COMPILER_ICC
#elif (defined(__GNUC__))
#   define SCBY_BUILD_COMPILER_GCC
#else
#   error Unrecognized compiler
#endif

/**
 * This master switch toggles assertion checking
 * (will enable even if NDEBUG is defined)
 */
//#define SCBY_BUILD_ENABLE_ASSERT

// Allow assertions to be on by default for debug builds and off o/w
#ifndef SCBY_BUILD_ENABLE_ASSERT
#   ifdef SCBY_BUILD_TYPE_DEBUG
#       define SCBY_BUILD_ENABLE_ASSERT
#   else
#       ifndef NDEBUG
#           define NDEBUG
#       endif // NDEBUG
#   endif // SCBY_BUILD_TYPE_DEBUG
#endif // SCBY_BUILD_ENABLE_ASSERT

#ifdef SCBY_BUILD_OS_WINDOWS
// Windows specific defines go here
#   include <string>
#   define stricmp _stricmp // HACK to remove VC++ complaining about POSIX
#elif defined(SCBY_BUILD_OS_UNIX)
#   include <strings.h>
#   define stricmp strcasecmp // HACK, apparently stricmp is not on UNIX
#endif

// Toggle armadillo debug checks
#ifndef SCBY_BUILD_ARMADILLO_DEBUG
#   ifdef SCBY_BUILD_TYPE_DEBUG
#       define SCBY_BUILD_ARMADILLO_DEBUG
#   else
// Uncomment to enable debug checks in release
//#       define SCBY_BUILD_ARMADILLO_DEBUG
#   endif
#endif // SCBY_BUILD_ARMADILLO_DEBUG

/**
 * Switch to toggle seeding the random number generator
 */
#ifndef SCBY_BUILD_SEED_RAND
#   ifndef SCBY_BUILD_TYPE_DEBUG
#       define SCBY_BUILD_SEED_RAND
#   endif
#endif // SCBY_BUILD_SEED_RAND

#endif // SCBYBUILD_H
