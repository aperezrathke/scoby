/**
 * @author Alan Perez-Rathke
 * @date July 25, 2014
 */

#include "scbybuild.h"
#include "scbyassert.h"
#include "scbytypes.h"
#include "scbytuple_utils.h"
#include "scbysymmatrix_io.h"
#include "scbyvector_io.h"
#include "scbykdist.h"
#include "scbydbscan.h"
#include "scbycrop.h"
#include "scbypca_reduce.h"
#include "scbykmeans_silh.h"
#include "scbycompute_pdist_front.h"
#include "scbyapcluster_front.h"
#include "scbyshwap_front.h"

#include <stdlib.h>
#include <string>

namespace {
    // @return upper case version of a string
    std::string get_upper_case(const std::string &str) {
        std::string tmp = str;
        std::transform(tmp.begin(), tmp.end(), tmp.begin(), ::toupper);
        return tmp;
    }

    // @return true if strings are equal (case insensitive), false o/w
    bool case_insenstive_cmp(const std::string &a, const std::string &b) {
        return get_upper_case(a) == get_upper_case(b);
    }

    // Exit failure if argument count not in [min_argc, max_argc]
    void exit_if_argc_fail(const int argc, const int min_argc, const int max_argc) {
        if (argc < min_argc || argc > max_argc) {
            std::cout << "Error: argument count of " << argc << " not in range ["
                << min_argc << ", " << max_argc << "].\nExiting." << std::endl;
            scby_assert(false);
            exit(EXIT_FAILURE);
        }
    }

    // Exit failure if argument count not equal to target
    void exit_if_argc_fail(const int argc, const int target_argc) {
        if (argc != target_argc) {
            std::cout << "Error: argument count of " << argc
                      << " not equal to expected count of "
                      << target_argc << ".\nExiting." << std::endl;
            scby_assert(false);
            exit(EXIT_FAILURE);
        }
    }
} // end of helper namespace

//******************************************************
// APP: scby_convert_sym_matrix
//******************************************************

void scby_convert_sym_matrix_lower_tri_tsv_to_bin(const char *in_tsv_path, const char *out_bin_path) {
    // verify paths are not null
    scby_assert(NULL != in_tsv_path);
    scby_assert(NULL != out_bin_path);

    // convert tsv to binary
    std::cout << "Reading matrix: " << in_tsv_path << std::endl;
    scby_sym_matrix m;
    if (scby_sym_matrix_read_lower_tri_tsv(m, in_tsv_path)) {
        std::cout << "Writing symmetric matrix: " << out_bin_path << std::endl;
        scby_sym_matrix_write_bin(m, out_bin_path);
    }
    else {
        std::cout << "Error: unable to read path or matrix is not square.\n";
    }
}

int scby_convert_sym_matrix_lower_tri_tsv_to_bin_main(int argc, char **argv) {
    exit_if_argc_fail(argc, 4);
    scby_convert_sym_matrix_lower_tri_tsv_to_bin(
        argv[2], // in_tsv_path
        argv[3]  // out_hack_bin_path
    );
    return 0;
}

void scby_convert_sym_matrix_bin_to_lower_tri_tsv(const char *in_bin_path, const char *out_tsv_path) {
    // verify paths are not null
    scby_assert(NULL != in_bin_path);
    scby_assert(NULL != out_tsv_path);

    // convert binary to tsv
    std::cout << "Reading symmetric matrix: " << in_bin_path << std::endl;
    scby_sym_matrix m;
    scby_sym_matrix_read_bin(m, in_bin_path);
    std::cout << "Writing lower tri: " << out_tsv_path << std::endl;
    scby_sym_matrix_write_lower_tri_tsv(m, out_tsv_path);
}

int scby_convert_sym_matrix_bin_to_lower_tri_tsv_main(int argc, char **argv) {
    exit_if_argc_fail(argc, 4);
    scby_convert_sym_matrix_bin_to_lower_tri_tsv(
        argv[2], // in_bin_path
        argv[3]  // out_tsv_path
    );
    return 0;
}

//******************************************************
// APP: get_sorted_k_dists_for_bin_matrix
//******************************************************

// Note: MinPts = k = 4 is recommended setting for use with DBSCAN
void scby_get_sorted_k_dists_for_sym_bin(const char *in_bin_path, const size_t k, const char *out_k_dists_path) {
    // verify paths are not null
    scby_assert(NULL != in_bin_path);
    scby_assert(NULL != out_k_dists_path);

    // read binary matrix
    std::cout << "Reading symmetric binary matrix: " << in_bin_path << std::endl;
    scby_sym_matrix dist_matrix;
    scby_sym_matrix_read_bin(dist_matrix, in_bin_path);

    // get distance profile
    std::cout << "Getting distance profile for " << dist_matrix.size1()
              << " x " << dist_matrix.size2()
              << " symmetric, pairwise distance matrix with k = " << k << std::endl;
    std::vector<scby_real> sorted_k_dists;
    scby_get_sorted_k_dist(sorted_k_dists, dist_matrix, k);

    // export distance profile
    std::cout << "Writing distance profile: " << out_k_dists_path << std::endl;
    scby_vector_write(sorted_k_dists, out_k_dists_path);
}

int scby_get_sorted_k_dists_main(int argc, char **argv) {
    exit_if_argc_fail(argc, 5);
    scby_get_sorted_k_dists_for_sym_bin(
        argv[2], // in_sym_bin_path
        boost::lexical_cast<size_t>(argv[3]), // k = candidate MinPts
        argv[4] // out_k_dists_path
    );
    return 0;
}

//******************************************************
// APP: dbscan_sym_bin_matrix
//******************************************************

// Note: min_pts = 4 is recommended setting for use with DBSCAN
void scby_dbscan_sym_bin(
    const char *in_bin_path,
    const scby_real eps,
    const size_t min_pts, 
    const char *out_clusters_path) {

    // validate parameters
    scby_assert(NULL != in_bin_path);
    scby_assert(NULL != out_clusters_path);
    scby_assert(eps > 0.0);

    if (eps <= 0.0) {
        std::cout << "Error: DBSCAN Epsilon must be positive real.\n";
        return;
    }

    // read binary matrix
    std::cout << "Reading symmetric matrix: " << in_bin_path << std::endl;
    scby_sym_matrix dist_matrix;
    scby_sym_matrix_read_bin(dist_matrix, in_bin_path);

    // find clusters
    std::cout << "DBSCAN " << dist_matrix.size1() << " x " << dist_matrix.size2()
              << " symmetric, pairwise distance matrix with eps = "
              << eps << " and min_pts = " << min_pts << std::endl;
    std::vector<scby_sym_matrix::size_type> clusters;
    scby_dbscan(clusters, dist_matrix, eps, min_pts);

    // write clusters
    std::cout << "Writing clusters to " << out_clusters_path << std::endl;
    scby_vector_write(clusters, out_clusters_path);
}

int scby_dbscan_main(int argc, char **argv) {
    exit_if_argc_fail(argc, 6);
    scby_dbscan_sym_bin(
        argv[2], // in_hack_bin_path
        boost::lexical_cast<scby_real>(argv[3]), // eps
        boost::lexical_cast<size_t>(argv[4]),  // min_pts
        argv[5] // out_clusters_path
    );
    return 0;
}

//******************************************************
// APP: get_dbs_cluster_freq_counts
//******************************************************

namespace {
    // enum defining roles of a cluster analysis tuple
    enum ESCBY_DBS_CLUSTER_ANALYSIS {
        eClusterId = 0,
        eClusterCount = 1,
        eClusterFreq = 2,
        eClusterNodes = 3
    };

    // sorting predicate for ascending analysis tuples
    bool scby_sort_dbs_cluster_analysis_tuple_asc(const scby_cluster_analysis_tuple &lhs,
            const scby_cluster_analysis_tuple &rhs) {
        return std::get<ESCBY_DBS_CLUSTER_ANALYSIS::eClusterCount>(lhs) <
               std::get<ESCBY_DBS_CLUSTER_ANALYSIS::eClusterCount>(rhs);
    }

    // sorting predicate for descending analysis tuples
    bool scby_sort_dbs_cluster_analysis_tuple_desc(const scby_cluster_analysis_tuple &lhs,
            const scby_cluster_analysis_tuple &rhs) {
        return std::get<ESCBY_DBS_CLUSTER_ANALYSIS::eClusterCount>(lhs) >
               std::get<ESCBY_DBS_CLUSTER_ANALYSIS::eClusterCount>(rhs);
    }
} // end of helper namepsace

void scby_get_dbs_cluster_freq_counts(const char *in_clusters_path, const char *out_freq_counts_path,
                                      const int b_ascending = 1) {
    // validate parameters
    scby_assert(NULL != in_clusters_path);
    scby_assert(NULL != out_freq_counts_path);

    std::cout << "Reading clusters: " << in_clusters_path << std::endl;
    std::vector<size_t> clusters;
    scby_vector_read(clusters, in_clusters_path);

    scby_assert(clusters.size() > 0);
    std::sort(clusters.begin(), clusters.end(), std::less<size_t>());

    std::vector<scby_cluster_analysis_tuple> analysis;

    analysis.push_back(std::make_tuple(clusters[0] /*id*/, 1 /*count*/, 0.0 /*freq*/, std::vector<size_t>(1, 0)));

    for (size_t inode = 1; inode < clusters.size(); ++inode) {
        // see if we need to start tallying a different cluster
        if (std::get<ESCBY_DBS_CLUSTER_ANALYSIS::eClusterId>(analysis.back()) != clusters[inode]) {
            // fill in frequency of current cluster
            std::get<ESCBY_DBS_CLUSTER_ANALYSIS::eClusterFreq>(analysis.back()) =
                ((scby_real) std::get<ESCBY_DBS_CLUSTER_ANALYSIS::eClusterCount>(analysis.back())) /
                ((scby_real) clusters.size());
            analysis.push_back(
                std::make_tuple(clusters[inode] /*id*/, 1 /*count*/, 0.0 /*freq*/, std::vector<size_t>(1, inode)));
        }
        // else, increment cluster count
        else {
            ++(std::get<ESCBY_DBS_CLUSTER_ANALYSIS::eClusterCount>(analysis.back()));
            (std::get<ESCBY_DBS_CLUSTER_ANALYSIS::eClusterNodes>(analysis.back())).push_back(inode);
        }
    }

    // fill in frequency of last cluster
    std::get<ESCBY_DBS_CLUSTER_ANALYSIS::eClusterFreq>(analysis.back()) =
        ((scby_real) std::get<ESCBY_DBS_CLUSTER_ANALYSIS::eClusterCount>(analysis.back())) / ((scby_real) clusters.size());
    std::sort(analysis.begin(), analysis.end(),
              b_ascending ? &scby_sort_dbs_cluster_analysis_tuple_asc : &scby_sort_dbs_cluster_analysis_tuple_desc);

    // output analysis
    std::cout << "Writing analysis(" << (b_ascending ? "asc" : "desc") << "): " << out_freq_counts_path << std::endl;
    scby_vector_write(analysis, out_freq_counts_path);
}

int scby_get_dbs_cluster_freq_counts_main(int argc, char **argv) {
    exit_if_argc_fail(argc, 5);
    scby_get_dbs_cluster_freq_counts(
        argv[2], // in_clusters_path
        argv[3], // out_freq_counts_path
        boost::lexical_cast<int>(argv[4]) // ascending
    );
    return 0;
}

//******************************************************
// APP: Fast matrix csv <-> bin converters
//******************************************************

// Expected inputs
// @param in_csv_path - path to input csv matrix
// @param out_bin_path - path to write binary matrix to
// @param has_header_row - TRUE if first row is header row,
//  else if FALSE, then no header is assumed.
// @return TRUE if successful, FALSE otherwise
int scby_convert_matrix_csv_to_bin_main(const int argc, char **argv) {
    exit_if_argc_fail(argc, 5);
    return scby_convert_matrix_csv_to_bin(
               argv[2], // in_csv_path
               argv[3], // out_bin_path
               boost::lexical_cast<bool>(argv[4])
           ) ? 0 : 1;
}

// Expected inputs
// @param in_bin_path - path to write binary matrix to
// @param out_csv_path - path to output csv matrix
// @return TRUE if successful, FALSE otherwise
int scby_convert_matrix_bin_to_csv_main(const int argc, char **argv) {
    exit_if_argc_fail(argc, 4);
    return scby_convert_matrix_bin_to_csv(
               argv[2], // in_bin_path
               argv[3]  // out_csv_path
           ) ? 0 : 1;
}

//******************************************************
// APP: crop coordinates
//******************************************************

// @param num_keep_rows - The max number of rows to keep, if <= 0, then all rows are kept
// @param num_keep_cols - The max number of columns to keep, if <= 0, then all columns are kept
// @param in_bin_matrix_path - Path to binary input matrix
// @param out_matrix_path - Path to write cropped binary matrix
int scby_crop_main(const int argc, char **argv) {
    exit_if_argc_fail(argc, escby_crop_max_num_args);
    struct scby_crop_args_t args;
    args.num_keep_rows = boost::lexical_cast<int>(argv[escby_crop_num_keep_rows]);
    args.num_keep_cols = boost::lexical_cast<int>(argv[escby_crop_num_keep_cols]);
    args.in_matrix_path = argv[escby_crop_in_matrix_path];
    args.out_matrix_path = argv[escby_crop_out_matrix_path];

    scby_crop_bin(args);
    return 0;
}

//******************************************************
// APP: PCA reduce
//******************************************************

// @param perc_keep - The percentage of variance explained to keep in (0.0, 1.0]
// = sum(eigenvalues 1 to k) / sum(eigenvalues 1 to n)
// where k <= n and n is total number of eigenvalues
// @param obs_in_rows - Each row of input matrix is an observation and each column is a variable,
// If this is the case, then pass in '1', else pass in '0'
// (ie. if samples are in rows - pass '1', else pass '0')
// @param n_dims - Number of base dimensions of each observation
// @param in_matrix_path - Path to input matrix
// @param out_matrix_path - Path to write reduced matrix
int scby_pca_reduce_main(const int argc, char **argv) {
    exit_if_argc_fail(argc, escby_pca_reduce_max_num_args);
    struct scby_pca_reduce_args_t args;
    args.perc_keep = boost::lexical_cast<scby_real>(argv[escby_pca_reduce_perc_keep]);
    args.obs_in_rows = boost::lexical_cast<bool>(argv[escby_pca_reduce_obs_in_rows]);
    args.n_dims = boost::lexical_cast<scby_uint>(argv[escby_pca_reduce_n_dims]);
    args.in_matrix_path = argv[escby_pca_reduce_in_matrix_path];
    args.out_matrix_path = argv[escby_pca_reduce_out_matrix_path];

    scby_pca_reduce_bin(args);
    return 0;
}

//******************************************************
// APP: compute rmsd
//******************************************************

// Notes on expected inputs:
// @param in_bin_path - character string
// @param ndim - positive integer
// @param out_bin_path - character string - if "NULL" -> null char*
// @param out_csv_path - (optional) character string - if "NULL" -> null char*
int scby_compute_rmsd_main(const int argc, char **argv) {
    exit_if_argc_fail(argc, 5, 6);
    const bool is_valid_bin_path = !case_insenstive_cmp(argv[4], "NULL");
    const bool is_valid_csv_path = (argc == 6) && !case_insenstive_cmp(argv[5], "NULL");
    scby_compute_rmsd_front(
        argv[2], // in_bin_path
        boost::lexical_cast<unsigned int>(argv[3]), // n_dims
        is_valid_bin_path ? argv[4] : ((char *)NULL), // out_bin_path
        is_valid_csv_path ? argv[5] : ((char *)NULL) // out_csv_path
    );
    return 0;
}

// Notes on expected inputs:
// @param n_threads - the number of threads to allocate
// @param inc_by - the size of the work batch
// @param in_bin_path - character string
// @param ndim - positive integer
// @param out_bin_path - character string - if "NULL" -> null char*
// @param out_csv_path - (optional) character string - if "NULL" -> null char*
int scby_compute_rmsd_mt_main(const int argc, char **argv) {
    exit_if_argc_fail(argc, 7, 8);
    const bool is_valid_bin_path = !case_insenstive_cmp(argv[6], "NULL");
    const bool is_valid_csv_path = (argc == 8) && !case_insenstive_cmp(argv[7], "NULL");
    scby_compute_rmsd_mt_front(
        boost::lexical_cast<unsigned int>(argv[2]), // n_threads
        boost::lexical_cast<unsigned int>(argv[3]), // inc_by
        argv[4], // in_bin_path
        boost::lexical_cast<unsigned int>(argv[5]), // n_dims
        is_valid_bin_path ? argv[6] : ((char *)NULL), // out_bin_path
        is_valid_csv_path ? argv[7] : ((char *)NULL) // out_csv_path
    );
    return 0;
}

//******************************************************
// APP: k-means
//******************************************************

// See scby_kmeans_silh_args_t for argument descriptions.
int scby_k_means_silh_main(const int argc, char **argv) {
    exit_if_argc_fail(argc, escby_kmeans_silh_min_num_args, escby_kmeans_silh_max_num_args);
    // Arguments to silhouette based k-means clusterer
    struct scby_kmeans_silh_args_t args;
    args.min_k = boost::lexical_cast<scby_uint>(argv[escby_kmeans_silh_min_k]);
    args.max_k = boost::lexical_cast<scby_uint>(argv[escby_kmeans_silh_max_k]);
    args.k_inc = boost::lexical_cast<scby_uint>(argv[escby_kmeans_silh_k_inc]);
    args.k_report_interv = boost::lexical_cast<scby_uint>(argv[escby_kmeans_silh_k_report_interv]);
    args.n_rand_restarts = boost::lexical_cast<scby_uint>(argv[escby_kmeans_silh_n_rand_restarts]);
    args.n_dims = boost::lexical_cast<scby_uint>(argv[escby_kmeans_silh_n_dims]);
    args.delta = boost::lexical_cast<scby_real>(argv[escby_kmeans_silh_delta]);
    args.max_iters_per_run = boost::lexical_cast<scby_uint>(argv[escby_kmeans_silh_max_iters_per_run]);
    args.in_bin_coords_path = argv[escby_kmeans_silh_in_bin_coords_path];
    args.out_clusters_path = argv[escby_kmeans_silh_out_clusters_path];

    // Handle optional output paths
    const bool is_valid_centroids_path =
        (argc > escby_kmeans_silh_min_num_args) &&
        !case_insenstive_cmp(argv[escby_kmeans_silh_out_centroids_path], "NULL");
    const bool is_valid_cluster_counts_path =
        (argc > (escby_kmeans_silh_min_num_args+1)) &&
        !case_insenstive_cmp(argv[escby_kmeans_silh_out_cluster_counts_path], "NULL");
    const bool is_valid_silh_scores_path =
        (argc > (escby_kmeans_silh_min_num_args+2)) &&
        !case_insenstive_cmp(argv[escby_kmeans_silh_out_silh_scores_path], "NULL");
    const bool is_valid_silh_k_scores_path =
        (argc > (escby_kmeans_silh_min_num_args+3)) &&
        !case_insenstive_cmp(argv[escby_kmeans_silh_out_silh_k_scores_path], "NULL");

    args.out_centroids_path = is_valid_centroids_path ?
                              argv[escby_kmeans_silh_out_centroids_path] : (char *) NULL;

    args.out_cluster_counts_path = is_valid_cluster_counts_path ?
                                   argv[escby_kmeans_silh_out_cluster_counts_path] : (char *) NULL;

    args.out_silh_scores_path = is_valid_silh_scores_path ?
                                argv[escby_kmeans_silh_out_silh_scores_path] : (char *) NULL;

    args.out_silh_k_scores_path = is_valid_silh_k_scores_path ?
                                  argv[escby_kmeans_silh_out_silh_k_scores_path] : (char *) NULL;

    scby_k_means_silh(args);

    return 0;
}

//******************************************************
// APP: Affinity Propagation
//******************************************************

/**
 * Expected arguments
 * @param in_matrix_path - Path to binary col-major input matrix
 * @param n_dims - number of consecutive columns defining a single sample,
 *  where each column is a different dimension of the sample
 * @param out_samples_to_clusters_path - Path to write 0-based sample id to cluster id mapping in csv format
 * @param out_clusters_to_samples_path - Path to write cluster id to samples ids mapping in csv format
 * @param out_exemplars_path - Path to write exemplar ids in csv format
 */
int scby_apcluster_main(const int argc, char **argv) {
    exit_if_argc_fail(argc, escby_apcb_max_num_args);
    scby_apcluster_bin_args_t args;
    args.in_matrix_path = argv[escby_apcb_in_matrix_path];
    args.n_dims = boost::lexical_cast<scby_uint>(argv[escby_apcb_n_dims]);
    args.out_samples_to_clusters_path = argv[escby_apcb_out_samples_to_clusters_path];
    args.out_clusters_to_samples_path = argv[escby_apcb_out_clusters_to_samples_path];
    args.out_exemplars_path = argv[escby_apcb_out_exemplars_path];

    scby_apcluster_bin(args);

    return 0;
}

//******************************************************
// APP: SHWAP
//******************************************************

/**
 * Streaming, hierarchical, weighted affinity propagation for clustering large data sets.
 *  - based on StrAP algorithm.
 * Expected arguments:
 * @param in_matrix_path - Path to input matrix
 * @param n_dims - Number of dimensions
 * @param out_samples_to_clusters_path - Path to write sample id to cluster id mapping in csv format
 * @param out_clusters_to_samples_path - Path to write cluster id to set of sample ids in csv format
 * @param out_exemplars_path - Path to write list of exemplar ids
 * @param init_size - The first init_size elements are clustered using affinity propagation to
 *  find the initial exemplar and cluster assignments
 * @param ex_assoc_heur - The type of heuristic to use for attempting to associate an unlabeled sample
 *  to an existing exemplar - 0 -> nascent universal average distance to exemplar 1 -> nascent universal median average distance to exemplar
 * @param ex_assoc_heur_scale - The association heuristic cutoff distance is scaled by this factor
 *  -> values less than 1.0 make it harder to associate an unlabeled sample to an exemplar
 *  -> values greater than 1.0 make it easier to associate an unlabeled sample to an exemplar
 * @param max_unlabeled_thresh - Threshold for triggering a weighted, local affinity propagation on samples
 *  which failed to be associated to an exemplar
 * @param max_exemplars_thresh - Threshold for triggering a weighted, hierarchical affinity propagation run on only
 *  the exemplars. This is to attempt to merge exemplars and reduce the total count in order
 *  for the local affinity propagation runs to be performant.
 * @param decay_window -  Each exemplar has a window length defined as the number of samples that have been processed since
 *  the last time a sample was associated to that exemplar. If the window length exceeds the decay value
 *  then the exemplar is removed. Furthermore, the multiplicity weights associated to that exemplar are
 *  reduced as a function of this decay length.
 * @param q - Specifies which quantile similarity to assign as the no-prior preference
 * @param maxits - Maximum number of iterations for each affinity propagation run
 * @param convits - A single run of affinity propagation terminates if the exemplars have not changed for convits
 *  iterations
 * @param lam - Damping factor: should be a value in the range [0.5, 1); higher values correspond
 *  to heavy damping which may be needed if oscillations occur
 * @param nonoise - If false, adds a small amount of noise to similarities to prevent degenerate cases
 * @param p_conv_decay_fact - Real-valued scaling factor using for scaling preferences when a weighted,
 *  affinity propagation run fails to converge.
 * @param p_conv_max_attemps - Integer number of attempts at scaling the preference scores if a weighted,
 *  affinity propagation run fails before giving up
 * @param init_swiz_max_attempts - Integer maximum number of times to the swizzle the input samples in an
 #  attempt to obtain an ensemble that converges during the initial affinity propagation run.
 */
int scby_shwap_main(const int argc, char **argv) {
    exit_if_argc_fail(argc, escby_shwapc_pd2b_min_num_args, escby_shwapc_pd2b_max_num_args);
    scby_shwap_pdist2_bin_args args;

    // Initialize default arguments
    args.init_swiz_max_attempts = SCBY_SHWAP_DEF_INIT_SWIZ_MAX_ATTEMPTS;
    args.p_conv_max_attempts = SCBY_SHWAP_DEF_P_CONV_MAX_ATTEMPTS;
    args.p_conv_decay_fact = SCBY_SHWAP_DEF_P_CONV_DECAY_FACT;
    args.nonoise = SCBY_AP_DEF_NONOISE;
    args.lam = SCBY_AP_DEF_LAM;
    args.convits = SCBY_AP_DEF_CONVITS;
    args.maxits = SCBY_AP_DEF_MAXITS;
    args.q = SCBY_AP_DEF_PREF_QUANTILE;
    args.decay_window = SCBY_SHWAP_DEF_DECAY_WINDOW;
    args.max_exemplars_thresh = SCBY_SHWAP_DEF_MAX_EXEMPLARS_THRESH;
    args.max_unlabeled_thresh = SCBY_SHWAP_DEF_MAX_UNLABELED_THRESH;
    args.ex_assoc_heur = SCBY_SHWAP_DEF_EX_ASSOC_HEUR;
    args.ex_assoc_heur_scale = SCBY_SHWAP_DEF_EX_ASSOC_HEUR_SCALE;
    args.init_size = SCBY_SHWAP_DEF_INIT_SIZE;

    // Process mandatory arguments
    args.in_matrix_path = argv[escby_shwapc_pd2b_in_matrix_path];
    args.n_dims = boost::lexical_cast<scby_uint>(argv[escby_shwapc_pd2b_n_dims]);
    args.out_samples_to_clusters_path = argv[escby_shwapc_pd2b_out_samples_to_clusters_path];
    args.out_clusters_to_samples_path = argv[escby_shwapc_pd2b_out_clusters_to_samples_path];
    args.out_exemplars_path = argv[escby_shwapc_pd2b_out_exemplars_path];

    // Process optional arguments
    switch(argc) {
    case (escby_shwapc_pd2b_max_num_args):
        args.init_swiz_max_attempts = boost::lexical_cast<scby_uint>(argv[escby_shwapc_pd2b_init_swiz_max_attempts]);
    case (escby_shwapc_pd2b_max_num_args-1):
        args.p_conv_max_attempts = boost::lexical_cast<scby_uint>(argv[escby_shwapc_pd2b_p_conv_max_attempts]);
    case (escby_shwapc_pd2b_max_num_args-2) :
        args.p_conv_decay_fact = boost::lexical_cast<scby_real>(argv[escby_shwapc_pd2b_p_conv_decay_fact]);
    case (escby_shwapc_pd2b_max_num_args-3):
        args.nonoise = boost::lexical_cast<bool>(argv[escby_shwapc_pd2b_nonoise]);
    case (escby_shwapc_pd2b_max_num_args-4):
        args.lam = boost::lexical_cast<scby_real>(argv[escby_shwapc_pd2b_lam]);
    case (escby_shwapc_pd2b_max_num_args-5):
        args.convits = boost::lexical_cast<scby_uint>(argv[escby_shwapc_pd2b_convits]);
    case (escby_shwapc_pd2b_max_num_args-6):
        args.maxits = boost::lexical_cast<scby_uint>(argv[escby_shwapc_pd2b_maxits]);
    case (escby_shwapc_pd2b_max_num_args-7):
        args.q = boost::lexical_cast<scby_real>(argv[escby_shwapc_pd2b_q]);
    case (escby_shwapc_pd2b_max_num_args-8):
        args.decay_window = boost::lexical_cast<scby_uint>(argv[escby_shwapc_pd2b_decay_window]);
    case (escby_shwapc_pd2b_max_num_args-9):
        args.max_exemplars_thresh = boost::lexical_cast<scby_uint>(argv[escby_shwapc_pd2b_max_exemplars_thresh]);
    case (escby_shwapc_pd2b_max_num_args-10):
        args.max_unlabeled_thresh = boost::lexical_cast<scby_uint>(argv[escby_shwapc_pd2b_max_unlabeled_thresh]);
    case (escby_shwapc_pd2b_max_num_args-11):
        args.ex_assoc_heur_scale = boost::lexical_cast<scby_real>(argv[escby_shwapc_pd2b_ex_assoc_heur_scale]);
    case (escby_shwapc_pd2b_max_num_args-12):
        args.ex_assoc_heur =
            (escby_shwap_ex_assoc_heur)boost::lexical_cast<scby_uint>(argv[escby_shwapc_pd2b_ex_assoc_heur]);
    case (escby_shwapc_pd2b_max_num_args-13):
        args.init_size = boost::lexical_cast<scby_uint>(argv[escby_shwapc_pd2b_init_size]);
    }

    scby_shwap_pdist2_bin(args);
    return 0;
}

//******************************************************
// APP registration
//******************************************************

// application info structure
struct scby_app_info_t {
    const char *m_name;
    int(*m_fp_main)(const int, char **);
};

// @return true if command line says we should run parameter commandlet, false otherwise
inline int scby_should_run_app(char **argv, const char *pstr_app_name) {
    scby_assert(argv);
    return case_insenstive_cmp(argv[1], pstr_app_name);
}

// register applications here
const struct scby_app_info_t G_SCBY_APPS[] = {
    {"convert_sym_matrix_lower_tri_tsv_to_bin", &scby_convert_sym_matrix_lower_tri_tsv_to_bin_main},
    {"convert_sym_matrix_bin_to_lower_tri_tsv", &scby_convert_sym_matrix_bin_to_lower_tri_tsv_main},
    {"get_sorted_k_dists",                      &scby_get_sorted_k_dists_main},
    {"dbscan",                                  &scby_dbscan_main},
    {"get_dbs_cluster_freq_counts",             &scby_get_dbs_cluster_freq_counts_main},
    {"convert_matrix_csv_to_bin",               &scby_convert_matrix_csv_to_bin_main},
    {"convert_matrix_bin_to_csv",               &scby_convert_matrix_bin_to_csv_main},
    {"crop",                                    &scby_crop_main},
    {"pca_reduce",                              &scby_pca_reduce_main},
    {"compute_rmsd",                            &scby_compute_rmsd_main},
    {"compute_rmsd_mt",                         &scby_compute_rmsd_mt_main},
    {"k_means_silh",                            &scby_k_means_silh_main},
    {"apclust",                                 &scby_apcluster_main},
    {"shwap",                                   &scby_shwap_main}
};

//******************************************************
// main
//******************************************************

int main(int argc, char **argv) {
    const int num_apps = sizeof(G_SCBY_APPS) / sizeof(scby_app_info_t);

    // Determine which app to run
    if (argc >= 2) {
        for (int iapp = 0; iapp < num_apps; ++iapp) {
            if (scby_should_run_app(argv, G_SCBY_APPS[iapp].m_name)) {
                return (*(G_SCBY_APPS[iapp].m_fp_main))(argc, argv);
            }
        }
    }

    return 0;
}
