/**
 * @author Alan Perez-Rathke
 * @date July 25, 2014
 *
 * DBSCAN implementation
 */

#ifndef SCBYDBSCAN_H
#define SCBYDBSCAN_H

#include "scbybuild.h"
#include "scbyassert.h"
#include "scbytypes.h"
#include <vector>

// find all neighboring nodes within distance 'eps' of 'inode'
template <typename t_matrix>
void scby_dbscan_distance_query(
    scbybitset& out_neighbor_nodes,
    const typename t_matrix::size_type inode,
    const t_matrix& dist_matrix,
    const scby_real eps) {

    const typename t_matrix::size_type num_nodes = dist_matrix.size1();
    out_neighbor_nodes.reset(); // make sure we have no neighbors to start with
    scby_assert(!out_neighbor_nodes.any());
    scby_assert(out_neighbor_nodes.size() == num_nodes);
    for (typename t_matrix::size_type ineighbor = 0; ineighbor < num_nodes; ++ineighbor) {
        out_neighbor_nodes.set(ineighbor, (dist_matrix(inode, ineighbor) <= eps));
    }
}

// based on psuedo-code from http://en.wikipedia.org/wiki/DBSCAN
// from ester et al, section 4.2:
// min_pts = 4 is recommended parameter setting for any set of points > 4
// http://www.it.uu.se/edu/course/homepage/infoutv/ht11/literature_material/KDD-96.final.frame.pdf
template <typename t_matrix>
void scby_dbscan(
    std::vector<typename t_matrix::size_type>& out_clusters,
    const t_matrix& dist_matrix,
    const scby_real eps,
    const size_t min_pts = 4) {

    // assuming symmetric distance matrix
    scby_assert(dist_matrix.size1() == dist_matrix.size2());
    scby_assert(dist_matrix.size1() > 0);

    enum {
        eClusterId_NOISE = 0,
        eClusterId_UNCLASSIFIED,
        eClusterId_FIRST_REAL_ID
    };

    const typename t_matrix::size_type num_nodes = dist_matrix.size1();

    out_clusters.resize(num_nodes, eClusterId_UNCLASSIFIED);
    assert(out_clusters[0] == eClusterId_UNCLASSIFIED);
    typename t_matrix::size_type cluster_id = eClusterId_FIRST_REAL_ID;

    scbybitset b_visited(num_nodes);
    scby_assert(!b_visited.any());

    scbybitset neighbor_nodes(num_nodes);
    scby_assert(!neighbor_nodes.any());

    scbybitset secondary_nodes(num_nodes);
    scby_assert(!secondary_nodes.any());

    for (typename t_matrix::size_type inode = 0; inode < num_nodes; ++inode) {

        // skip to next node if we've already visited this node
        if (b_visited.test(inode)) {
            continue;
        }

        // mark node as visited
        b_visited.set(inode, true);

        // find neighbors of node
        scby_dbscan_distance_query(neighbor_nodes, inode, dist_matrix, eps);

        // check if this is a cluster by seeing if it has at least min neighbors, if so -> expand it!
        if (neighbor_nodes.count() >= min_pts) {
            out_clusters[inode] = cluster_id++;
            const typename t_matrix::size_type inode_cluster_id = out_clusters[inode];

            // find all nodes that belong to this cluster
            while (neighbor_nodes.any()) {
                for (scbybitset::size_type ineighbor = neighbor_nodes.find_first();
                        ineighbor != neighbor_nodes.npos;
                        ineighbor = neighbor_nodes.find_next(ineighbor)) {

                    // remove this neighbor so that we can terminate outer while loop
                    neighbor_nodes.set(ineighbor, false);

                    // if neighbor does not already have an assigned cluster, assign it to our current cluster
                    if ((out_clusters[ineighbor] == eClusterId_UNCLASSIFIED) ||
                            (out_clusters[ineighbor] == eClusterId_NOISE)) {
                        out_clusters[ineighbor] = inode_cluster_id;
                    }

                    // if we haven't already visited this neighbor, see if we should visit its neighbors and add them to our cluster
                    if (!b_visited.test(ineighbor)) {
                        b_visited.set(ineighbor, true);
                        scby_dbscan_distance_query(secondary_nodes, ineighbor, dist_matrix, eps);
                        if (secondary_nodes.count() >= min_pts) {
                            neighbor_nodes |= secondary_nodes;
                        }
                    }

                } // end for loop over set of neighboring nodes
            } // end while loop over all nodes in this cluster

        } // end of cluster expansion
        else {
            // not enough nearby nodes, mark as noise
            out_clusters[inode] = eClusterId_NOISE;
        }
    } // end iteration over node
}

#endif // SCBYDBSCAN_H
