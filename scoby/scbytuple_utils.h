/**
 * @author Alan Perez-Rathke
 * @date July 25, 2014
 */

#ifndef SCBYTUPLE_UTILS_H
#define SCBYTUPLE_UTILS_H

#include "scbybuild.h"
#include <tuple>

// from http://cpplove.blogspot.com/2012/07/printing-tuples.html

template<std::size_t> struct scby_tuple_int_t {};

template <class tuple_t, size_t pos_t>
std::ostream& scby_tuple_print_csv(std::ostream& out, const tuple_t& t, scby_tuple_int_t<pos_t>) {
    out << std::get< std::tuple_size<tuple_t>::value - pos_t >(t) << ',';
    return scby_tuple_print_csv(out, t, scby_tuple_int_t<pos_t - 1>());
}

template <class tuple_t>
std::ostream& scby_tuple_print_csv(std::ostream& out, const tuple_t& t, scby_tuple_int_t<1>) {
    return out << std::get<std::tuple_size<tuple_t>::value - 1>(t);
}

template <class... args_t>
std::ostream& operator<<(std::ostream& out, const std::tuple<args_t...>& t) {
    return scby_tuple_print_csv(out, t, scby_tuple_int_t<sizeof...(args_t)>());
}

template <typename t>
std::ostream& operator<<(std::ostream& out, const std::vector<t>& v) {
    for (size_t i = 0; i < (v.size() - 1); ++i) {
        out << v[i] << ",";
    }
    if (!v.empty()) {
        out << v.back();
    }
    return out;
}

#endif // SCBYTUPLE_UTILS_H
