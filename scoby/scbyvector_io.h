/**
 * @author Alan Perez-Rathke
 * @date July 25, 2014
 */

#ifndef SCBYVECTOR_IO_H
#define SCBYVECTOR_IO_H

#include "scbybuild.h"
#include "scbyassert.h"
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <fstream>

/**
 * utility function for writing a vector to disk
 */
template <typename vec_t>
void scby_vector_write(const vec_t& vec, const char* path) {
    scby_assert(NULL != path);

    std::ofstream out;
    out.open(path);

    for (size_t i = 0; i < vec.size(); ++i) {
        out << vec[i] << std::endl;
    }

    out.close();
}

/**
 * utility function for writing a vector of vectors to disk (csv format)
 */
template <typename vec_vec_t>
void scby_vector_write_2d(const vec_vec_t& vec_vec, const char* path) {
    scby_assert(NULL != path);

    std::ofstream out;
    out.open(path);

    for (size_t i = 0; i < vec_vec.size(); ++i) {
        if (!vec_vec[i].empty()) {
            out << vec_vec[i][0];
            for (size_t j = 1; j < vec_vec[i].size(); ++j) {
                out << "," << vec_vec[i][j];
            }
        }
        out << std::endl;
    }

    out.close();
}

/**
 * utility function for reading a vector from  disk
 */
template <typename t>
void scby_vector_read(std::vector<t>& out_vec, const char* path) {
    scby_assert(NULL != path);

    std::ifstream in;
    in.open(path);

    std::string line;
    out_vec.clear();
    while (std::getline(in, line)) {
        // ignore whitespace, skip empty lines
        boost::trim(line);
        if (line.empty()) {
            continue;
        }

        out_vec.push_back(boost::lexical_cast<t>(line));
    }

    in.close();
}

#endif // SCBY_VECTOR_IO_H
