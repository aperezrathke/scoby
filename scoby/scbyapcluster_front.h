/**
 * Front-facing utilities for clustering with affinity propagation
 *
 * Based on affinity propagation utility within APCluster R package
 *
 * Frey, Brendan J., and Delbert Dueck. "Clustering by passing messages between data points."
 * science 315, no. 5814 (2007): 972-976.
 *
 * Bodenhofer, Ulrich, Andreas Kothmeier, and Sepp Hochreiter.
 * "APCluster: an R package for affinity propagation clustering." Bioinformatics 27, no. 17 (2011): 2463-2464.
 */

#ifndef SCBYAPCLUSTER_FRONT_H
#define SCBYAPCLUSTER_FRONT_H

//****************************************************************************
// Includes
//****************************************************************************

#include "scbybuild.h"
#include "scbytypes.h"
#include "scbyapcluster.h"

//****************************************************************************
// Arguments
//****************************************************************************

// Enumerated user arguments for use in argv, argc main
enum escby_apcluster_bin_args {
    escby_apcb_exe_name=0,
    escby_apcb_app_name,
    escby_apcb_in_matrix_path,
    escby_apcb_n_dims,
    escby_apcb_out_samples_to_clusters_path,
    escby_apcb_out_clusters_to_samples_path,
    escby_apcb_out_exemplars_path,
    escby_apcb_max_num_args
};

// Arguments to utility
struct scby_apcluster_bin_args_t {
    // Path to input matrix
    const char* in_matrix_path;
    // Number of dimensions
    scby_uint n_dims;
    // Path to write sample id to cluster id mapping in csv format
    const char* out_samples_to_clusters_path;
    // Path to write cluster id to set of sample ids in csv format
    const char* out_clusters_to_samples_path;
    // Path to write list of exemplar ids
    const char* out_exemplars_path;
};

//****************************************************************************
// Extern(s)
//****************************************************************************

/**
 * Utility exports results from affinity propagation
 */
extern void scby_apcluster_export_results(
    const scby_index_vec_t &samples_to_clusters,
    const scby_ap_c2s_map_t &clusters_to_samples,
    const scby_index_vec_t exemplars,
    const char* out_samples_to_clusters_path,
    const char* out_clusters_to_samples_path,
    const char* out_exemplars_path
);

/**
 * Given input samples with coordinates in columns, will perform
 * stock apcluster routine and write results to disk
 */
extern void scby_apcluster_bin(const scby_apcluster_bin_args_t &args);

#endif // SCBYAPCLUSTER_FRONT_H
