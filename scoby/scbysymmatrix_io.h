/**
 * @author Alan Perez-Rathke
 * @date July 25, 2014
 */

#ifndef SCBYSYMMATRIX_IO_H
#define SCBYSYMMATRIX_IO_H

#include "scbybuild.h"
#include "scbyassert.h"
#include "scbytypes.h"

// reads a lower, symmetric matrix in tsv format from disk
// @param out reference to scby_sym_matrix that will be read from file
// @param path - character string with path to file
// @return 1 if matrix successfully loaded into 'out', 0 otherwise
extern int scby_sym_matrix_read_lower_tri_tsv(scby_sym_matrix &outm, const char *path);

// writes tsv representation of lower, symmetric matrix to disk
// @param m - reference to scby_sym_l_matrix that will be written to plain text
// @param path - character string with path to output file
// note: all columns above diagonal are written as 0's
extern void scby_sym_matrix_write_lower_tri_tsv(const scby_sym_matrix &m, const char *path);

// writes tsv representation of symmetric matrix to disk (both upper and lower tri)
// @param m - reference to scby_sym_matrix that will be written to plain text
// @param path character string with path to output file
extern void scby_sym_matrix_write_tsv(const scby_sym_matrix &m, const char *path);

// reads a lower, symmetric matrix in csv format from disk
// @param out reference to scby_sym_matrix that will be read from file
// @param path - character string with path to file
// @return 1 if matrix successfully loaded into 'out', 0 otherwise
extern int scby_sym_matrix_read_lower_tri_csv(scby_sym_matrix &outm, const char *path);

// writes csv representation of lower, symmetric matrix to disk
// @param m - reference to scby_sym_l_matrix that will be written to plain text
// @param path character string with path to output file
// note: all columns above diagonal are written as 0's
extern void scby_sym_matrix_write_lower_tri_csv(const scby_sym_matrix &m, const char *path);

// writes csv representation of symmetric matrix to disk (both upper and lower tri)
// @param m - reference to scby_sym_matrix that will be written to plain text
// @param path - character string with path to output file
extern void scby_sym_matrix_write_csv(const scby_sym_matrix &m, const char *path);

// writes binary representation of symmetric matrix to disk
extern void scby_sym_matrix_write_bin(const scby_sym_matrix &m, const char *path);

// reads binary representation of symmetric matrix from disk
extern void scby_sym_matrix_read_bin(scby_sym_matrix &out_m, const char *path);

#endif // SCBYSYMMATRIX_IO_H
