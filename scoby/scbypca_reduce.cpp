//****************************************************************************
// scbypca_reduce.cpp
//****************************************************************************

/**
 * @author Alan Perez-Rathke
 * @date December 1st, 2015
 *
 * Utility for reducing dimensionality of an input matrix using principal
 * component analysis:
 *
 * https://en.wikipedia.org/wiki/Principal_component_analysis
 */

#include "scbybuild.h"
#include "scbyassert.h"
#include "scbypca_reduce.h"
#include "scbyarmadillo.h"

/**
 * Utilities
 */
namespace {

    /**
     * Display arguments to user
     */
    void print_args(const scby_pca_reduce_args_t &args) {
        std::cout << "PCA reduce util args:\n";
        // The percentage of variance explained to keep in (0.0, 1.0]
        // = sum(eigenvalues 1 to k) / sum(eigenvalues 1 to n)
        // where k <= n and n is total number of eigenvalues
        std::cout << "\tPercent to keep: " << args.perc_keep << std::endl;
        // Each row of input matrix is an observation and each column is a variable,
        // If this is the case, then pass in '1', else pass in '0'
        // (ie. if samples are in rows - pass '1', else pass '0')
        std::cout << "\tObservations in rows?: " << args.obs_in_rows << std::endl;
        // number of base dimensions of each observation
        std::cout << "\tNumber of base dimensions: " << args.n_dims << std::endl;
        // Path to binary input matrix
        std::cout << "\tInput matrix path: " << args.in_matrix_path << std::endl;
        // Path to write reduced matrix
        std::cout << "\tOutput matrix path: " << args.out_matrix_path << std::endl;
    }

    /**
     * @return TRUE if arguments are proper, FALSE o/w
     */
    bool check_args(const scby_pca_reduce_args_t &args) {
        if ((args.perc_keep <= ((scby_real)0.0)) || (args.perc_keep >= ((scby_real)1.0))) {
            std::cout << "Error: percent to keep must be in (0.0, 1.0].\n";
            return false;
        }

        if (args.n_dims < 1) {
            std::cout << "Error: number of base dimensions must be positive.\n";
            return false;
        }

        if (args.in_matrix_path == NULL) {
            std::cout << "Error: input matrix path is invalid.\n";
            return false;
        }

        if (args.out_matrix_path == NULL) {
            std::cout << "Error: output matrix path is invalid.\n";
            return false;
        }

        return true;
    }

    /**
     * Reads input matrix and internally resizes such that observations are in rows
     */
    bool load_and_format_input_matrix(scby_matrix &mat, const scby_pca_reduce_args_t &args) {
        return scby_load_mat_bin_and_reshape_col_maj(
                mat,
                args.in_matrix_path,
                args.n_dims,
                args.obs_in_rows);
    }

} // end of helper namespace

/**
 * Implementation based on MLPack library
 * Note: reduced mat and mat can be passed in as same variable
 * @param reduced_mat - the output dim reduced matrix (column major)
 * @param mat - column major data matrix - samples are in columns, variables are in rows
 *  - matrix will be transformed to PCA space and rows contributing past variance threshold
 *  (as specified by perc_keep) will be removed - this transformed version is stored in trans_mat
 * @param perc_keep - the percent variance to keep in (0,1]
 * @return actual amount of variance retained, which will always be greater than or equal to perc_keep.
 */
double scby_pca_reduce(
        scby_matrix &reduced_mat,
        const scby_matrix &mat,
        const double perc_keep) {

    // To compute the covariance matrix - we want (mat - mean) * trans(mat - mean)
    // = centered_mat * trans(centered_mat)
    // So, we must center the input data matrix

    // Get the mean of the elements in each row.
    const scby_vec_col row_means = scby_matrix_utils::sum(mat, 1 /*sum across rows*/) / mat.n_cols;

    // Now, center matrix by subtracting row means
    const scby_matrix centered_mat =
        mat - scby_matrix_utils::repmat(row_means, 1 /*num_copies_per_row*/, mat.n_cols /*num_copies_per_col*/);

    // Note: in MLPack implementation, there is also an option to scale data by std dev,
    // but we are assuming homogenous coordinate data all in the same scale, so we will avoid this

    // Perform singular value decomposition. Recall, singular values are the square roots of the
    // eigenvalues of the matrix X * trans(X) and trans(X) * X. The decomposition produces matrices
    // U, D, V such that X = U * D * trans(V) where U is matrix of ["left"] eigenvectors of X * trans(X),
    // V is matrix of ["right"] eigenvectors of trans(X) * X and D is diagonal matrix of singular values.

    // Since we are only interested in the covariance among the rows = X * trans(X), we only need the left eigenvectors
    // in the U matrix. The corresponding eigenvalues can be obtained by squaring the singular values in D and
    // dividing by (n-1) because recall that sample variance is divided by (n-1).

    // The left eigenvectors. This matrix is the U matrix in expression X = U*D*trans(V)
    // We want to retain these eigenvectors and project to this eigenspace.
    // Recall - these are the eigenvectors of centered_mat * trans(centered_mat)
    scby_matrix left_eigen_vecs;

    // This matrix will store the right singular vectors; we do not need them. This matrix
    // is the V matrix in expression X = U*D*trans(V)
    // Note: the right singular vectors are the eigenvectors of trans(centered_mat) * centered_mat
    scby_matrix right_eigen_vecs;

    // Column vector for storing singular values (representation of matrix D in X = U*D*trans(V)).
    // These will later be squared and divided by (n-1) in order to convert to eigenvalues
    // of covariance matrix centered_mat * trans(centered_mat).
    scby_vec_col eigen_vals;

    // Use the economical singular value decomposition if the columns are much larger than the rows.
    if (mat.n_rows < mat.n_cols) {
        // Do economical singular value decomposition and compute only the left
        // singular vectors. Also use divide-and-conquer method 'dc' which is supposed
        // to be faster for larger matrices
        scby_matrix_utils::svd_econ(left_eigen_vecs,
                                    eigen_vals,
                                    right_eigen_vecs,
                                    centered_mat,
                                    "left",
                                    "dc");
    }
    else {
        scby_matrix_utils::svd(left_eigen_vecs, eigen_vals, right_eigen_vecs, centered_mat, "dc");
    }

    // Now we must square the singular values to get the eigenvalues.
    // In addition we must divide by the number of points, because the covariance
    // matrix is X * X' / (N - 1).
    eigen_vals %= eigen_vals / (mat.n_cols - 1);

    // Project samples to principal components
    reduced_mat = scby_matrix_utils::trans(left_eigen_vecs) * centered_mat;

    // Calculate the dimensions we should keep.
    sbcy_mat_sz_t new_dim = 0;
    scby_real var_sum = ((scby_real)0.0);
    eigen_vals /= scby_matrix_utils::sum(eigen_vals); // Normalize eigenvalues.
    while ((var_sum < perc_keep) && (new_dim < eigen_vals.n_elem)) {
        var_sum += eigen_vals[new_dim];
        ++new_dim;
    }

    // Trim any unwanted dimensions
    if (new_dim < eigen_vals.n_elem) {
        reduced_mat.shed_rows(new_dim, (reduced_mat.n_rows - 1));
    }

    return var_sum;
}

/**
 * Performs PCA-based dimensionality reduction
 */
void scby_pca_reduce_bin(const scby_pca_reduce_args_t &args) {
    // Display arguments to user
    print_args(args);

    // Validate arguments
    if (!check_args(args)) {
        return;
    }

    // Load input matrix
    scby_matrix mat;
    if (!load_and_format_input_matrix(mat, args)) {
        return;
    }

    // Reduce dimensionality
    const std::clock_t pca_start_time = std::clock();
    const double var_sum =
        scby_pca_reduce(mat, mat, args.perc_keep);
    const std::clock_t pca_end_time = std::clock();
    const scby_real duration = (pca_end_time - pca_start_time) / ((scby_real)CLOCKS_PER_SEC);

    std::cout << "Finished PCA reduction in " << duration << " seconds\n";
    std::cout << "\tactual variance retained (0,1]: " << var_sum << std::endl;

    // Export reduced matrix
    scby_write_matrix_bin(mat, args.out_matrix_path);
}
