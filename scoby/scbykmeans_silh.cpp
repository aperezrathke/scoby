//****************************************************************************
// scbykmeans_silh.cpp
//
// Uses method of silhouettes to determine the number of "natural" clusters
// https://en.wikipedia.org/wiki/Silhouette_(clustering)
// Peter J. Rousseeuw (1987). "Silhouettes: a Graphical Aid to the Interpretation
// and Validation of Cluster Analysis". Computational and Applied Mathematics
// 20: 53–65. doi:10.1016/0377-0427(87)90125-7
//****************************************************************************

//****************************************************************************
// Includes
//****************************************************************************

#include "scbykmeans_silh.h"
#include "scbyassert.h"
#include "scbyrand.h"
#include "scbyvector_io.h"

#include <ctime>
#include <unordered_map>
#include <map>

//****************************************************************************
// Helper namespace
//****************************************************************************

namespace {

    // Core k-means data
    struct scby_kmeans_silh_core_t {
        scby_matrix positions_mat;
        // n_structs = positions_mat.n_cols
        scby_uint n_structs;
        // inv_n_structs = 1/n_structs
        scby_real inv_n_structs;
    };

    // Structure containing buffers for centroids, cluster assignments, and silhouette scores
    struct scby_kmeans_buffs_t {
        // double buffers for storing centroid coordinates
        // warning: view should be dependent on k
        scby_matrix cent_buffs[2];
        // buffer for storing cluster assignments for each struct
        std::vector<scby_uint> clusters;
        // the number of structs assigned to each cluster
        // warning: view should be dependent on k
        scby_vec_col cluster_counts;
        // buffer for storing silhouette scores for each struct
        scby_vec_col silh_scores;
        // buffer for storing mean squared distances for each member of the cluster
        // to that cluster's [updated] centroid
        // warning: view should be dependent on k
        scby_vec_col cluster_mean_sqdists;
    };

    // History from k-means runs including grand average silhouette scores for each k-value,
    // top grand average silhouettes score, top-scoring k-value, top-scoring centroid positions,
    // and top-scoring cluster assignments
    struct scby_kmeans_hist_t {
        // top-scoring k-value (number of clusters)
        scby_uint top_k;
        // corrected top-scoring k-value with empty clusters removed
        scby_uint top_corrected_k;
        // top-scoring grand average silhouette value
        scby_real top_silh_grand_avg_score;
        // delta between centroid iterations
        scby_real top_delta;
        // number of k-means iterations completed for top-scoring k-value
        scby_uint top_n_iters;
        // map from k-value to grand average silhouette score
        std::unordered_map<scby_uint, scby_real> grand_avg_silh_hist;
        // final centroid positions for top-scoring k
        // warning: view should be dependent on k
        scby_matrix top_cents;
        // cluster assignments for each struct for top-scoring k
        std::vector<scby_uint> top_clusters;
        // number of structs assigned to each cluster for top-scoring k
        // warning: view should be dependent on k
        scby_vec_col top_cluster_counts;
        // per-structure silhouette scores for top-scoring k
        scby_vec_col top_silh_scores;
    };

    // @return TRUE if arguments are proper, FALSE o/w
    bool check_args(const scby_kmeans_silh_args_t &args) {

        if (args.min_k < 1) {
            std::cout << "Error: min_k must be positive.\n";
            return false;
        }

        if (args.max_k < args.min_k) {
            std::cout << "Error: max_k must be greater than or equal to min_k.\n";
            return false;
        }

        // require positive cluster increment
        if (args.k_inc < 1) {
            std::cout << "Error: k_inc must be positive.\n";
            return false;
        }

        if (args.k_report_interv < 1) {
            std::cout << "Error: report interval must be positive\n";
            return false;
        }

        // require positive random seed count
        if (args.n_rand_restarts < 1) {
            std::cout << "Error: n_rand_restarts must be positive.\n";
            return false;
        }

        // require positive number of dimensions
        if (args.n_dims < 1) {
            std::cout << "Error: must have at least 1 dimension\n";
            return false;
        }

        // verify delta is non-negative
        if (args.delta < ((scby_real)0.0)) {
            std::cout << "Error: delta termination threshold must be >= 0\n";
            return false;
        }

        // verify max_iters_per_run is positive
        if (args.max_iters_per_run < 1)
        {
            std::cout << "Error: max iterations must be greater than 0\n";
            return false;
        }

        if (NULL == args.in_bin_coords_path) {
            std::cout << "Error: input binary coords path is invalid.\n";
            return false;
        }

        if (NULL == args.out_clusters_path) {
            std::cout << "Error: output cluster assignments path is invalid.\n";
            return false;
        }

        // everything seems okay
        return true;
    }

    void print_args(const scby_kmeans_silh_args_t &args) {
        std::cout << "Parameters:\n";
        std::cout << "\tmin_k: " << args.min_k << std::endl;
        std::cout << "\tmax_k: " << args.max_k << std::endl;
        std::cout << "\tk_inc: " << args.k_inc << std::endl;
        std::cout << "\tk_report_interv: " << args.k_report_interv << std::endl;
        std::cout << "\tn_rand_restarts: " << args.n_rand_restarts << std::endl;
        std::cout << "\tn_dims: " << args.n_dims << std::endl;
        std::cout << "\tdelta: " << args.delta << std::endl;
        std::cout << "\tmax_iters_per_run: " << args.max_iters_per_run << std::endl;
        std::cout << "\tin_bin_coords_path: " << args.in_bin_coords_path << std::endl;
        std::cout << "\tout_clusters_path: " << args.out_clusters_path << std::endl;
        std::cout << "\tout_centroids_path: " << ((args.out_centroids_path != NULL) ? args.out_centroids_path : "null") << std::endl;
        std::cout << "\tout_cluster_counts_path: " << ((args.out_cluster_counts_path != NULL) ? args.out_cluster_counts_path : "null") << std::endl;
        std::cout << "\tout_silh_scores_path: " << ((args.out_silh_scores_path != NULL) ? args.out_silh_scores_path : "null") << std::endl;
        std::cout << "\tout_silh_k_scores_path: " << ((args.out_silh_k_scores_path != NULL) ? args.out_silh_k_scores_path : "null") << std::endl;
    }

    // Makes sure that [lower or upper] bound for number of clusters k is no greater than
    // total number of structures that are being clustered.
    void check_k_bound(scby_uint &k_bound, const char* desc, const scby_uint n_structs) {
        if (k_bound > n_structs) {
            std::cout << "Warning: " << desc << " = " << k_bound
                      << " is greater than number of loaded structures: " << n_structs << std::endl;
            k_bound = n_structs;
            std::cout << "\tSetting " << desc << " = " << k_bound << std::endl;
        }
    }

    bool load_core(scby_kmeans_silh_core_t &core, scby_kmeans_silh_args_t &args) {
        if (!scby_load_mat_bin_and_reshape_col_maj(core.positions_mat, args.in_bin_coords_path, args.n_dims)) {
            return false;
        }

        // verify input coords matrix has enough nodes
        scby_assert(core.positions_mat.n_rows % args.n_dims == 0);
        const scby_uint n_nodes = core.positions_mat.n_rows / args.n_dims;
        if (n_nodes < 1)
        {
            std::cout << "Error: input coordinates matrix must have at least 1 node\n";
            return false;
        }

        // verify we have enough structures to perform a clustering
        core.n_structs = core.positions_mat.n_cols;
        if (core.n_structs < 3) {
            std::cout << "Error: must have at least 3 structures to perform clustering.\n";
            return false;
        }
        core.inv_n_structs = ((scby_real)1.0) / ((scby_real)core.n_structs);

        // ensure k ranges do not exceed number of structures
        check_k_bound(args.min_k, "min_k", core.n_structs);
        check_k_bound(args.max_k, "max_k", core.n_structs);

        // everything seems to have loaded okay
        return true;
    }

    // slight hack - convenience method that casts away constness of the args
    bool load_core_wrapper(scby_kmeans_silh_core_t &core, const scby_kmeans_silh_args_t &args) {
        return load_core(core, const_cast<scby_kmeans_silh_args_t&>(args));
    }

    // DRY principle - avoid duplicating details of centroid buffer allocation
    void prealloc_cent_buff(scby_matrix &cent_buff, const scby_uint max_k, const scby_kmeans_silh_core_t &core) {
        assert(max_k > 1);
        cent_buff.zeros(core.positions_mat.n_rows, max_k);
    }

    // Pre-allocate centroid buffers, cluster assignments buffer, and silhouette scores buffer
    void prealloc_buffs(scby_kmeans_buffs_t &buffs, const scby_uint max_k, const scby_kmeans_silh_core_t &core) {
        assert(core.n_structs == core.positions_mat.n_cols);
        assert(buffs.clusters.size() == 0);
        assert(buffs.silh_scores.size() == 0);
        prealloc_cent_buff(buffs.cent_buffs[0], max_k, core);
        prealloc_cent_buff(buffs.cent_buffs[1], max_k, core);
        buffs.clusters.resize(core.n_structs, 0);
        buffs.cluster_counts.zeros(max_k);
        buffs.silh_scores.zeros(core.n_structs);
        buffs.cluster_mean_sqdists.zeros(max_k);
    }

    // Pre-allocate history buffers (like top scoring cluster assignments, etc.)
    void prealloc_hist(scby_kmeans_hist_t &hist, const scby_kmeans_silh_core_t &core, const scby_kmeans_silh_args_t &args) {
        hist.top_k = 0;
        hist.top_corrected_k = 0;
        // silhouette scores are between [-1,1] so this ensures that first score will overwrite initial value
        hist.top_silh_grand_avg_score = ((scby_real)-2.0);
        hist.top_delta = std::numeric_limits<scby_real>::max();
        hist.top_n_iters = 0;
        hist.grand_avg_silh_hist.reserve(args.max_k - args.min_k + 1);
        prealloc_cent_buff(hist.top_cents, args.max_k, core);
        hist.top_clusters.resize(core.n_structs, 0);
        hist.top_cluster_counts.zeros(args.max_k);
        hist.top_silh_scores.set_size(core.n_structs);
        hist.top_silh_scores.fill(((scby_real)-2.0));
    }

    // randomly picks num_centroids random samples to serve as the starting seeds for a k-means run
    // @param centroids - pre-allocated positions_mat.n_rows x num_centroids
    void init_cents_rand(scby_matrix &cents,
                         const scby_uint num_cents,
                         const scby_kmeans_silh_core_t &core,
                         scby_rand &rng) {
        scby_assert(cents.n_rows == core.positions_mat.n_rows);
        scby_assert(cents.n_cols == num_cents);
        scby_assert(num_cents > 0);
        for (scby_uint i_cent=0; i_cent < num_cents; ++i_cent) {
            // select random structure
            const scby_uint i_struct = rng.unif_int<scby_uint>(0, core.n_structs - 1);
            scby_assert_bounds(i_struct, 0, core.n_structs);
            // assign to centroid
            cents.unsafe_col(i_cent) = core.positions_mat.unsafe_col(i_struct);
        }
    }

    // removes empty clusters and renames remaining clusters so that
    // identifiers are contiguous
    void remove_empty_clusters(scby_kmeans_hist_t &hist) {
        scby_assert_bounds(hist.top_corrected_k, 1, hist.top_k + 1);
        // Nothing needs to be done if no empty clusters exist
        if (hist.top_corrected_k == hist.top_k) {
            return;
        }

        // Obtain contiguous mapping
        scby_uint n_clusts = 0;
        std::unordered_map<scby_uint, scby_uint> cluster_id_map;
        cluster_id_map.reserve(hist.top_k);
        scby_assert(hist.top_k == hist.top_cluster_counts.n_elem);
        scby_assert(hist.top_cluster_counts.n_elem == hist.top_cents.n_cols);
        for (scby_uint i_clust=0; i_clust<hist.top_k; ++i_clust) {
            const scby_real cluster_count = hist.top_cluster_counts.at(i_clust);
            // @TODO - handle any precision issues
            if (cluster_count >= 1.0) {
                cluster_id_map[i_clust] = n_clusts++;
            }
        }
        scby_assert(n_clusts == hist.top_corrected_k);

        // Remove empty clusters
        // this should defragment counts array so all non-zero elements are at front
        // but also preserve ordering
        for (scby_uint i_clust=0; i_clust<hist.top_cluster_counts.n_elem; ++i_clust) {
            const scby_real cluster_count = hist.top_cluster_counts.at(i_clust);
            if (cluster_count >= 1.0) {
                const auto remap_itr = cluster_id_map.find(i_clust);
                scby_assert(remap_itr != cluster_id_map.end());
                const scby_uint new_cluster_id = remap_itr->second;
                scby_assert(new_cluster_id <= i_clust);
                // defragment cluster counts
                hist.top_cluster_counts.at(new_cluster_id) = cluster_count;
                // defragment centroids
                hist.top_cents.unsafe_col(new_cluster_id) = hist.top_cents.unsafe_col(i_clust);
            }
        }
        // trim ends
        hist.top_cluster_counts.shed_rows(n_clusts, hist.top_k - 1);
        hist.top_cents.shed_cols(n_clusts, hist.top_k - 1);

        // Remap clusters
        hist.top_k = n_clusts;
        const auto n_structs = hist.top_clusters.size();
        for (scby_uint i_struct=0; i_struct<n_structs; ++i_struct) {
            const auto remap_itr = cluster_id_map.find(hist.top_clusters[i_struct]);
            scby_assert(remap_itr != cluster_id_map.end());
            const scby_uint new_cluster_id = remap_itr->second;
            hist.top_clusters[i_struct] = new_cluster_id;
        }
    }

    void calc_silh_scores(scby_real &silh_grand_avg_score,
                          scby_uint &silh_num_nonempty_clusters,
                          scby_vec_col &cluster_mean_sqdists,
                          scby_vec_col &sqdist_buff,
                          scby_kmeans_buffs_t &buffs,
                          const scby_uint k,
                          const scby_matrix &cent_buff,
                          const scby_vec_col &cluster_counts,
                          const scby_kmeans_silh_core_t &core) {
        //////////////////////////////////////////////////////
        // compute silhouette scores
        //////////////////////////////////////////////////////

        // note: (1/n_i) * sum(sqdist(X_i - a) ) = (1/n_i) * sum(sqdist(X_i, X_bar)) + sqdist(X_bar, a)
        // where X_bar is centroid of i-th cluster, n_i is number of samples belonging to i-th cluster,
        // X_i is a sample in the in the i-th cluster, a is any sample (typically not in i-th cluster),
        // and sqdist(q,p) = transpose(q-p) * (q-p), this only holds for Euclidean distance

        // compute all within-cluster variances: (1/n_i) * sum(sqdist(X_i, X_bar)):

        // compute mean squared within cluster distance - WARNING NOT THREAD SAFE
        memset(cluster_mean_sqdists.memptr(), 0, buffs.cluster_mean_sqdists.n_elem * sizeof(scby_real));
        for (scby_uint i_struct=0; i_struct<core.n_structs; ++i_struct) {
            scby_assert_bounds(i_struct, 0, buffs.clusters.size());
            const scby_uint cluster_id = buffs.clusters[i_struct];
            scby_assert_bounds(cluster_id, 0, cent_buff.n_cols);
            scby_assert_bounds(cluster_id, 0, cluster_mean_sqdists.n_elem);
            SCBY_INC_BY_DIST2(cluster_mean_sqdists.at(cluster_id),
                              sqdist_buff,
                              core.positions_mat.unsafe_col(i_struct),
                              cent_buff.unsafe_col(cluster_id));
            scby_assert(cluster_mean_sqdists(cluster_id) >= scby_real(0.0));
        }

        // normalize within cluster distance by cluster counts - unfortunately, cannot just perform element-wise
        // multiply as some clusters may have zero elements
        silh_num_nonempty_clusters = 0;
        for (scby_uint i_clust=0; i_clust < k; ++i_clust) {
            // skip empty clusters
            scby_assert_bounds(i_clust, 0, cluster_counts.n_elem);
            const scby_real n_struct_at_clust = cluster_counts.at(i_clust);
            if (n_struct_at_clust <= 0.0)
            {
                continue;
            }
            // keep track of number of nonempty clusters
            ++silh_num_nonempty_clusters;
            scby_assert_bounds(i_clust, 0, cluster_mean_sqdists.n_elem);
            cluster_mean_sqdists.at(i_clust) /= n_struct_at_clust;
        }
        scby_assert_bounds(silh_num_nonempty_clusters, 1, (k+1));

        // now, use identity (1/n_i) * sum(sqdist(X_i - a) ) = (1/n_i) * sum(sqdist(X_i, X_bar)) + sqdist(X_bar, a)
        // to compute silhouette score of each sample a, where silh(a):
        // https://en.wikipedia.org/wiki/Silhouette_(clustering)
        // silh(a) = [SSB(a) - SSW(a)] / max{SSB(a), SSW(a)}, where
        // SSB(a) is lowest average dissimilarity of a to any cluster (in this case, lowest average sqdist to
        // all points within another cluster = "between cluster dissimiliary") and SSW(a) is average dissimilarity
        // to points within a's cluster. Furthermore, for clusters with only a single point, let silh(a) = 0.
        // Note: SSB and SSW are analagous but not exactly like the definitions from ANOVA analysis.
        silh_grand_avg_score = scby_real(0.0);

        if (silh_num_nonempty_clusters > 1) {

            scby_real silh_within_sqdist  = scby_real(0.0);
            scby_real silh_between_sqdist = scby_real(0.0);
            scby_real n_struct_at_clust   = scby_real(0.0);

            for (scby_uint i_struct = 0; i_struct < core.n_structs; ++i_struct) {
                scby_assert_bounds(i_struct, 0, buffs.silh_scores.n_elem);
                // If cluster only has single point, then set silh(a) = 0
                const scby_uint cluster_id = buffs.clusters[i_struct];
                scby_assert_bounds(cluster_id, 0, cluster_counts.n_elem);
                // For singleton clusters, assign a score of 0
                n_struct_at_clust = cluster_counts.at(cluster_id);
                if (n_struct_at_clust <= 1.0) {
                    scby_assert(n_struct_at_clust == 1.0);
                    buffs.silh_scores.at(i_struct) = 0.0;
                    continue;
                }

                // Else, compute within-cluster dissimilarity (squared distance):
                // SSW(a) =(n_i/(n_i-1))[ (1/n_i) * sum(sqdist(X_i, X_bar)) + sqdist(X_bar, a) ]
                // We have to correct by (n_i/(n_i - 1)) because we are only doing distance
                // to all other points within cluster, hence only n-1 comparisons instead of n.
                // To start, compute: sqdist(X_bar, a)
                scby_assert_bounds(cluster_id, 0, cent_buff.n_cols);
                SCBY_DIST2(silh_within_sqdist,
                           sqdist_buff,
                           core.positions_mat.unsafe_col(i_struct),
                           cent_buff.unsafe_col(cluster_id));
                // Now, add previously computed term: (1/n_i) * sum(sqdist(X_i, X_bar))
                scby_assert_bounds(cluster_id, 0, cluster_mean_sqdists.n_elem);
                silh_within_sqdist += cluster_mean_sqdists.at(cluster_id);
                // Now correct by factor: (n_i/(n_i - 1))
                scby_assert_bounds(cluster_id, 0, cluster_counts.n_elem);
                silh_within_sqdist *= n_struct_at_clust;
                silh_within_sqdist /= (n_struct_at_clust - scby_real(1.0));

                // Now, find smallest between-cluster dissimilarity
                buffs.silh_scores.at(i_struct) = std::numeric_limits<scby_real>::max();
                silh_between_sqdist = std::numeric_limits<scby_real>::max();
                for (scby_uint i_clust = 0; i_clust < k; ++i_clust) {
                    // skip self cluster
                    if (i_clust == cluster_id) {
                        continue;
                    }
                    // skip empty clusters
                    scby_assert_bounds(i_clust, 0, cluster_counts.n_elem);
                    const scby_real n_struct_at_clust = cluster_counts.at(i_clust);
                    if (n_struct_at_clust <= 0.0) {
                        continue;
                    }
                    // Compute: sqdist(X_bar, a)
                    scby_assert_bounds(i_clust, 0, cent_buff.n_cols);
                    SCBY_DIST2(buffs.silh_scores.at(i_struct),
                               sqdist_buff,
                               core.positions_mat.unsafe_col(i_struct),
                               cent_buff.unsafe_col(i_clust));
                    // Now add previously computed term: (1/n_i) * sum(sqdist(X_i, X_bar))
                    buffs.silh_scores.at(i_struct) += cluster_mean_sqdists.at(i_clust);
                    // Check if we've found a new minimum SSB(a)
                    if (buffs.silh_scores.at(i_struct) < silh_between_sqdist) {
                        silh_between_sqdist = buffs.silh_scores.at(i_struct);
                    }
                }

                // Compute: silh(a) = [SSB(a) - SSW(a)] / max{SSB(a), SSW(a)}
                scby_assert((silh_between_sqdist > 0.0) || (silh_within_sqdist > 0.0));
                buffs.silh_scores.at(i_struct) = (silh_between_sqdist - silh_within_sqdist);
                buffs.silh_scores.at(i_struct) /= std::max(silh_between_sqdist, silh_within_sqdist);

                // update grand total silhouette score
                silh_grand_avg_score += buffs.silh_scores.at(i_struct);
            }

            // compute grand average silhouette score
            silh_grand_avg_score /= core.n_structs;
        }
        else {
            // edge case, we only have a single non-empty cluster
            buffs.silh_scores.zeros();
        }

        // check grand average score makes sense (add a little padding as well)
        scby_assert_bounds(silh_grand_avg_score, -1.1, 1.1);
    }

    void update_history(scby_kmeans_hist_t &hist,
                        const scby_real silh_grand_avg_score,
                        const scby_uint silh_num_nonempty_clusters,
                        const scby_kmeans_buffs_t &buffs,
                        const scby_uint k,
                        const scby_real delta,
                        const scby_uint n_iters,
                        const scby_matrix &cent_buff,
                        const scby_vec_col &cluster_counts,
                        const scby_kmeans_silh_core_t &core) {
        // check if new top score and save relevant history data
        if (hist.top_silh_grand_avg_score < silh_grand_avg_score) {
            hist.top_k = k;
            hist.top_corrected_k = silh_num_nonempty_clusters;
            hist.top_silh_grand_avg_score = silh_grand_avg_score;
            hist.grand_avg_silh_hist[silh_num_nonempty_clusters] = silh_grand_avg_score;
            hist.top_cents = cent_buff;
            scby_assert(buffs.clusters.size() == hist.top_clusters.size());
            scby_assert(buffs.clusters.size() == core.n_structs);
            memcpy(&(hist.top_clusters[0]), &(buffs.clusters[0]), core.n_structs * sizeof(scby_uint));
            hist.top_cluster_counts = cluster_counts;
            hist.top_silh_scores = buffs.silh_scores;
            hist.top_delta = delta;
            hist.top_n_iters = n_iters;
        } else {
            auto score_hist_itr = hist.grand_avg_silh_hist.find(silh_num_nonempty_clusters);
            if ((score_hist_itr == hist.grand_avg_silh_hist.end()) || (score_hist_itr->second < silh_grand_avg_score)) {
                // update maximum score encountered at this k-value
                hist.grand_avg_silh_hist[silh_num_nonempty_clusters] = silh_grand_avg_score;
            }
        }
    }

    void report_heartbeat(const scby_uint k,
                          const std::clock_t &k_start_time,
                          const scby_kmeans_hist_t &hist,
                          const scby_kmeans_silh_args_t &args) {
        // Hearbeat
        if (k % args.k_report_interv == 0) {
            const std::clock_t k_end_time = std::clock();
            const scby_real duration = (k_end_time - k_start_time) / ((scby_real)CLOCKS_PER_SEC);
            std::cout << "Finished runs for k = " << k << std::endl;
            std::cout << "\ttop k-value: " << hist.top_corrected_k << std::endl;
            std::cout << "\ttop silhouette grand average score: " << hist.top_silh_grand_avg_score << std::endl;
            std::cout << "\ttop centroid delta: " << hist.top_delta << std::endl;
            std::cout << "\ttop iterations: " << hist.top_n_iters << std::endl;
            std::cout << "\tinterval time (s): " << duration << std::endl;
        }
    }

    void export_results(const scby_kmeans_hist_t &hist,
                        const scby_kmeans_silh_args_t &args) {
        // export cluster assignments
        if (args.out_clusters_path) {
            std::cout << "Writing cluster assignments to: " << args.out_clusters_path << std::endl;
            scby_vector_write(hist.top_clusters, args.out_clusters_path);
        }

        // export centroid coordinates
        if (args.out_centroids_path) {
            std::cout << "Writing centroid coordinates to: " << args.out_centroids_path << std::endl;
            // @HACK - to avoid having to resize a matrix and possibly call dynamic memory allocation, let's
            // just construct a matrix but using the pre-allocated max-sized buffer as the auxiliary memory
            const scby_matrix cent_coords(const_cast<scby_real*>(hist.top_cents.memptr()),
                                               hist.top_cents.n_rows / args.n_dims,
                                               hist.top_cents.n_cols * args.n_dims,
                                               false /*copy_aux_mem*/,
                                               true /*strict*/);
            cent_coords.save(args.out_centroids_path, scby_matrix_utils::csv_ascii);
        }

        // export number of members within each cluster
        if (args.out_cluster_counts_path) {
            std::cout << "Writing cluster membership counts to: " << args.out_cluster_counts_path << std::endl;
            // Convert to integer types before export
            scby_matrix_utils::Mat<scby_uint> uint_cluster_counts(hist.top_cluster_counts.n_rows, hist.top_cluster_counts.n_cols);
            std::copy(hist.top_cluster_counts.begin(), hist.top_cluster_counts.end(), uint_cluster_counts.begin());
            uint_cluster_counts.save(args.out_cluster_counts_path, scby_matrix_utils::csv_ascii);
        }

        // export silhouette scores for each struct
        if (args.out_silh_scores_path) {
            std::cout << "Writing structure silhouette scores to: " << args.out_silh_scores_path << std::endl;
            hist.top_silh_scores.save(args.out_silh_scores_path, scby_matrix_utils::csv_ascii);
        }

        // export scores for each k attempted
        if (args.out_silh_k_scores_path) {
            std::cout << "Writing grand average silhouette scores for each k level to: " << args.out_silh_k_scores_path << std::endl;
            // convert from unordered to ordered map
            std::map<scby_uint, scby_real> sorted_grand_avg_silh_hist(hist.grand_avg_silh_hist.begin(),
                    hist.grand_avg_silh_hist.end());
            std::ofstream out;
            out.open(args.out_silh_k_scores_path);
            out << "k,score\n";
            for (const auto &k_score: sorted_grand_avg_silh_hist) {
                out << k_score.first << "," << k_score.second << "\n";
            }
            out.close();
        }
    }

} // end of helper namespace

//****************************************************************************
// scby k-means silhouette
//****************************************************************************

// @TODO - investigate Calinski-Harabasz Criterion
// http://www.mathworks.com/help/stats/clustering.evaluation.calinskiharabaszevaluation-class.html
// want max VRK(k) = (SSB/SSW) * (N-k)/(k-1) where k is number of clusters, N is total number of samples,
// overall between-cluster variance:
//  SSB = sum from i = 1...k: n_i * sqdist(m_i - m) where
//      k is number of clusters, n_i is number of elements in i-th clustet,
//      m_i is centroid of i-th cluster, and m is centroid of entire dataset
// overall within-cluster variance:
//  SSW = sum from i = 1...k: sum for all x belong to i-th cluster: sqdist(x - m_i)
// Calinski, T., and J. Harabasz. "A dendrite method for cluster analysis." Communications in Statistics. Vol. 3, No. 1, 1974, pp. 1–27.
// It's nice because the ratio (N-k)/(k-1) penalizes for having too many clusters

// Searches for a cluster count between [min_k and max_k] of the parameter
// data. Uses silhouette method to pick k-value with largest average
// silhouette score. Single-threaded version
void scby_k_means_silh(const scby_kmeans_silh_args_t &args)
{
    const std::clock_t whole_proc_start_time = std::clock();

    // validate arguments
    if (!check_args(args)) {
        return;
    }

    // load core data
    scby_kmeans_silh_core_t core;
    if (!load_core_wrapper(core, args)) {
        return;
    }

    // display arguments to user
    print_args(args);

    // pre-allocate buffers for storing centroids, cluster assignments, and silhouette scores
    scby_kmeans_buffs_t buffs;
    prealloc_buffs(buffs, args.max_k, core);

    // pre-allocate buffers for storing top-scoring results
    scby_kmeans_hist_t hist;
    prealloc_hist(hist, core, args);

    // need random number generator for initializing centroid positions
    scby_rand rng;

    // Used for swapping centroid buffers
    scby_uint i_front_cent_buff = 0;
    scby_uint i_back_cent_buff = 1 - i_front_cent_buff;

    // The number of iterations for the current k-means run
    scby_uint n_iters = 0;

    // keep track of change in centroid positions between iterations
    scby_real iter_delta = std::numeric_limits<scby_real>::max();

    // used for computing silhouette score
    scby_real silh_grand_avg_score = -2.0;
    scby_uint silh_num_nonempty_clusters = 0;

    // buffer used for computing RMSD^2 (actually just sqdist which is prop. to RMSD)
    scby_vec_col sqdist_buff(core.positions_mat.n_rows);

    // @TODO - handle empty clusters

    // Iterate over each target cluster count k
    for (scby_uint k=args.max_k; k>=args.min_k; k-=args.k_inc) {
        const std::clock_t k_start_time = std::clock();
        // @TODO - investigate using set_size() since we're only decreasing k

        // hacky - to avoid having to resize a matrix and possibly call dynamic memory allocation, let's
        // just construct a matrix but using the pre-allocated max-sized buffer as the auxilliary memory
        scby_matrix cent_buffs_0(buffs.cent_buffs[0].memptr(), buffs.cent_buffs[0].n_rows, k /*n_cols*/, false /*copy_aux_mem*/, true /*strict*/);
        scby_assert(NULL != cent_buffs_0.memptr());
        scby_matrix cent_buffs_1(buffs.cent_buffs[1].memptr(), buffs.cent_buffs[1].n_rows, k /*n_cols*/, false /*copy_aux_mem*/, true /*strict*/);
        scby_assert(NULL != cent_buffs_1.memptr());
        scby_matrix* cent_buffs[2] = { &cent_buffs_0, &cent_buffs_1 };

        // hacky - similarly, let's construct a vector for keeping track of number of structs within each cluster
        // (needed for normalizing the new centroid at end of each run)
        scby_vec_col cluster_counts(buffs.cluster_counts.memptr(), k /*n_elems*/, false /*copy_aux_mem*/, true /*strict*/);
        scby_assert(cluster_counts.n_elem == k);
        scby_assert(NULL != cluster_counts.memptr());

        // hacky - similarly, let's construct a vector for keeping track of squared distance from structures
        // within a cluster to that cluster's centroid - needed for computing silhouette scores
        scby_vec_col cluster_mean_sqdists(buffs.cluster_mean_sqdists.memptr(), k /*n_elems*/, false /*copy_aux_mem*/, true /*strict*/);
        scby_assert(cluster_mean_sqdists.n_elem == k);
        scby_assert(NULL != cluster_mean_sqdists.memptr());

        // Iterate over random restart trials with different initial centroid positions
        // to help alleviate converging to local minima
        for (scby_uint i_run=0; i_run<args.n_rand_restarts; ++i_run) {

            // reset iteration counter and delta tracker for next run
            n_iters = 0;
            iter_delta = std::numeric_limits<scby_real>::max();

            // initialize centroids with random elements from input matrix
            scby_assert_bounds(i_front_cent_buff, 0, 2);
            scby_assert(i_front_cent_buff == !i_back_cent_buff);
            init_cents_rand(*cent_buffs[i_front_cent_buff], k, core, rng);

            // run until convergence or maximum iteration count reached
            while ((n_iters++ < args.max_iters_per_run) && (iter_delta > args.delta)) {
                // reset per cluster counts to zero
                memset(cluster_counts.memptr(), 0, cluster_counts.n_elem * sizeof(scby_real));
                // reset centroids for next iteration to origin
                memset(cent_buffs[i_back_cent_buff]->memptr(), 0, cent_buffs[i_back_cent_buff]->n_elem * sizeof(scby_real));

                //////////////////////////////////////////////////////
                // assign each struct to a centroid
                //////////////////////////////////////////////////////

                // @TODO - investigate multi-threading this part
                for (scby_uint i_struct=0; i_struct<core.n_structs; ++i_struct) {
                    scby_assert_bounds(i_struct, 0, core.positions_mat.n_cols);
                    // see which centroid we are closest to
                    scby_uint cluster_id = std::numeric_limits<scby_uint>::max();
                    scby_real min_sqdist = std::numeric_limits<scby_real>::max();
                    scby_real sqdist = std::numeric_limits<scby_real>::max();
                    // compute squared distance to each centroid
                    for (scby_uint i_cent=0; i_cent<k; ++i_cent) {
                        scby_assert_bounds(i_cent, 0, cent_buffs[i_front_cent_buff]->n_cols);
                        SCBY_DIST2(sqdist,
                                   sqdist_buff,
                                   core.positions_mat.unsafe_col(i_struct),
                                   cent_buffs[i_front_cent_buff]->unsafe_col(i_cent));
                        scby_assert(sqdist >= scby_real(0.0));
                        // see if this is a new minimum
                        if (sqdist < min_sqdist) {
                            cluster_id = i_cent;
                            min_sqdist = sqdist;
                        }
                    }
                    scby_assert_bounds(cluster_id, 0, k);
                    scby_assert(min_sqdist >= scby_real(0.0));
                    // update cluster assignment
                    scby_assert_bounds(i_struct, 0, buffs.clusters.size());
                    buffs.clusters[i_struct] = cluster_id;
                    // update cluster count - WARNING: NOT THREAD SAFE
                    scby_assert_bounds(cluster_id, 0, cluster_counts.n_elem);
                    cluster_counts(cluster_id) += 1.0;
                    // update unnormalized centroid position - WARNING: NOT THREAD SAFE
                    scby_assert_bounds(cluster_id, 0, cent_buffs[i_back_cent_buff]->n_cols);
                    cent_buffs[i_back_cent_buff]->unsafe_col(cluster_id) += core.positions_mat.unsafe_col(i_struct);
                }

                //////////////////////////////////////////////////////
                // update centroids
                //////////////////////////////////////////////////////

                // normalize each centroid by dividing all node positions by number
                // of assigned structures to that cluster. also compute iteration delta
                iter_delta = 0.0;
                for (scby_uint i_cent = 0; i_cent < k; ++i_cent)
                {
                    // skip empty clusters
                    scby_assert_bounds(i_cent, 0, cluster_counts.n_elem);
                    if (cluster_counts.at(i_cent) <= 0.0)
                    {
                        continue;
                    }

                    // compute average position of clustered structures
                    const scby_real inv_n_struct_at_clust = 1.0 / cluster_counts(i_cent);
                    scby_assert_bounds(i_cent, 0, cent_buffs[i_back_cent_buff]->n_cols);
                    cent_buffs[i_back_cent_buff]->unsafe_col(i_cent) *= inv_n_struct_at_clust;

                    // update delta (the change in centroid)
                    scby_assert_bounds(i_cent, 0, cent_buffs[i_front_cent_buff]->n_cols);
                    SCBY_INC_BY_DIST2(iter_delta,
                                      sqdist_buff,
                                      cent_buffs[i_back_cent_buff]->unsafe_col(i_cent),
                                      cent_buffs[i_front_cent_buff]->unsafe_col(i_cent));
                }

                // swap centroids buffer
                i_front_cent_buff = 1 - i_front_cent_buff;
                i_back_cent_buff = 1 - i_front_cent_buff;
            }

            //////////////////////////////////////////////////////
            // score clusters
            //////////////////////////////////////////////////////

            calc_silh_scores(silh_grand_avg_score,
                             silh_num_nonempty_clusters,
                             cluster_mean_sqdists,
                             sqdist_buff,
                             buffs,
                             k,
                             *(cent_buffs[i_front_cent_buff]),
                             cluster_counts,
                             core);

            //////////////////////////////////////////////////////
            // update top cluster configuration
            //////////////////////////////////////////////////////

            update_history(hist,
                           silh_grand_avg_score,
                           silh_num_nonempty_clusters,
                           buffs,
                           k,
                           iter_delta,
                           n_iters,
                           *(cent_buffs[i_front_cent_buff]),
                           cluster_counts,
                           core);
        } // end of random restarts for a single k-value

        report_heartbeat(k, k_start_time, hist, args);
    }

    // remove clusters of size zero
    std::cout << "Removing empty clusters...\n";
    remove_empty_clusters(hist);

    // compute total time
    std::clock_t whole_proc_end_time = std::clock();
    const scby_real whole_proc_duration = (whole_proc_end_time - whole_proc_start_time) / ((scby_real)CLOCKS_PER_SEC);

    // report final results
    std::cout << "Finished k-means." << std::endl;
    std::cout << "\ttop k-value: " << hist.top_corrected_k << std::endl;
    std::cout << "\ttop silhouette grand average score: " << hist.top_silh_grand_avg_score << std::endl;
    std::cout << "\ttop centroid delta: " << hist.top_delta << std::endl;
    std::cout << "\ttop iterations: " << hist.top_n_iters << std::endl;
    std::cout << "\ttotal time (s): " << whole_proc_duration << std::endl;

    // write cluster data to disk
    export_results(hist, args);
}
