//****************************************************************************
// scbyParallelMapper.cpp
//****************************************************************************

//****************************************************************************
// Includes
//****************************************************************************

#include "scbyassert.h"
#include "scbyparallel_mapper.h"

//****************************************************************************
// scbyMapperThread
//****************************************************************************

/**
 * Hook to route a thread to its member main
 */
static void thread_entry(void* p_thread)
{
    scby_assert(p_thread);
    scbyMapperThread* const p_mapper_thread = ((scbyMapperThread*)p_thread);
    p_mapper_thread->thread_main();
}

/**
 * Constructs mapper thread
 * @param mapper - owning mapper pool
 * @param thread_id - a unique identifier for this thread
 */
scbyMapperThread::scbyMapperThread(
      scbyParallelMapper& mapper
    , const scby_uint thread_id
)
    : m_mapper(mapper)
    , m_id(thread_id)
    , mp_sys_thread(new scbyThread_t(thread_entry, (void*)this))
{
    scby_assert(NULL != mp_sys_thread);
}

/**
 * Destructor
 */
scbyMapperThread::~scbyMapperThread()
{
    scby_assert(NULL != mp_sys_thread);
    // Owning pool is responsible for waking up thread and making sure it
    // exits
    scby_assert(should_exit());
    // Risky - we are expecting that pool will have woken us up
    if (mp_sys_thread->joinable())
    {
        mp_sys_thread->join();
    }
    delete mp_sys_thread;
}

/**
 * Thread entry point - analogous to process main
 */
void scbyMapperThread::thread_main()
{
    while (!this->should_exit())
    {
        // Wait until notified that work is available
        this->wait_until_work_ready();

        // Kick out of loop if we're exiting
        if (this->should_exit())
        {
            break;
        }

        // Do work until we run out of it
        this->do_available_work();
    }
}

/**
 * @return TRUE if thread should exit, FALSE o/w
 */
scby_bool scbyMapperThread::should_exit() const
{
    return m_mapper.exiting();
}

/**
 * Waits until signaled to start
 * Also notifies mapper thread when all workers have become idle
 */
void scbyMapperThread::wait_until_work_ready() const
{
    scbyLock_t lk(m_mapper.m_mutex);
    // Note - mapper is responsible for setting initial active worker count
    // upon receiving work (or initializing)
    if (m_mapper.m_num_active_workers.fetch_sub(1, scbyTh::memory_order_relaxed) == 1) {
        // Tell mapper we need more work - this is a "pessimistic" notify as the the
        // mapper thread will immediately block (until mutex is released by worker
        // thread wating on cv_work_begin), but we need this to remove race
        // conditions and it should be more efficient than having the mapper thread
        // loop using sleep(0).
        m_mapper.m_cv_all_workers_idle.notify_one();
    }
    // Use loop to handle spurious wake ups
    do {
        m_mapper.m_cv_work_begin.wait(lk);
    } while ((m_mapper.mp_work == NULL) && !m_mapper.exiting());
}

/**
 * Keeps grabbing chunks of work until none is available
 */
void scbyMapperThread::do_available_work() const
{
    // Verify that work is valid
    scby_assert(NULL != m_mapper.mp_work);
    // If we are not incrementing by a positive amount then we could spin
    // forever!
    scby_assert(m_mapper.m_inc_by > 0);

    const scby_uint inc_by = m_mapper.m_inc_by.load(scbyTh::memory_order_relaxed);

    // Grab a chunk of work to do until we run out of it
    while (m_mapper.mp_work->do_range(
                  m_mapper.m_iter.fetch_add(inc_by, scbyTh::memory_order_relaxed)
                , m_mapper.m_inc_by
                , m_id
            ))
    {}
}

//****************************************************************************
// scbyParallelMapper
//****************************************************************************

/**
 * Default constructor
 */
scbyParallelMapper::scbyParallelMapper()
    : mp_work(NULL)
    , m_inc_by(0)
    , m_iter(0)
    , m_num_active_workers(0)
    , m_should_exit(SCBY_FALSE)
{
    scby_assert(m_inc_by.is_lock_free());
    scby_assert(m_iter.is_lock_free());
    scby_assert(m_num_active_workers.is_lock_free());
    scby_assert(m_should_exit.is_lock_free());
}

/**
 * Destructor
 */
scbyParallelMapper::~scbyParallelMapper()
{
    teardown();
}

/**
 * Can only be called once, initializes thread pool
 */
void scbyParallelMapper::init(const scby_uint num_threads)
{
    scby_assert(num_threads > 0);
    scby_assert(m_should_exit == SCBY_FALSE);
    scby_assert(m_num_active_workers == 0);
    scby_assert(m_pool.size() == 0);
    scby_assert(NULL == mp_work);

    m_pool.resize(num_threads, NULL);
    m_num_active_workers.store(as_scby_uint(m_pool.size()), scbyTh::memory_order_seq_cst);
    // Allocate threads
    scbyLock_t lk(m_mutex);
    for (size_t i = 0; i < m_pool.size(); ++i) {
        m_pool[i] = new scbyMapperThread(*this, as_scby_uint(i));
        scby_assert(NULL != m_pool[i]);
    }
    // Block until all worker threads are waiting to be assigned new work
    // (note: waiting on condition variable releases lock)
    wait_until_all_workers_idle(lk);
}

/**
 * Barrier method - waits until workers threads
 * have blocked on condition variable used for work
 * available notification. This helps avoid race
 * conditions that could otherwise occur if work
 * becomes available before worker threads block
 * (which would cause cond. var::notify_all() to be
 * called before worker thread is waiting on it, which
 * when worker thread does finally call wait, will cause
 * the thread to block instead, which leads to deadlock
 * as main thread is counting on workers to decrement
 * m_num_active_workers to 0.
 */
void scbyParallelMapper::wait_until_all_workers_idle(scbyLock_t &lk) {
    // Loop to handle any spurious wake up calls
    do {
        // Wait call releases lock
        m_cv_all_workers_idle.wait(lk);
    } while (m_num_active_workers > 0);
}

/**
 * Processes work object and blocks calling thread until finished.
 * @param work - the work to process in parallel
 * @param inc_by - the chunk size of each work unit processed by a thread
 */
void scbyParallelMapper::wait_until_work_finished(
      scbyMappableWork& work
    , const scby_uint inc_by
)
{
    scby_assert(NULL == mp_work);
    scby_assert(inc_by > 0);
    scby_assert(0 == m_num_active_workers);
    scby_assert(SCBY_FALSE == m_should_exit);
    scby_assert(m_pool.size() > 0);

    mp_work = &work;
    m_iter.store(0, scbyTh::memory_order_relaxed);
    m_inc_by.store(inc_by, scbyTh::memory_order_relaxed);
    m_num_active_workers.store(as_scby_uint(m_pool.size()), scbyTh::memory_order_seq_cst);

    // Note - this is a "pessimistic" lock as any workers that wake up
    // immediately from the notify call will block on the shared mutex.
    // However, we need this to avoid race conditions as we need to
    // guarantee parent mapper is actually waiting on the all workers idle
    // condition variable (cv) before any of the workers can notify on it.
    // (If we didn't guard this section, then it's theoretically possible
    // for all workers to finish, then signal the mapper, before the
    // mapper is actually waiting on the all workers idle cv; in which case,
    // we would deadlock)
    scbyLock_t lk(m_mutex);
    // Wake up worker threads
    m_cv_work_begin.notify_all();
    // Block until all threads finish - releases lock
    wait_until_all_workers_idle(lk);
    scby_assert(0 == m_num_active_workers);

    // Clear our work pointer
    // (client is responsible for managing its lifetime)
    mp_work = NULL;
}

/**
 * @return TRUE if threads should exit, FALSE o/w
 */
scby_bool scbyParallelMapper::exiting() const
{
    return (SCBY_TRUE == m_should_exit.load(scbyTh::memory_order_relaxed));
}

/**
 * Outputs mapper status
 */
void scbyParallelMapper::log_status() const
{
    std::cout << "-----------------------\n";
    std::cout << "scbyParallelMapper status:\n";
    std::cout << "-mp_work is null: " << ((mp_work == NULL) ? "true" : "false") << std::endl;
    std::cout << "-m_inc_by: " << m_inc_by.load() << std::endl;
    std::cout << "-m_iter: " << m_iter.load() << std::endl;
    std::cout << "-m_num_active_workers: " << m_num_active_workers.load() << std::endl;
    std::cout << "-m_should_exit: " << m_should_exit.load() << std::endl;
    std::cout << "-m_pool size: " << m_pool.size() << std::endl;
    std::cout << "-----------------------\n";
}

/**
 * Address the following issue:
 * http://stackoverflow.com/questions/10915233/stdthreadjoin-hangs-if-called-after-main-exits-when-using-vs2012-rc
 * Basically on windows, if threads are still "alive" before main thread
 * exits, then the program will hang. Therefore, we need to destroy our
 * threads prior to main() exiting.
 */
void scbyParallelMapper::teardown()
{
    // Nothing should still be working
    scby_assert(0 == m_num_active_workers);
    // We shouldn't have any assigned work
    scby_assert(NULL == mp_work);

    // Signal that we are exiting
    m_should_exit.store(SCBY_TRUE, scbyTh::memory_order_seq_cst);
    m_cv_work_begin.notify_all();

    // Call thread destructors
    for (size_t i = 0; i < m_pool.size(); ++i)
    {
        scby_assert(NULL != m_pool[i]);
        delete m_pool[i];
    }

    m_pool.clear();
}
