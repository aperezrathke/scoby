/**
 * @author Alan Perez-Rathke
 * @date August 24, 2015
 */

//****************************************************************************
// Includes
//****************************************************************************

#include "scbybuild.h"
#include "scbycompute_pdist.h"
#include "scbyassert.h"

//****************************************************************************
// Helper namespace
//****************************************************************************

// Common utilities for RMSD computation
namespace {

    /**
     * computes the pairwise RMSD for [start_ix, one_past_end_ix) to all structures
     * with indices greater than themselves.
     * @param rmsd_mat - output matrix for storing pairwise RMSD calculations
     * @param positions_mat - input coordinates, each sample is a single column
     * @param inv_n_nodes - 1 / number of nodes per sample
     * @param dist_vec - buffer for storing intermediate RMSD computations
     * @param start_ix - the index of the starting conformation to compute
     *  RMSD against all other conformations (upper triangle only)
     * @param one_past_end_ix - denotes upper bound for stopping pairwise RMSD computation
     */
    template <typename pdist_mat_t>
    void rmsd_range(pdist_mat_t &rmsd_mat,
                    const scby_matrix &positions_mat,
                    const scby_real inv_n_nodes,
                    scby_vec_col &dist_vec,
                    const scby_uint start_ix,
                    const scby_uint one_past_end_ix) {

        const scby_uint n_structs = positions_mat.n_cols;

        // make sure size is as expected
        assert(dist_vec.n_elem == positions_mat.n_rows);

        scby_real tmp_rmsd = 0.0;

        // compute RMSD based on:
        // https://en.wikipedia.org/wiki/Root-mean-square_deviation_of_atomic_positions
        for (unsigned int i_struct = start_ix; i_struct < one_past_end_ix; ++i_struct) {
            // diagonal RMSD is always 0.0
            rmsd_mat(i_struct, i_struct) = 0.0;

            for (unsigned int j_struct = (i_struct + 1); j_struct < n_structs; ++j_struct) {
                dist_vec = positions_mat.unsafe_col(i_struct) - positions_mat.unsafe_col(j_struct);
                dist_vec %= dist_vec;
                tmp_rmsd = scby_matrix_utils::sum(dist_vec);
                tmp_rmsd *= inv_n_nodes;
                tmp_rmsd = std::sqrt(tmp_rmsd);
                rmsd_mat(i_struct, j_struct) = tmp_rmsd;
                rmsd_mat(j_struct, i_struct) = tmp_rmsd;
            }
        }
    }

    /**
     * Overload for a memory efficient symmetric matrix - only write upper tri
     * computes the pairwise squared distance for [start_ix, one_past_end_ix) to all structures
     * with indices greater than themselves.
     * @param pdist2_mat - output matrix for storing pairwise squared distance calculations
     * @param positions_mat - input coordinates, each sample is a single column
     * @param dist_vec - buffer for storing intermediate squared distance computations
     * @param start_ix - the index of the starting conformation to compute
     *  squared distance against all other conformations (upper triangle only)
     * @param one_past_end_ix - denotes upper bound for stopping pairwise squared sitance computation
     */
    void pdist2_range(scby_sym_matrix &pdist2_mat,
                      const scby_matrix &positions_mat,
                      scby_vec_col &dist_vec,
                      const scby_uint start_ix,
                      const scby_uint one_past_end_ix) {

        const scby_uint n_structs = positions_mat.n_cols;

        // make sure size is as expected
        assert(dist_vec.n_elem == positions_mat.n_rows);

        // compute pairwise squared distances
        for (unsigned int i_struct = start_ix; i_struct < one_past_end_ix; ++i_struct) {
            // diagonal distance is always 0.0
            pdist2_mat(i_struct, i_struct) = 0.0;

            for (unsigned int j_struct = (i_struct + 1); j_struct < n_structs; ++j_struct) {
                dist_vec = positions_mat.unsafe_col(i_struct) - positions_mat.unsafe_col(j_struct);
                dist_vec %= dist_vec;
                pdist2_mat(i_struct, j_struct) = scby_matrix_utils::sum(dist_vec);
            }
        }
    }

    /**
     * Overload for a full-sized matrix - avoid bounds checking
     * computes the pairwise squared distance for [start_ix, one_past_end_ix) to all structures
     * with indices greater than themselves.
     * @param pdist2_mat - output matrix for storing pairwise squared distance calculations
     * @param positions_mat - input coordinates, each sample is a single column
     * @param dist_vec - buffer for storing intermediate squared distance computations
     * @param start_ix - the index of the starting conformation to compute
     *  squared distance against all other conformations (upper triangle only)
     * @param one_past_end_ix - denotes upper bound for stopping pairwise squared sitance computation
     */
    void pdist2_range(scby_matrix &pdist2_mat,
                      const scby_matrix &positions_mat,
                      scby_vec_col &dist_vec,
                      const scby_uint start_ix,
                      const scby_uint one_past_end_ix) {

        const scby_uint n_structs = positions_mat.n_cols;

        // make sure size is as expected
        assert(dist_vec.n_elem == positions_mat.n_rows);

        // compute pairwise squared distances
        for (unsigned int i_struct = start_ix; i_struct < one_past_end_ix; ++i_struct) {
            // diagonal distance is always 0.0
            pdist2_mat(i_struct, i_struct) = 0.0;

            for (unsigned int j_struct = (i_struct + 1); j_struct < n_structs; ++j_struct) {
                SCBY_DIST2(pdist2_mat.at(j_struct, i_struct),
                           dist_vec,
                           positions_mat.unsafe_col(i_struct),
                           positions_mat.unsafe_col(j_struct));
                pdist2_mat.at(i_struct, j_struct) = pdist2_mat.at(j_struct, i_struct);
            }
        }
    }

    /**
     * Overload for allocating memory efficient pairwise distance matrix
     */
    void alloc_pdist_mat(scby_sym_matrix &pdist_mat, const scby_uint n_structs) {
        scby_assert(n_structs > 1);
        pdist_mat.resize(n_structs, false /*preserve*/);
    }

    /**
     * Overload for allocating non-memory efficient pairwise distance matrix
     */
    void alloc_pdist_mat(scby_matrix &pdist_mat, const scby_uint n_structs) {
        scby_assert(n_structs > 1);
        pdist_mat.set_size(n_structs /*n_rows*/, n_structs /*n_cols*/);
    }

} // end of helper namespace

//****************************************************************************
// Common utilities for multi-threaded overloads
//****************************************************************************

/**
 * If want to pre-allocate multi-thread scratch buffers for RMSD, pdist2, pdist
 * calculations, then use any overload of this method
 * @param dist_vecs_mt - the output per-thread allocated scratch buffers
 * @param pm - initialized parallel mapper for managing threads
 * @param positions_mat - each sample is a single column
 */
void scby_alloc_mt_pdist_buffers(std::vector<scby_vec_col> &dist_vecs_mt,
                                 const scbyParallelMapper &pm,
                                 const scby_matrix &positions_mat) {
    scby_alloc_mt_pdist_buffers(dist_vecs_mt, pm.num_pool_threads(), positions_mat.n_rows);
}

/**
 * If want to pre-allocate multi-thread scratch buffers for RMSD, pdist2, pdist
 * calculations, then use any overload of this method
 * @param dist_vecs_mt - the output per-thread allocated scratch buffers
 * @param n_threads - the number of threads used for parallel distance calculations
 * @param n_elems - the total number of elements (dimensions) defining a single sample
 */
void scby_alloc_mt_pdist_buffers(std::vector<scby_vec_col> &dist_vecs_mt,
                                 const scby_uint n_threads,
                                 const scby_uint n_elems) {
    scby_assert(n_threads >= 1);
    scby_assert(n_elems >= 1);
    dist_vecs_mt.resize(n_threads);
    for (scby_uint i=0; i<n_threads; ++i) {
        dist_vecs_mt[i].set_size(n_elems);
    }
}

//****************************************************************************
// Single-threaded (serial) RMSD implementation
//****************************************************************************

/**
 * Utility computes root-mean-square distance (RMSD) - single threaded
 * https://en.wikipedia.org/wiki/Root-mean-square_deviation_of_atomic_positions
 * @param rmsd_mat - output symmetric RMSD matrix
 * @param positions_mat - each sample is a single column
 * @param n_nodes - the number of nodes defining each sample
 */
template <typename pdist_mat_t>
void scby_compute_rmsd_(pdist_mat_t &rmsd_mat,
                        const scby_matrix &positions_mat,
                        const scby_uint n_nodes) {
    scby_assert(n_nodes >= 1);
    const scby_real inv_n_nodes = as_scby_real(1.0) / as_scby_real(n_nodes);
    alloc_pdist_mat(rmsd_mat, positions_mat.n_cols);
    // allocate buffer for temporary distance calculations
    scby_vec_col dist_vec(positions_mat.n_rows);
    // perform RMSD calculations
    rmsd_range(rmsd_mat,
               positions_mat,
               inv_n_nodes,
               dist_vec,
               0, /*start_ix*/
               positions_mat.n_cols /*one_past_end_ix*/);
}

/**
 * Computes root-mean-square distance (RMSD)
 * https://en.wikipedia.org/wiki/Root-mean-square_deviation_of_atomic_positions
 * @param rmsd_mat - output symmetric RMSD matrix
 * @param positions_mat - each sample is a single column
 * @param n_nodes - the number of nodes defining each sample
 */
void scby_compute_rmsd(scby_sym_matrix &rmsd_mat,
                       const scby_matrix &positions_mat,
                       const scby_uint n_nodes) {
    scby_compute_rmsd_(rmsd_mat, positions_mat, n_nodes);
}

/**
 * Overload to use memory intensive matrix
 * Computes root-mean-square distance (RMSD)
 * https://en.wikipedia.org/wiki/Root-mean-square_deviation_of_atomic_positions
 * @param rmsd_mat - output symmetric RMSD matrix
 * @param positions_mat - each sample is a single column
 * @param n_nodes - the number of nodes defining each sample
 */
void scby_compute_rmsd(scby_matrix &rmsd_mat,
                       const scby_matrix &positions_mat,
                       const scby_uint n_nodes) {
    scby_compute_rmsd_(rmsd_mat, positions_mat, n_nodes);
}

//****************************************************************************
// Multi-threaded RMSD mappable work
//****************************************************************************

/**
 * Interface for an array to be processed in parallel
 */
template <typename pdist_mat_t>
class scbyRmsdWork : public scbyMappableWork
{
public:

    /**
     * Constructor
     * @param rmsd_mat - output symmetric RMSD matrix
     * @param positions_mat - each sample is a single column
     * @param inv_n_nodes - the number of nodes defining each sample
     * @param dist_vecs_mt - per thread scratch buffers for distance calculations
     *  -must be allocated via scby_alloc_mt_pdist_buffers(...)
     */
    scbyRmsdWork(pdist_mat_t &rmsd_mat,
                 const scby_matrix &positions_mat,
                 const scby_real inv_n_nodes,
                 std::vector<scby_vec_col> &dist_vecs_mt)
        : m_rmsd_mat(rmsd_mat)
        , m_positions_mat(positions_mat)
        , m_inv_n_nodes(inv_n_nodes)
        , m_dist_vecs(dist_vecs_mt) {
        scby_assert(inv_n_nodes > as_scby_real(0.0));
    }

    /**
     * Defines what work is to be performed for the parameter range
     * @param ix_start - the starting index to perform work at
     * @param count - the number of work units to perform beginning at ix_start
     * @param thread_id - a unique calling thread identifier
     */
    virtual scby_bool do_range(const scby_uint ix_start,
                               const scby_uint count,
                               const scby_uint thread_id) {
        scby_assert_bounds(thread_id, 0, m_dist_vecs.size());
        const scby_uint n_structs = m_positions_mat.n_cols;
        const scby_uint end = std::min(ix_start + count, n_structs);

        rmsd_range(m_rmsd_mat,
                   m_positions_mat,
                   m_inv_n_nodes,
                   m_dist_vecs[thread_id],
                   ix_start,
                   end);

        return end < n_structs;
    }

private:

    /**
     * disallow copy and assignment
     */
    scbyRmsdWork(const scbyRmsdWork &);
    scbyRmsdWork& operator=(const scbyRmsdWork &);

    /**
     * output RMSD matrix
     */
    pdist_mat_t &m_rmsd_mat;

    /**
     * input coordinates - each sample is a single column
     */
    const scby_matrix &m_positions_mat;

    /**
     * 1 / number of nodes per sample
     */
    const scby_real m_inv_n_nodes;

    /**
     * per-thread buffers for RMSD calculations
     */
    std::vector<scby_vec_col> &m_dist_vecs;
};

//****************************************************************************
// Multi-threaded (parallel) RMSD implementation
//****************************************************************************

/**
 * multi-threaded version of scby_compute_rmsd
 * uses pre-computed per-thread scratch buffers
 * @param rmsd_mat - output symmetric RMSD matrix
 * @param positions_mat - each sample is a single column
 * @param n_nodes - the number of nodes defining each sample
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 * @param dist_vecs_mt - per thread scratch buffers for distance calculations
 *  -must be allocated via scby_alloc_mt_pdist_buffers(...)
 */
template <typename pdist_mat_t>
void scby_compute_rmsd_mt_(pdist_mat_t &rmsd_mat,
                           const scby_matrix &positions_mat,
                           const scby_uint n_nodes,
                           scbyParallelMapper &pm,
                           const scby_uint inc_by,
                           std::vector<scby_vec_col> &dist_vecs_mt) {
    scby_assert(inc_by >= 1);
    scby_assert(n_nodes >= 1);
    const scby_real inv_n_nodes = as_scby_real(1.0) / as_scby_real(n_nodes);

    // allocate pairwise distance matrix
    // note: this is a cheap call if rmsd_mat is already pre-sized properly
    alloc_pdist_mat(rmsd_mat, positions_mat.n_cols);

    // initialize work object
    scbyRmsdWork<pdist_mat_t> work(rmsd_mat,
                                   positions_mat,
                                   inv_n_nodes,
                                   dist_vecs_mt);

    pm.wait_until_work_finished(work, inc_by);
}

/**
 * multi-threaded version of scby_compute_rmsd
 * uses pre-allocated scratch buffers
 * @param rmsd_mat - output symmetric RMSD matrix
 * @param positions_mat - each sample is a single column
 * @param n_nodes - the number of nodes defining each sample
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 * @param dist_vecs_mt - per thread scratch buffers for distance calculations
 * -must be allocated via scby_alloc_mt_pdist_buffers(...)
 */
void scby_compute_rmsd_mt(scby_sym_matrix &rmsd_mat,
                          const scby_matrix &positions_mat,
                          const scby_uint n_nodes,
                          scbyParallelMapper &pm,
                          const scby_uint inc_by,
                          std::vector<scby_vec_col> &dist_vecs_mt) {
    scby_compute_rmsd_mt_(rmsd_mat,
                          positions_mat,
                          n_nodes,
                          pm,
                          inc_by,
                          dist_vecs_mt);
}

/**
 * overload to use memory intensive matrix
 * multi-threaded version of scby_compute_rmsd
 * uses pre-allocated scratch buffers
 * @param rmsd_mat - output symmetric RMSD matrix
 * @param positions_mat - each sample is a single column
 * @param n_nodes - the number of nodes defining each sample
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 * @param dist_vecs_mt - per thread scratch buffers for distance calculations
 * -must be allocated via scby_alloc_mt_pdist_buffers(...)
 */
void scby_compute_rmsd_mt(scby_matrix &rmsd_mat,
                          const scby_matrix &positions_mat,
                          const scby_uint n_nodes,
                          scbyParallelMapper &pm,
                          const scby_uint inc_by,
                          std::vector<scby_vec_col> &dist_vecs_mt) {
    scby_compute_rmsd_mt_(rmsd_mat,
                          positions_mat,
                          n_nodes,
                          pm,
                          inc_by,
                          dist_vecs_mt);
}

/**
 * multi-threaded version of scby_compute_rmsd
 * @param rmsd_mat - output symmetric RMSD matrix
 * @param positions_mat - each sample is a single column
 * @param n_nodes - the number of nodes defining each sample
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 */
template <typename pdist_mat_t>
void scby_compute_rmsd_mt_(pdist_mat_t &rmsd_mat,
                           const scby_matrix &positions_mat,
                           const scby_uint n_nodes,
                           scbyParallelMapper &pm,
                           const scby_uint inc_by) {
    // allocate multi-threaded scratch buffers
    std::vector<scby_vec_col> dist_vecs_mt;
    scby_alloc_mt_pdist_buffers(dist_vecs_mt, pm, positions_mat);

    scby_compute_rmsd_mt_(rmsd_mat,
                          positions_mat,
                          n_nodes,
                          pm,
                          inc_by,
                          dist_vecs_mt);
}

/**
 * multi-threaded version of scby_compute_rmsd
 * @param rmsd_mat - output symmetric RMSD matrix
 * @param positions_mat - each sample is a single column
 * @param n_nodes - the number of nodes defining each sample
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 */
void scby_compute_rmsd_mt(scby_sym_matrix &rmsd_mat,
                          const scby_matrix &positions_mat,
                          const scby_uint n_nodes,
                          scbyParallelMapper &pm,
                          const scby_uint inc_by) {
    scby_compute_rmsd_mt_(rmsd_mat,
                          positions_mat,
                          n_nodes,
                          pm,
                          inc_by);
}

/**
 * overload to use memory intensive matrix
 * multi-threaded version of scby_compute_rmsd
 * @param rmsd_mat - output symmetric RMSD matrix
 * @param positions_mat - each sample is a single column
 * @param n_nodes - the number of nodes defining each sample
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 */
void scby_compute_rmsd_mt(scby_matrix &rmsd_mat,
                          const scby_matrix &positions_mat,
                          const scby_uint n_nodes,
                          scbyParallelMapper &pm,
                          const scby_uint inc_by) {
    scby_compute_rmsd_mt_(rmsd_mat,
                          positions_mat,
                          n_nodes,
                          pm,
                          inc_by);
}

//****************************************************************************
// Single-threaded squared distance implementation
//****************************************************************************

/**
 * Utility computes pairwise squared distances - single threaded
 * @param pdist2_mat - output symmetric squared distances matrix
 * @param positions_mat - each sample is a single column
 */
template <typename pdist_mat_t>
void scby_compute_pdist2_(pdist_mat_t &pdist2_mat,
                          const scby_matrix &positions_mat) {
    alloc_pdist_mat(pdist2_mat, positions_mat.n_cols);
    // allocate buffer for temporary distance calculations
    scby_vec_col dist_vec(positions_mat.n_rows);
    // perform pairwise squared distance calculations
    pdist2_range(pdist2_mat,
                 positions_mat,
                 dist_vec,
                 0, /*start_ix*/
                 positions_mat.n_cols /*one_past_end_ix*/);
}

/**
 * Computes pairwise squared distances
 * @param pdist2_mat - output symmetric squared distance matrix
 * @param positions_mat - each sample is a single column
 */
void scby_compute_pdist2(scby_sym_matrix &pdist2_mat,
                         const scby_matrix &positions_mat) {
    scby_compute_pdist2_(pdist2_mat, positions_mat);
}


/**
 * Overload to use memory intensive matrix
 * Computes pairwise squared distances
 * @param pdist2_mat - output symmetric squared distance matrix
 * @param positions_mat - each sample is a single column
 */
void scby_compute_pdist2(scby_matrix &pdist2_mat,
                         const scby_matrix &positions_mat) {
    scby_compute_pdist2_(pdist2_mat, positions_mat);
}

//****************************************************************************
// Multi-threaded squared distance mappable work
//****************************************************************************

/**
 * Interface for an array to be processed in parallel
 */
template <typename pdist_mat_t>
class scbyPdist2Work : public scbyMappableWork
{
public:

    /**
     * Constructor
     * @param pdist2_mat - output symmetric squared distance matrix
     * @param positions_mat - each sample is a single column
     * @param dist_vecs_mt - per thread scratch buffers for distance calculations
     *  -must be allocated via scby_alloc_mt_pdist_buffers(...)
     */
    scbyPdist2Work(pdist_mat_t &pdist2_mat,
                   const scby_matrix &positions_mat,
                   std::vector<scby_vec_col> &dist_vecs_mt)
        : m_pdist2_mat(pdist2_mat)
        , m_positions_mat(positions_mat)
        , m_dist_vecs(dist_vecs_mt) {}

    /**
     * Defines what work is to be performed for the parameter range
     * @param ix_start - the starting index to perform work at
     * @param count - the number of work units to perform beginning at ix_start
     * @param thread_id - a unique calling thread identifier
     */
    virtual scby_bool do_range(const scby_uint ix_start,
                               const scby_uint count,
                               const scby_uint thread_id) {
        scby_assert_bounds(thread_id, 0, m_dist_vecs.size());
        const scby_uint n_structs = m_positions_mat.n_cols;
        const scby_uint end = std::min(ix_start + count, n_structs);

        pdist2_range(m_pdist2_mat,
                     m_positions_mat,
                     m_dist_vecs[thread_id],
                     ix_start,
                     end);

        return end < n_structs;
    }

private:

    /**
     * disallow copy and assignment
     */
    scbyPdist2Work(const scbyPdist2Work &);
    scbyPdist2Work& operator=(const scbyPdist2Work &);

    /**
     * output squared distance matrix
     */
    pdist_mat_t &m_pdist2_mat;

    /**
     * input coordinates - each sample is a single column
     */
    const scby_matrix &m_positions_mat;

    /**
     * per-thread buffers for distance calculations
     */
    std::vector<scby_vec_col> &m_dist_vecs;
};

//****************************************************************************
// Multi-threaded (parallel) squared distance implementation
//****************************************************************************

/**
 * multi-threaded version of scby_compute_pdist2
 * uses pre-computed per-thread scratch buffers
 * @param pdist2_mat - output symmetric squared distance matrix
 * @param positions_mat - each sample is a single column
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 * @param dist_vecs_mt - per thread scratch buffers for distance calculations
 *  -must be allocated via scby_alloc_mt_pdist_buffers(...)
 */
template <typename pdist_mat_t>
void scby_compute_pdist2_mt_(pdist_mat_t &pdist2_mat,
                             const scby_matrix &positions_mat,
                             scbyParallelMapper &pm,
                             const scby_uint inc_by,
                             std::vector<scby_vec_col> &dist_vecs_mt) {
    scby_assert(inc_by >= 1);

    // allocate pairwise distance matrix
    // note: this is a cheap call if rmsd_mat is already pre-sized properly
    alloc_pdist_mat(pdist2_mat, positions_mat.n_cols);

    // initialize work object
    scbyPdist2Work<pdist_mat_t> work(pdist2_mat,
                                     positions_mat,
                                     dist_vecs_mt);

    pm.wait_until_work_finished(work, inc_by);
}

/**
 * multi-threaded version of scby_compute_pdist2
 * uses pre-allocated scratch buffers
 * @param pdist2_mat - output symmetric squared distance matrix
 * @param positions_mat - each sample is a single column
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 * @param dist_vecs_mt - per thread scratch buffers for distance calculations
 * -must be allocated via scby_alloc_mt_pdist_buffers(...)
 */
void scby_compute_pdist2_mt(scby_sym_matrix &pdist2_mat,
                            const scby_matrix &positions_mat,
                            scbyParallelMapper &pm,
                            const scby_uint inc_by,
                            std::vector<scby_vec_col> &dist_vecs_mt) {
    scby_compute_pdist2_mt_(pdist2_mat,
                            positions_mat,
                            pm,
                            inc_by,
                            dist_vecs_mt);
}


/**
 * overload to use memory intensive matrix
 * multi-threaded version of scby_compute_pdist2
 * uses pre-allocated scratch buffers
 * @param pdist2_mat - output symmetric squared distance matrix
 * @param positions_mat - each sample is a single column
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 * @param dist_vecs_mt - per thread scratch buffers for distance calculations
 * -must be allocated via scby_alloc_mt_pdist_buffers(...)
 */
void scby_compute_pdist2_mt(scby_matrix &pdist2_mat,
                            const scby_matrix &positions_mat,
                            scbyParallelMapper &pm,
                            const scby_uint inc_by,
                            std::vector<scby_vec_col> &dist_vecs_mt) {
    scby_compute_pdist2_mt_(pdist2_mat,
                            positions_mat,
                            pm,
                            inc_by,
                            dist_vecs_mt);
}

/**
 * multi-threaded version of scby_compute_pdist2
 * @param pdist2_mat - output symmetric squared distance matrix
 * @param positions_mat - each sample is a single column
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 */
template <typename pdist_mat_t>
void scby_compute_pdist2_mt_(pdist_mat_t &pdist2_mat,
                             const scby_matrix &positions_mat,
                             scbyParallelMapper &pm,
                             const scby_uint inc_by) {
    // allocate multi-threaded scratch buffers
    std::vector<scby_vec_col> dist_vecs_mt;
    scby_alloc_mt_pdist_buffers(dist_vecs_mt, pm, positions_mat);

    scby_compute_pdist2_mt_(pdist2_mat,
                            positions_mat,
                            pm,
                            inc_by,
                            dist_vecs_mt);
}

/**
 * multi-threaded version of scby_compute_pdist2
 * @param pdist2_mat - output symmetric squared distance matrix
 * @param positions_mat - each sample is a single column
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 */
void scby_compute_pdist2_mt(scby_sym_matrix &pdist2_mat,
                            const scby_matrix &positions_mat,
                            scbyParallelMapper &pm,
                            const scby_uint inc_by) {
    scby_compute_pdist2_mt_(pdist2_mat,
                            positions_mat,
                            pm,
                            inc_by);
}

/**
 * overload to use memory intensive non-symmetric matrix
 * multi-threaded version of scby_compute_pdist2
 * @param pdist2_mat - output symmetric squared distance matrix
 * @param positions_mat - each sample is a single column
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 */
void scby_compute_pdist2_mt(scby_matrix &pdist2_mat,
                            const scby_matrix &positions_mat,
                            scbyParallelMapper &pm,
                            const scby_uint inc_by) {
    scby_compute_pdist2_mt_(pdist2_mat,
                            positions_mat,
                            pm,
                            inc_by);
}
