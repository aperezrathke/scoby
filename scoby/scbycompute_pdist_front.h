/**
 * Front-facing (shell) utilities for computing pairwise distances
 */
#ifndef SCBYCOMPUTE_PDIST_FRONT_H
#define SCBYCOMPUTE_PDIST_FRONT_H

//****************************************************************************
// Includes
//****************************************************************************

#include "scbybuild.h"

//****************************************************************************
// Single-threaded (serial) RMSD implementation
//****************************************************************************

/**
 * @param in_bin_path - The input bin path. Each column represents the
 * coordinate of the structure nodes at a single dimension. Each row
 *  represents the positions of the i-th node. For example, assume N
 *  structures (e.g., N protein structures):
 *
 * Each column is ordered like so:
 *  Col = x.structA, y.structB, z.structC, ... x.structN, y.structN, z.structN
 *
 * Each row is ordered like so:
 *  Row 1 = 1st node: x_1A, y_1A, z_1A, x_1B, y_1B, z_1B, ... x_1N, y_1N, z_YN
 *  Row 2 = 2nd node: x_2A, y_2A, z_2A, x_2B, y_2B, z_2B, ... x_2N, y_2N, z_2N
 *      ...
 *  Row k = k-th node: x_kA, y_kA, z_kA, z_kA, ... x_kN, y_kN, z_kN
 *
 * @param n_dims - the number of dimensions for each position (e.g. 2-D or 3-D),
 *  must be a positive integer. Dimensions are assumed to be consecutive. So,
 *  for a 2-D representation, input columns and rows (for file specified by
 *  in_csv_path) are:
 *
 *  Col = x.structA, y.structB, ... x.structN, y.structN
 *  Row = 1st node: x_1A, y_1A, ... x_1N, y_1N
 *      ...
 *  Row k = k-th node: x_kA, y_kA, ... x_kN, y_1N
 *
 * @param out_bin_path - output path for binary matrix for use with clustering.
 *  if null, then will not be output.
 * @param out_csv_path - OPTIONAL output path to plain-text csv file of RMSD
 *  distances. If non-null, output will be lower symmetric.
 */
extern void scby_compute_rmsd_front(const char *in_bin_path,
                                    const unsigned int n_dims,
                                    const char *out_bin_path,
                                    const char *out_csv_path);

//****************************************************************************
// Multi-threaded (parallel) RMSD implementation
//****************************************************************************

/**
 * multi-threaded version of scby_compute_rmsd
 * @param n_threads - the number of threads to allocate
 * @param inc_by - the size of the work batch
 * @param in_bin_path - see scby_compute_rmsd
 * @param n_dims - see scby_compute_rmsd
 * @param out_bin_path - see scby_compute_rmsd
 * @param out_csv_path - see scby_compute_rmsd
 */
extern void scby_compute_rmsd_mt_front(const unsigned int n_threads,
                                       const unsigned int inc_by,
                                       const char *in_bin_path,
                                       const unsigned int n_dims,
                                       const char *out_bin_path,
                                       const char *out_csv_path);

#endif // SCBYCOMPUTE_PDIST_FRONT_H
