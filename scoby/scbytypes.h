/**
 * @author Alan Perez-Rathke
 * @date July 25, 2014
 */

#ifndef SCBYTYPES_H
#define SCBYTYPES_H

#include "scbybuild.h"

#include <limits>

// floating point type
typedef double scby_real;
#define as_scby_real(i) static_cast<scby_real>(i)

// maximum value of floating point type
#define SCBY_REAL_MAX std::numeric_limits<scby_real>::max()

// machine epsilon - difference between 1.0 and next value
// representable by this floating-point type
#define SCBY_REAL_EPS (as_scby_real(2.0) * std::numeric_limits<scby_real>::epsilon())

// unsigned int type
typedef unsigned int scby_uint;
#define as_scby_uint(i) static_cast<scby_uint>(i)

// boolean type
typedef scby_uint scby_bool;
#define SCBY_TRUE  1
#define SCBY_FALSE 0

// a symmetric matrix
#include <boost/numeric/ublas/symmetric.hpp>
typedef boost::numeric::ublas::symmetric_matrix<scby_real, boost::numeric::ublas::lower> scby_sym_matrix;

// a dynamic bitset
#include <boost/dynamic_bitset.hpp>
typedef boost::dynamic_bitset<> scbybitset;

// a cluster analysis tuple
#include <tuple>
typedef std::tuple<size_t, size_t, scby_real, std::vector<size_t> > scby_cluster_analysis_tuple;

// fast matrix types
#include "scbyarmadillo.h"

#endif // SCBYTYPES_H
