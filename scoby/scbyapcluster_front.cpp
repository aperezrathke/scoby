/**
 * Front-facing utilities for clustering with affinity propagation
 *
 * Based on affinity propagation utility within APCluster R package
 *
 * Frey, Brendan J., and Delbert Dueck. "Clustering by passing messages between data points."
 * science 315, no. 5814 (2007): 972-976.
 *
 * Bodenhofer, Ulrich, Andreas Kothmeier, and Sepp Hochreiter.
 * "APCluster: an R package for affinity propagation clustering." Bioinformatics 27, no. 17 (2011): 2463-2464.
 */

//****************************************************************************
// Includes
//****************************************************************************

#include "scbybuild.h"
#include "scbyassert.h"
#include "scbyapcluster_front.h"
#include "scbyapcluster.h"
#include "scbyvector_io.h"

//****************************************************************************
// namespace and utilities
//****************************************************************************

namespace {

    /**
     * Utility prints user arguments for stock clustering
     */
    void print_args(const scby_apcluster_bin_args_t &args) {
        std::cout << "scby_apcluster_bin parameters:\n";
        std::cout << "\tin_matrix_path: " << args.in_matrix_path << std::endl;
        std::cout << "\tn_dims: " << args.n_dims << std::endl;
        std::cout << "\tout_samples_to_clusters_path: " << args.out_samples_to_clusters_path << std::endl;
        std::cout << "\tout_clusters_to_samples_path: " << args.out_clusters_to_samples_path << std::endl;
        std::cout << "\tout_exemplars_path: " << args.out_exemplars_path << std::endl;
    }

} // end of helper namespace

/**
 * Utility exports results from affinity propagation
 */
void scby_apcluster_export_results(
    const scby_index_vec_t &samples_to_clusters,
    const scby_ap_c2s_map_t &clusters_to_samples,
    const scby_index_vec_t exemplars,
    const char* out_samples_to_clusters_path,
    const char* out_clusters_to_samples_path,
    const char* out_exemplars_path) {

    // export cluster assignments
    if (out_samples_to_clusters_path) {
        std::cout << "Writing cluster assignments to: " << out_samples_to_clusters_path << std::endl;
        scby_vector_write(samples_to_clusters, out_samples_to_clusters_path);
    }

    // export clusters to samples
    if (out_clusters_to_samples_path) {
        std::cout << "Writing clusters -> samples mapping to: " << out_clusters_to_samples_path << std::endl;
        scby_vector_write_2d(clusters_to_samples, out_clusters_to_samples_path);
    }

    // export exemplars
    if (out_exemplars_path) {
        std::cout << "Writing exemplar indices to: " << out_exemplars_path << std::endl;
        scby_vector_write(exemplars, out_exemplars_path);
    }
}

//****************************************************************************
// scby_apcluster_bin
//****************************************************************************

/**
 * Given input samples with coordinates in columns, will perform
 * stock apcluster routine and write results to disk
 */
void scby_apcluster_bin(const scby_apcluster_bin_args_t &args) {
    // Output arguments
    print_args(args);

    scby_matrix mat;
    // Load input matrix
    scby_load_mat_bin_and_reshape_col_maj(mat, args.in_matrix_path, args.n_dims);

    std::cout << "AP clustering...\n";
    scby_index_vec_t samples_to_clusters;
    scby_ap_c2s_map_t clusters_to_samples;
    scby_index_vec_t exemplars;
    scby_ap_timers timers;
    scby_ap_timers::begin_capture(&timers, e_scby_ap_timer_all);
    if (!scby_apcluster_pdist2(samples_to_clusters,
                              clusters_to_samples,
                              exemplars,
                              mat,
                              &timers)) {
        std::cout << "\tclustering failed to converge\n";
        return;
    }
    scby_ap_timers::end_capture(&timers, e_scby_ap_timer_all);
    std::cout << "\tclustering successfully converged\n";
    scby_ap_timers::report();

    scby_apcluster_export_results(
        samples_to_clusters,
        clusters_to_samples,
        exemplars,
        args.out_samples_to_clusters_path,
        args.out_clusters_to_samples_path,
        args.out_exemplars_path
    );
}
