/**
 * Streaming, hierarchical, weighted affinity propagation - a C++ port of the Matlab StrAP algorithm.
 * Uses reservoir size and exemplar count thresholds as criterion for re-running affinity propagation.
 *
 * Zhang, Xiangliang, Cyril Furtlehner, and Michele Sebag. "Data streaming with affinity propagation."
 * In Machine Learning and Knowledge Discovery in Databases, pp. 628-643. Springer Berlin Heidelberg, 2008.
 *
 * Zhang, Xiangliang, Cyril Furtlehner, Cecile Germain-Renaud, and Michele Sebag.
 * "Data stream clustering with affinity propagation." Knowledge and Data Engineering,
 * IEEE Transactions on 26, no. 7 (2014): 1644-1656.
 */

//****************************************************************************
// Includes
//****************************************************************************

#include "scbybuild.h"
#include "scbyassert.h"
#include "scbyshwap.h"
#include "scbycompute_pdist.h"

#include <algorithm>
#include <unordered_map>
#include <vector>

//****************************************************************************
// Utilities
//****************************************************************************

namespace {

    /**
     * Utility computes the pairwise squared distances for a contiguous subset of samples
     * (samples must be in columns)
     * @param pdist2_mat - the output symmetric squared distance matrix
     * @param positions_mat - each sample is a single column
     * @param first_col_ix - index of column containing first sample
     * @param num_samples - relative to first_col_ix, the number of samples to select
     *  will be min(num_samples, positions_mat.n_cols - first_col_x)
     * @param timers - (optional) used for timing the operation
     */
    void compute_pdist2_for_subset(scby_matrix &pdist2_mat,
                                   const scby_matrix &positions_mat,
                                   const scby_uint first_col_ix,
                                   const scby_uint num_samples,
                                   scby_ap_timers *timers=NULL) {
        scby_ap_timers::begin_capture(timers, e_scby_ap_timer_pdist);
        scby_assert_bounds(first_col_ix, 0, positions_mat.n_cols);
        scby_assert(num_samples >= 2);
        scby_assert((positions_mat.n_cols - first_col_ix) >= 2);
        const scby_matrix_utils::uword n_cols =
            std::min(static_cast<scby_matrix_utils::uword>(num_samples),
                     positions_mat.n_cols - static_cast<scby_matrix_utils::uword>(first_col_ix));
        // construct a "sub-matrix view" without allocating new memory
        const scby_matrix submat(
            const_cast<scby_real*>(positions_mat.colptr(first_col_ix)), /*ptr_aux_mem*/
            positions_mat.n_rows, /*n_rows*/
            n_cols, /*n_cols*/
            false, /*copy_aux_mem*/
            true /*strict*/
        );
        scby_assert(NULL != submat.memptr());
        scby_compute_pdist2(pdist2_mat, submat);
        scby_ap_timers::end_capture(timers, e_scby_ap_timer_pdist);
    }

    /**
     * Finds minimum squared distance (and associated column index) of a sample
     * to the first n columns of another matrix (e.g. an exemplar matrix)
     * @param min_col - the column index of the closest column in other_positions_mat to positions_mat[sample_col]
     * @param dist_vec - a buffer used for transient distance calculations
     * @param sample_col - the column index within position_mat to check against first n columns of other positions mat
     * @param other_positions_mat - will test sample column against first n columns of this matrix
     * @return the minimum squared distance found of the sample column against the first n column of other positions mat
     */
    scby_real find_min_dist2(scby_uint &min_col,
                             scby_vec_col &dist_vec,
                             const scby_matrix &positions_mat,
                             const scby_uint sample_col,
                             const scby_matrix &other_positions_mat,
                             const scby_uint first_n_col) {
        // Validate arguments
        scby_assert_bounds(sample_col, 0, positions_mat.n_cols);
        scby_assert_bounds(first_n_col, 1, other_positions_mat.n_cols+1);

        // Initialize by computing distance to first (0th) column
        scby_real min_dist2;
        SCBY_DIST2(min_dist2,
                   dist_vec,
                   positions_mat.unsafe_col(sample_col),
                   other_positions_mat.unsafe_col(0));
        min_col = 0;

        // Check remaining columns
        scby_real dist2;
        for (scby_uint i=1; i<first_n_col; ++i) {
            SCBY_DIST2(dist2,
                       dist_vec,
                       positions_mat.unsafe_col(sample_col),
                       other_positions_mat.unsafe_col(i));
            if (dist2 < min_dist2) {
                min_dist2 = dist2;
                min_col = i;
            }
        }
        scby_assert(min_dist2 >= as_scby_real(0.0));

        return min_dist2;
    }

    /**
     * Utility to check if a 2-D pairwise distance matrix can be allocated
     * for the given size
     * @param cluster_size - Will attempt to allocate cluster_size * cluster_size
     *  real types
     * @return True if test allocation successful, False otherwise
     */
    inline bool can_alloc_pdist_mat(const scby_uint cluster_size) {
        // http://stackoverflow.com/questions/24401422/how-does-armadillo-library-handle-error
        scby_matrix X;
        try {
            X.set_size(cluster_size, cluster_size);
        } catch (...) {
            return false;
        }
        return true;
    }

    /**
     * MEMORY HEAVY VERSION
     * Find minimal exemplar index - the node that is closest to all other nodes.
     * Utility called during refinement phase. Only should be called if pairwise
     *  distance matrix can be allocated. Uses caching of previous distance
     *  calculations to avoid redundant operations at the expense of O(n^2) memory.
     * @param i_clust - index of cluster to find exemplar for
     * @param cluster_to_samples - mapping from clusters to samples
     * @param positions_mat - positions matrix
     * @return index of element that is exemplar for this cluster
     */
    scby_uint find_min_ex_ix_cached(
          const scby_uint i_clust
        , const scby_ap_c2s_map_t &clusters_to_samples
        , const scby_matrix &positions_mat
    ) {

        scby_matrix pdist2s;
        scby_vec_col dist2_vec;
        scby_real min_sum_dist2;
        scby_real sum_dist2;
        scby_uint min_ex_ix;

        const scby_uint cluster_size = as_scby_uint(clusters_to_samples[i_clust].size());

        // assuming clusters are reasonably small such that we can store all pairwise distances
        pdist2s.set_size(cluster_size, cluster_size);
        min_sum_dist2 = SCBY_REAL_MAX;
        // find exemplar for this set
        for (scby_uint i_elem = 0; i_elem < cluster_size; ++i_elem) {
            sum_dist2 = as_scby_real(0.0);
            const scby_uint i_ix = clusters_to_samples[i_clust][i_elem];
            scby_assert_bounds(i_ix, 0, positions_mat.n_cols);
            // these distances are already pre-computed, so avoid re-computing them
            for (scby_uint j_elem = 0; j_elem < i_elem; ++j_elem) {
                sum_dist2 += pdist2s.at(i_elem, j_elem);
            }
            // these distances have not been computed yet, so compute them
            for (scby_uint j_elem = i_elem + 1; j_elem < cluster_size; ++j_elem) {
                const scby_uint j_ix = clusters_to_samples[i_clust][j_elem];
                scby_assert_bounds(j_ix, 0, positions_mat.n_cols);
                // cache distance
                SCBY_DIST2(pdist2s.at(i_elem, j_elem),
                           dist2_vec,
                           positions_mat.unsafe_col(i_ix),
                           positions_mat.unsafe_col(j_ix));
                pdist2s.at(j_elem, i_elem) = pdist2s.at(i_elem, j_elem);
                sum_dist2 += pdist2s.at(i_elem, j_elem);
            }
            // Check if new candidate exemplar
            if (sum_dist2 < min_sum_dist2) {
                min_sum_dist2 = sum_dist2;
                min_ex_ix = i_ix;
            }
        }

        return min_ex_ix;
    }

    /**
     * MEMORY LIGHT VERSION
     * Find minimal exemplar index - the node that is closest to all other nodes.
     * Utility called during refinement phase. Only should be called if pairwise
     *  distance matrix cannot be allocated. Does not perform distance caching and
     *  therefore is memory efficient but ~2 X slower than cached version.
     * @param i_clust - index of cluster to find exemplar for
     * @param cluster_to_samples - mapping from clusters to samples
     * @param positions_mat - positions matrix
     * @return index of element that is exemplar for this cluster
     */
    scby_uint find_min_ex_ix_nocache(
          const scby_uint i_clust
        , const scby_ap_c2s_map_t &clusters_to_samples
        , const scby_matrix &positions_mat
    ) {

        scby_vec_col dist2_vec;
        scby_real dist2;
        scby_real min_sum_dist2;
        scby_real sum_dist2;
        scby_uint min_ex_ix;

        const scby_uint cluster_size = as_scby_uint(clusters_to_samples[i_clust].size());

        min_sum_dist2 = SCBY_REAL_MAX;
        // find exemplar for this set
        for (scby_uint i_elem = 0; i_elem < cluster_size; ++i_elem) {
            sum_dist2 = as_scby_real(0.0);
            const scby_uint i_ix = clusters_to_samples[i_clust][i_elem];
            scby_assert_bounds(i_ix, 0, positions_mat.n_cols);
            // Compute distance to all nodes - note no caching
            // so all distances between nodes (a,b) are computed twice!
            // Therefore, if memory is available, the memory heavy version
            // should always be preferred.
            for (scby_uint j_elem = 0; j_elem < cluster_size; ++j_elem) {
                const scby_uint j_ix = clusters_to_samples[i_clust][j_elem];
                scby_assert_bounds(j_ix, 0, positions_mat.n_cols);
                SCBY_DIST2(dist2,
                           dist2_vec,
                           positions_mat.unsafe_col(i_ix),
                           positions_mat.unsafe_col(j_ix));
                sum_dist2 += dist2;
            }
            // Check if new candidate exemplar
            if (sum_dist2 < min_sum_dist2) {
                min_sum_dist2 = sum_dist2;
                min_ex_ix = i_ix;
            }
        }

        return min_ex_ix;
    }

} // end of helper namespace

//****************************************************************************
// SampleInfo
//****************************************************************************

/**
 * Stores information (meta-data) about each sample.
 */
struct scbyShwapSampleInfo {
    /**
     * The index into the global data stream (timestamp encountered)
     */
    scby_uint index;

    /**
     * Multiplicity - number of samples (including exemplar) associated
     * with each sample
     */
    scby_real n;

    /**
     * Timestamp - keeps track of when a sample was last associated to this exemplar
     */
    scby_uint touch;

    /**
     * Approximates average distance between exemplar and samples associated to an
     * exemplar (excluding the exemplar itself) - corresponds to mui variable in original
     * StrAP code.
     */
    scby_real mean_dist;

    /**
     * Approximates sum of distances between exemplar and samples associated to
     * exemplar (excluding the exemplar itself) - corresponds to mui2 variable in original
     * StrAP code.
     */
    scby_real sum_dist;

    /**
     * Approximates the sum of squared distances between exemplar and samples associated
     * to this exemplar (excluding the exemplar itself) - corresponds to sigma2 variable in
     * original StrAP code.
     */
    scby_real sum_dist2;

    /**
     * Approximates a term similar to the standard deviation from the average distance to
     * the exemplar - corresponds to sigma variable in original StrAP code
     */
    scby_real stddev_dist;
};

namespace {

    /**
     * Performs basic sanity checks on sample meta-data
     */
    bool check_info(const scbyShwapSampleInfo &info) {
        bool result = info.n > as_scby_real(0.0);
        scby_assert(result);
        result &= info.mean_dist >= as_scby_real(0.0);
        scby_assert(result);
        result &= info.sum_dist >= as_scby_real(0.0);
        scby_assert(result);
        result &= info.sum_dist2 >= as_scby_real(0.0);
        scby_assert(result);
        result &= info.stddev_dist >= as_scby_real(0.0);
        scby_assert(result);
        return result;
    }

} // end of helper namespace

//****************************************************************************
// ExAssocHeurNascUnivAvgDist2
//****************************************************************************

/**
 * Exemplar Association Heuristic Nascent Universal Average Squared Distance
 * Heuristic for associating an unlabeled sample to a cluster:
 * - The heuristic finds the universal average squared distance among all samples to their exemplar
 * - This average squared distance is fixed based on the initial build set of samples for remainder of clustering run
 * - An unlabeled sample is associated to an exemplar if it's within squared distance threshold
 */
class scbyShwapExAssocHeurNascUnivAvgDist2 {
public:

    /**
     * Default constructor
     */
    scbyShwapExAssocHeurNascUnivAvgDist2()
        : m_dist2_cutoff(as_scby_real(0.0))
        , m_num_samples(0)
    {}

    /**
     * Used for training the cutoff threshold
     * @param cluster_id - the identifier for the cluster associated to param info
     * @param info - information on cluster such as mean distances
     */
    void update_build(const scby_uint cluster_id, const scbyShwapSampleInfo &info) {
        scby_assert(check_info(info));
        m_dist2_cutoff += info.sum_dist2;
        if (info.n > as_scby_real(1.0)) {
            m_num_samples += (info.n - as_scby_real(1.0)); // exclude self distance
        }
    }

    /**
     * Finalizes cutoff threshold after initial training build pass is complete
     * @param ex_assoc_heur_scale - final cutoff distance is scaled by this factor
     */
    void on_build_finish(const scby_real ex_assoc_heur_scale) {
        if (m_num_samples > 0) {
            m_dist2_cutoff /= m_num_samples;
            m_dist2_cutoff *= ex_assoc_heur_scale;
        } else {
            // trap this situation
            scby_assert(false);
        }
    }

    /**
     * Method for testing if an unlabeled sample can be added to parameter cluster
     * @param cluster_id - the identifier for the cluster to test for association
     * @param dist2 - the squared distance from unlabeled sample to cluster exemplar
     * @return True if sample info can be associated to param cluster_id, False otherwise
     */
    inline scby_bool should_assoc(const scby_uint cluster_id, const scby_real dist2) {
        scby_assert(m_dist2_cutoff >= as_scby_real(0.0));
        scby_assert(dist2 >= as_scby_real(0.0));
        return dist2 <= m_dist2_cutoff;
    }

private:

    /**
     * The squared distance cutoff - if an unlabeled sample is at least this close to an exemplar, then it
     * may be associated to that exemplar
     */
    scby_real m_dist2_cutoff;

    /**
     * Only used for training, the number of samples used for calculating the nascent, universal squared
     * distance threshold
     */
    scby_real m_num_samples;
};

//****************************************************************************
// ExAssocHeurNascUnivMedAvgDist2
//****************************************************************************

/**
 * Exemplar Association Heuristic Nascent Universal Median Average Squared Distance
 * Heuristic for associating an unlabeled sample to a cluster:
 * - The heuristic finds the median average squared distance among all samples to their exemplar
 * - This squared distance is fixed based on the initial build set of samples for remainder of clustering run
 * - An unlabeled sample is associated to an exemplar if it's within squared distance threshold
 */
class scbyShwapExAssocHeurNascUnivMedAvgDist2 {
public:

    /**
     * Default constructor
     */
    scbyShwapExAssocHeurNascUnivMedAvgDist2()
        : m_dist2_cutoff(as_scby_real(0.0))
    {}

    /**
     * Used for training the cutoff threshold
     * @param cluster_id - the identifier for the cluster associated to param info
     * @param info - information on cluster such as mean distances
     */
    void update_build(const scby_uint cluster_id, const scbyShwapSampleInfo &info) {
        scby_assert(check_info(info));
        if (info.n > as_scby_real(1.0)) {
            const scby_real mean_dist2 = info.sum_dist2 / (info.n - as_scby_real(1.0));
            m_mean_dist2s.push_back(mean_dist2);
        } else {
            m_mean_dist2s.push_back(as_scby_real(0.0));
        }
    }

    /**
     * Finalizes cutoff threshold after initial training build pass is complete
     * @param ex_assoc_heur_scale - final cutoff distance is scaled by this factor
     */
    void on_build_finish(const scby_real ex_assoc_heur_scale) {
        if (!m_mean_dist2s.empty()) {
            // find element corresponding to q-th quantile
            const scby_uint q_index = as_scby_uint((as_scby_real(m_mean_dist2s.size()-1) * as_scby_real(0.5)));
            scby_assert_bounds(q_index, 0, m_mean_dist2s.size());
            std::nth_element(m_mean_dist2s.begin(), m_mean_dist2s.begin() + q_index, m_mean_dist2s.end());
            m_dist2_cutoff = ex_assoc_heur_scale * m_mean_dist2s[q_index];
            m_mean_dist2s.clear();
        } else {
            // trap this situation
            scby_assert(false);
        }
    }

    /**
     * Method for testing if an unlabeled sample can be added to parameter cluster
     * @param cluster_id - the identifier for the cluster to test for association
     * @param dist2 - the squared distance from unlabeled sample to cluster exemplar
     * @return True if sample info can be associated to param cluster_id, False otherwise
     */
    inline scby_bool should_assoc(const scby_uint cluster_id, const scby_real dist2) {
        scby_assert(m_dist2_cutoff >= as_scby_real(0.0));
        scby_assert(dist2 >= as_scby_real(0.0));
        return dist2 <= m_dist2_cutoff;
    }

private:

    /**
     * The squared distance cutoff - if an unlabeled sample is at least this close to an exemplar, then it
     * may be associated to that exemplar
     */
    scby_real m_dist2_cutoff;

    /**
     * Only used for training, collection of average squared distances for each exemplar. The final distance threshold
     * is the median of these values.
     */
    std::vector<scby_real> m_mean_dist2s;
};

//****************************************************************************
// Reservoir
//****************************************************************************

class scbyShwapReservoir {

public:

    /**
     * Default constructor
     */
    scbyShwapReservoir() : m_num_samples(0) {}

    /**
     * Does not preserve data! Meant to be used for allocating initial buffer size.
     * @param n_rows - number of rows to initialize reservoir buffer
     * @param n_cols - number of cols to initialize reservoir buffer
     */
    void reserve(const scby_uint n_rows, const scby_uint n_cols) {
        scby_assert(n_rows >= 1);
        scby_assert(n_cols >= 3);
        m_samples.set_size(n_rows, n_cols);
        m_infos.resize(n_cols);
    }

    /**
     * Appends sample to reservoir
     * @param p_sample - pointer to sample data
     * @param info - information about sample to allow mapping to global data stream
     *  and for streaming cluster updates
     */
    void push_back(const scby_real *p_sample,
                   const scbyShwapSampleInfo &info) {
        scby_assert(NULL != p_sample);
        scby_assert(check_info(info));
        // Check if we can fit element
        if (m_num_samples >= m_samples.n_cols) {
            // Not enough space - need to allocate more storage
            scby_assert(m_num_samples == m_samples.n_cols);
            const scby_uint new_max_num_samples = m_samples.n_cols * 2;
            m_samples.resize(m_samples.n_rows, new_max_num_samples);
            m_infos.resize(new_max_num_samples);
        }

        // Append sample
        scby_assert_bounds(m_num_samples, 0, m_samples.n_cols);
        scby_assert(m_samples.n_cols == m_infos.size());
        memcpy(
            m_samples.colptr(m_num_samples),
            p_sample,
            sizeof(scby_real) * m_samples.n_rows
        );
        m_infos[m_num_samples++] = info;
    }

    /**
     * Clears reservoir - does not free backing store
     */
    inline void clear() {
        m_num_samples = 0;
    }

    /**
     * @return number of samples currently in reservoir
     */
    inline scby_uint size() const {
        return m_num_samples;
    }

    /**
     * @return reference to underlying sample positions (note matrix n_cols >= this->size())
     */
    inline scby_matrix& get_samples() {
        return m_samples;
    }

    /**
     * @return const reference to underlying sample positions (note matrix n_cols >= this->size())
     */
    inline const scby_matrix& get_samples() const {
        return m_samples;
    }

    /**
     * @return reference to infos buffer (note infos.size() >= this->size())
     */
    inline std::vector<scbyShwapSampleInfo>& get_infos() {
        return m_infos;
    }

    /**
     * @return const reference to front infos buffer (note infos.size() >= this->size())
     */
    inline const std::vector<scbyShwapSampleInfo>& get_infos() const {
        return m_infos;
    }

    /**
     * Removes all exemplars from reservoir that have not had a sample assigned within the decay interval.
     * Exemplars are assumed to be first num_exemplars columns.
     * @param tstamp - current timestamp iteration to check for stale exemplars
     * @param decay_window - a sample must be associated to exemplar within this window, else be culled
     * @param num_exemplars - the count of first set of columns designated as exemplars
     * @return number of exemplars removed
     */
    scby_uint remove_stale_exemplars(const scby_uint tstamp,
                                     const scby_uint decay_window,
                                     const scby_uint num_exemplars) {
        scby_assert(decay_window > 0);
        scby_assert(num_exemplars > 0);
        scby_uint i, j;
        // Find first stale exemplar
        scby_uint num_stale = 0;
        scby_assert(num_exemplars <= this->size());
        scby_assert(this->size() <= m_infos.size());
        scby_uint touch_interval;
        for (i=0; i<num_exemplars; ++i) {
            const scbyShwapSampleInfo &info = m_infos[i];
            scby_assert(info.touch <= tstamp);
            touch_interval = tstamp - info.touch;
            if (touch_interval >= decay_window) {
                ++num_stale;
                break;
            }
        }

        // At least 1 stale exemplar found, so remove it
        if (i != num_exemplars) {
            const scby_uint num_bytes_per_sample = sizeof(scby_real) * m_samples.n_rows;
            for (j=i; ++j < num_exemplars; ) {
                const scbyShwapSampleInfo &info = m_infos[j];
                scby_assert(info.touch <= tstamp);
                touch_interval = tstamp - info.touch;
                if (touch_interval < decay_window) {
                    memcpy(m_samples.colptr(i),
                           m_samples.colptr(j),
                           num_bytes_per_sample);
                    m_infos[i] = m_infos[j];
                    ++i;
                } else {
                    ++num_stale;
                }
            }

            scby_assert(num_stale <= m_num_samples);
            scby_assert_bounds(i, 0, num_exemplars);

            // bulk move remaining samples
            if (num_exemplars < m_num_samples) {
                const scby_uint num_non_exemplars = m_num_samples - num_exemplars;
                memmove(m_samples.colptr(i),
                        m_samples.colptr(num_exemplars),
                        num_bytes_per_sample * num_non_exemplars);
                memmove(&(m_infos[i]),
                        &(m_infos[num_exemplars]),
                        sizeof(scbyShwapSampleInfo) * num_non_exemplars);
            }

            // update samples count
            m_num_samples -= num_stale;
        }

        return num_stale;
    }

    /**
     * Pushes all exemplar positions and info to front of reservoir
     * Note: exemplars indices are sorted and inserted into reservoir in this order
     * (therefore, any cluster associations are not preserved)
     * @param exemplars_ - vector containing indices of exemplars (assumed to be sorted)
     */
    void defragment(const scby_index_vec_t &exemplars_) {
        const scby_index_vec_t exemplars = sort(exemplars_, "ascend");
        scby_assert(exemplars.is_sorted("ascend"));
        scby_assert(exemplars.n_elem <= this->size());
        scby_assert(this->size() <= m_infos.size());
        scby_assert(m_infos.size() == m_samples.n_cols);
        const scby_uint num_bytes_per_sample = sizeof(scby_real) * m_samples.n_rows;
        scby_uint res_index_front = 0;
        for (scby_uint i_ex=0; i_ex<exemplars.n_elem; ++i_ex) {
            const scby_uint res_index_ex = exemplars[i_ex];
            scby_assert_bounds(res_index_ex, 0, this->size());
            // skip copies if exemplar is already in proper position
            if (res_index_front < res_index_ex) {
                // move position
                scby_assert_bounds(res_index_front, 0, this->size());
                memcpy(m_samples.colptr(res_index_front),
                       m_samples.colptr(res_index_ex),
                       num_bytes_per_sample);
                // move info
                m_infos[res_index_front] = m_infos[res_index_ex];
            } else {
                scby_assert(res_index_front == res_index_ex);
            }
            ++res_index_front;
        }
        m_num_samples = exemplars.n_elem;
    }

private:

    /**
     * Keeps track of number of samples in reservoir
     */
    scby_uint m_num_samples;

    /**
     * Each column represents a single sample
     */
    scby_matrix m_samples;

    /**
     * Stores meta-data associated with each sample
     */
    std::vector<scbyShwapSampleInfo> m_infos;
};

//****************************************************************************
// ShwaperPdist2
//****************************************************************************

/**
 * Class is result of method object refactoring of scbyshwap_pdist2
 * Performs hierarchical, weighted affinity propagation for clustering large data set.
 *  - based on StrAP algorithm.
 */
template <typename assoc_heur_t>
class scbyShwaperPdist2 {
public:

    /**
     * Constructor - stores all arguments as private member variables
     */
    scbyShwaperPdist2(scby_index_vec_t &samples_to_clusters,
                      scby_ap_c2s_map_t &clusters_to_samples,
                      scby_index_vec_t &exemplars,
                      scby_matrix &positions_mat,
                      scby_ap_timers *timers,
                      const scby_uint init_size,
                      const scby_real ex_assoc_heur_scale,
                      const scby_uint max_unlabeled_thresh,
                      const scby_uint max_exemplars_thresh,
                      const scby_uint decay_window,
                      const scby_real q,
                      const size_t maxits,
                      const size_t convits,
                      const scby_real lam,
                      const bool nonoise,
                      const scby_real p_conv_decay_fact,
                      const scby_uint p_conv_max_attempts,
                      const scby_real init_swiz_max_attempts,
                      const bool restore_positions_mat)
        : m_samples_to_clusters(samples_to_clusters)
        , m_clusters_to_samples(clusters_to_samples)
        , m_exemplars(exemplars)
        , m_positions_mat(positions_mat)
        , m_timers(timers)
        , m_init_size(init_size)
        , m_ex_assoc_heur_scale(ex_assoc_heur_scale)
        , m_max_unlabeled_thresh(max_unlabeled_thresh)
        , m_max_exemplars_thresh(max_exemplars_thresh)
        , m_decay_window(decay_window)
        , m_q(q)
        , m_maxits(maxits)
        , m_convits(convits)
        , m_lam(lam)
        , m_nonoise(nonoise)
        , m_num_exemplars(0)
        , m_num_unlabeled(0)
        , m_p_conv_decay_fact(p_conv_decay_fact)
        , m_p_conv_max_attempts(p_conv_max_attempts)
        , m_init_swiz_max_attempts(init_swiz_max_attempts)
        , m_restore_positions_mat(restore_positions_mat)
        , m_swizzled(false) {
        // Validate arguments
        scby_assert(check_args());
        // Pre-allocate reservoir
        m_reservoir.reserve(positions_mat.n_rows, max_unlabeled_thresh + max_exemplars_thresh);
    }

    /**
     * Performs hierarchical, weighted affinity propagation
     */
    bool cluster() {
        std::cout << "Finding initial clusters.\n";
        if (!init_apcluster()) {
            return false;
        }

        std::cout << "Building initial cluster model.\n";
        build_clusters();
        std::cout << "Initial cluster model built with " << m_num_exemplars << " exemplars.\n";

        std::cout << "Mining exemplars.\n";
        mine_exemplars();

        std::cout << "Refining clusters.\n";
        refine_clusters();

        return true;
    }

private:

    /**
     * Verifies data members are in valid state
     */
    bool check_args() const {
        bool result = m_positions_mat.n_cols >= 3;
        scby_assert(result);
        result &= m_init_size >= 3;
        scby_assert(result);
        result &= m_max_unlabeled_thresh > 0;
        scby_assert(result);
        result &= m_max_exemplars_thresh > 0;
        scby_assert(result);
        result &= m_decay_window > 0;
        scby_assert(result);
        result &= m_q >= as_scby_real(0.0);
        scby_assert(result);
        result &= m_q <= as_scby_real(1.0);
        scby_assert(result);
        result &= m_maxits > 0;
        scby_assert(result);
        result &= m_convits > 0;
        scby_assert(result);
        result &= m_lam >= as_scby_real(0.5);
        scby_assert(result);
        result &= m_lam <= as_scby_real(1.0);
        scby_assert(result);
        result &= m_p_conv_decay_fact > as_scby_real(0.0);
        scby_assert(result);
        return result;
    }

    /**
     * Verifies basic assumptions of a successful AP call
     */
    bool check_apresults(const scby_uint max_num_samples) const {
        bool result = m_samples_to_clusters.n_elem <= max_num_samples;
        scby_assert(result);
        result &= m_clusters_to_samples.size() > 0;
        scby_assert(result);
        result &= m_exemplars.n_elem == m_clusters_to_samples.size();
        scby_assert(result);
        return result;
    }

    /**
     * Performs initial AP clustering
     */
    bool init_apcluster() {
        scby_assert(m_init_swiz_max_attempts >= 0);

        m_swizzled = false;
        // Initialize mapping
        m_swiz_ix_2_init_ix.set_size(m_positions_mat.n_cols);
        for (scby_index_vec_t::elem_type i = 0; i < m_positions_mat.n_cols; ++i) {
            m_swiz_ix_2_init_ix.at(i) = i;
        }

        for (scby_uint i_attempt = 0; i_attempt <= m_init_swiz_max_attempts; ++i_attempt) {
            if (i_attempt > 0) {
                std::cout << "\t" << i_attempt << ": Initial AP run failed. Attempting with new ensemble by swizzling inputs.\n";
                m_swizzled = true;
                // Swizzle mapping
                std::rotate(m_swiz_ix_2_init_ix.begin(),
                            m_swiz_ix_2_init_ix.begin() + m_init_size,
                            m_swiz_ix_2_init_ix.end());

                // Swizzle positions
                m_positions_mat = scby_matrix_utils::shift(m_positions_mat,
                                  -static_cast<int>(m_init_size),
                                  1 /* shift along rows */);
            }

            // Compute pairwise similarities as the negative euclidean squared distance
            compute_pdist2_for_subset(m_s, m_positions_mat, 0, m_init_size, m_timers);
            m_s *= as_scby_real(-1.0);

            // Initialize preferences based on quantile similarity
            scby_apcluster_util_qs_prefs_lower_tri(m_p, m_s, m_q);

            // Perform affinity propagation
            if (scby_apcluster(m_samples_to_clusters,
                               m_clusters_to_samples,
                               m_exemplars,
                               m_s,
                               m_p,
                               m_timers,
                               m_maxits,
                               m_convits,
                               m_lam,
                               m_nonoise)) {
                std::cout << "\t" << i_attempt << ": Initial AP run successful.\n";
                scby_assert(m_exemplars.n_elem > 0);
                return true;
            }
        }

        std::cout << "\t" << "Initial AP run failed. Max attempts reached.\n";
        return false;
    }

    /**
     * Using results from initial AP clustering, will initialize reservoir and exemplar
     * meta-data for streaming phase
     */
    void build_clusters() {
        scby_assert(check_apresults(m_init_size));
        scby_assert(m_num_exemplars == 0);
        scby_assert(m_num_unlabeled == 0);
        scby_assert(m_reservoir.size() == 0);
        // Add each exemplar to the reservoir
        scbyShwapSampleInfo info;
        scby_vec_col dist_vec;
        scby_real dist2;
        for (scby_uint i_ex =0; i_ex < m_exemplars.n_elem; ++i_ex) {
            // set global index for this exemplar
            info.index = m_exemplars[i_ex];
            scby_assert_bounds(info.index, 0, m_positions_mat.n_cols);

            // set multiplicity for this exemplar
            const scby_uint n = as_scby_uint(m_clusters_to_samples[i_ex].size());
            info.n = as_scby_real(n);

            // set timestamp
            scby_assert(m_init_size >= 3);
            info.touch = (m_init_size-1);

            // compute sum of distance of associated samples to this exemplar
            info.sum_dist = as_scby_real(0.0);
            info.sum_dist2 = as_scby_real(0.0);
            for (scby_uint i_elem=0; i_elem < n; ++i_elem) {
                SCBY_DIST2(dist2, dist_vec, m_positions_mat.unsafe_col(i_elem), m_positions_mat.unsafe_col(info.index));
                scby_assert(dist2 >= as_scby_real(0.0));
                info.sum_dist2 += dist2;
                info.sum_dist += sqrt(dist2);
            }

            // compute average distance of all other associated samples to this exemplar
            if (n > 1) {
                info.mean_dist = info.sum_dist / (info.n - as_scby_real(1.0));
            } else {
                info.mean_dist = as_scby_real(0.0);
            }

            // compute "standard deviation" of distance to this exemplar
            if (n > 2) {
                info.stddev_dist = sqrt(
                                       fabs((info.sum_dist2 - (info.mean_dist * info.mean_dist) * (info.n - as_scby_real(1.0))))
                                       / (info.n - as_scby_real(2.0))
                                   );
            } else {
                info.stddev_dist = as_scby_real(0.0);
            }

            // update association heuristic for future samples
            m_assoc_heur.update_build(i_ex, info);

            // add exemplar to reservoir
            m_reservoir.push_back(m_positions_mat.colptr(info.index), info);
            ++m_num_exemplars;
        }

        // finalize association heuristic
        m_assoc_heur.on_build_finish(m_ex_assoc_heur_scale);

        scby_assert(m_num_exemplars > 0);
        scby_assert(m_num_exemplars == m_exemplars.n_elem);
        scby_assert(m_reservoir.size() == m_num_exemplars);
    }

    /**
     * Streaming pass through remaining data samples to find exemplars
     * @return True if successful, False o/w
     */
    bool mine_exemplars() {
        // distance to closest exemplar
        scby_real min_ex_dist2;
        // column index of closest exemplar within reservoir
        scby_uint min_ex_col;
        // buffer used for distance calculations
        scby_vec_col dist_vec;
        // keep track of span between sample associations to an exemplar - corresponds to ddelt from StrAP code
        scby_uint touch_interval = 0;
        // used for degrading exemplars based on number of samples associated to exemplar
        // - corresponds to temon from StrAP code
        scby_real fact_size;
        // used for degrading exemplars based on timestamp span
        scby_real fact_timespan;
        // info struct for adding unlabeled samples to reservoir
        scbyShwapSampleInfo info;
        for (scby_uint i_unlab=m_init_size,
                // keep track of number of samples remaining to be processed
                num_remaining=((m_init_size <= m_positions_mat.n_cols) ? (m_positions_mat.n_cols - m_init_size) : 0);
                i_unlab<m_positions_mat.n_cols;
                ++i_unlab, --num_remaining) {

            // avoid processing remaining samples if it is no longer possible to trigger
            // another affinity propagation pass - the final refinement step will
            // assign these samples to nearest exemplar
            if ((num_remaining + m_num_unlabeled) < m_max_unlabeled_thresh) {
                break;
            }

            // determine nearest exemplar for current unlabeled sample
            min_ex_dist2 = find_min_dist2(min_ex_col,
                                          dist_vec,
                                          m_positions_mat,
                                          i_unlab,
                                          m_reservoir.get_samples(),
                                          m_num_exemplars);
            scby_assert_bounds(min_ex_col, 0, m_num_exemplars);

            // see if sample is close enough to be associated with this exemplar
            if (m_assoc_heur.should_assoc(min_ex_col, min_ex_dist2)) {
                // update associated cluster info
                scby_assert_bounds(min_ex_col, 0, m_reservoir.size());
                scby_assert(m_reservoir.size() <= m_reservoir.get_infos().size());
                scbyShwapSampleInfo &ex_info = m_reservoir.get_infos()[min_ex_col];
                scby_assert(i_unlab > ex_info.touch);
                touch_interval = i_unlab - ex_info.touch;
                scby_assert(touch_interval > 0);
                // check if it's been too long since a sample was associated, in this case "reset" cluster
                if (touch_interval >= m_decay_window) {
                    ex_info.n = as_scby_real(1.0);
                    ex_info.mean_dist = as_scby_real(0.0);
                    ex_info.sum_dist = sqrt(min_ex_dist2);
                    ex_info.sum_dist2 = min_ex_dist2;
                    ex_info.stddev_dist = as_scby_real(0.0);
                } else {
                    // else, cluster is not too old, update cluster details
                    fact_size = ex_info.n / (ex_info.n + as_scby_real(1.0));
                    scby_assert((m_decay_window + touch_interval) > 0);
                    fact_timespan = as_scby_real(m_decay_window) / as_scby_real((m_decay_window + touch_interval));
                    ex_info.n *= fact_timespan;
                    ex_info.n += fact_size;
                    ex_info.sum_dist *= fact_timespan;
                    ex_info.sum_dist += (fact_size * sqrt(min_ex_dist2));
                    ex_info.sum_dist2 *= fact_timespan;
                    ex_info.sum_dist2 += (fact_size * min_ex_dist2);

                    if (ex_info.n > (as_scby_real(1.0) + SCBY_REAL_EPS)) {
                        ex_info.mean_dist = ex_info.sum_dist / (ex_info.n - as_scby_real(1.0));
                    } else {
                        ex_info.mean_dist = as_scby_real(0.0);
                    }

                    if (ex_info.n > (as_scby_real(2.0) + SCBY_REAL_EPS)) {
                        // warning: fact_size is modified here
                        fact_size = (ex_info.n - as_scby_real(1.0)) / (ex_info.n - as_scby_real(2.0));
                        ex_info.stddev_dist = fabs(
                                                  ex_info.sum_dist2 - (ex_info.mean_dist * ex_info.mean_dist) * fact_size
                                              );
                        ex_info.stddev_dist = sqrt(ex_info.stddev_dist);
                    } else {
                        ex_info.stddev_dist = as_scby_real(0.0);
                    }
                }
                ex_info.touch = i_unlab;
            } else {
                // push sample to reservoir
                info.index = i_unlab;
                info.n = 1;
                info.touch = i_unlab;
                info.mean_dist = as_scby_real(0.0);
                info.sum_dist = as_scby_real(0.0);
                info.sum_dist2 = as_scby_real(0.0);
                info.stddev_dist = as_scby_real(0.0);

                m_reservoir.push_back(m_positions_mat.colptr(i_unlab), info);
                ++m_num_unlabeled;
                scby_assert((m_num_exemplars + m_num_unlabeled) == m_reservoir.size());
            }

            // determine if reservoir is full
            if (m_num_unlabeled >= m_max_unlabeled_thresh) {
                if (!wapcluster(i_unlab)) {
                    return false;
                }
            }

            // determine if there are too many exemplars - if so, attempt another affinity
            // propagation pass to hopefully reduce number of exemplars. Also, each call
            // to wapcluster will decay exemplar multiplicities making it less likely to
            // be selected as an exemplar again. Note, am checking this every iteration
            // to mimic "hierarchical/recursive" affinity propagation.
            if (m_num_exemplars >= m_max_exemplars_thresh) {
                if (!wapcluster(i_unlab)) {
                    return false;
                }
            }

        } // end iteration over remaining samples

        return true;
    }

    /**
     * Weighted affinity propagation. Exemplar info is decayed according to timestamp
     * and similarities and preferences are modulated by exemplar multiplicities.
     * @param tstamp - current timestamp iteration to check for stale exemplars and for
     *  decaying exemplar info
     * @return True if affinity propagation converged, False o/w
     */
    bool wapcluster(const scby_uint tstamp) {
        std::cout << "Weighted AP: called at timestamp " << tstamp << "\n"
                  << "\tnum_exemplars: " << m_num_exemplars << std::endl
                  << "\tnum_unlabeled: " << m_num_unlabeled << std::endl
                  << "\tnum_reservoir: " << m_reservoir.size() << std::endl;
        scby_assert(m_reservoir.size() == (m_num_exemplars + m_num_unlabeled));

        // @TODO - make removal optional
        remove_stale_exemplars(tstamp);
        std::cout << "Weighted AP: finished stale exemplar removal.\n"
                  << "\tnum_exemplars: " << m_num_exemplars << std::endl;

        // Decay exemplar info
        scby_uint touch_interval;
        scby_real fact_timespan;
        for (scby_uint i_ex=0; i_ex<m_num_exemplars; ++i_ex) {
            scbyShwapSampleInfo &ex_info = m_reservoir.get_infos()[i_ex];
            scby_assert(tstamp > ex_info.touch);
            touch_interval = tstamp - ex_info.touch;
            scby_assert(touch_interval > 0);
            fact_timespan = as_scby_real(m_decay_window) / as_scby_real((m_decay_window + touch_interval));
            ex_info.n *= fact_timespan;
            ex_info.sum_dist *= fact_timespan;
            ex_info.sum_dist2 *= fact_timespan;
        }

        // Compute similarity scores
        compute_pdist2_for_subset(m_s, m_reservoir.get_samples(), 0, m_reservoir.size(), m_timers);
        m_s *= as_scby_real(-1.0);

        // Multiply each exemplar similarity row by its multiplicity
        scby_assert(m_s.n_cols == m_s.n_rows);
        scby_assert(m_s.n_rows == m_reservoir.size());
        scby_assert(m_num_exemplars < m_s.n_rows);
        for (scby_uint i_ex=0; i_ex<m_num_exemplars; ++i_ex) {
            scby_assert_bounds(i_ex, 0, m_reservoir.size());
            scby_assert(m_reservoir.size() <= m_reservoir.get_infos().size());
            const scby_real n = m_reservoir.get_infos()[i_ex].n;
            m_s.row(i_ex) *= n;
        }

        // Compute preferences
        scby_apcluster_util_qs_prefs_no_diag(m_p, m_s, m_q);

        // Store the default quantile preference
        scby_real p_q = m_p.at(0);

        scby_vec_col p_delta = m_p;

        // Modify existing exemplar preferences by multiplicity and mean distance
        for (scby_uint i_ex=0; i_ex<m_num_exemplars; ++i_ex) {
            scbyShwapSampleInfo &ex_info = m_reservoir.get_infos()[i_ex];
            m_p.at(i_ex) += ( (ex_info.n - as_scby_real(1.0)) * ex_info.mean_dist );
        }

        // Determine the delta preferences relative to the default
        p_delta = m_p - p_q;

        // Perform affinity propagation
        const scby_matrix s_prev(m_s);
        const scby_vec_col p_prev(m_p);
        bool b_conv = false;
        for (scby_uint i_trial = 0; i_trial <= m_p_conv_max_attempts; ++i_trial) {
            std::cout << "Weighted AP: running stock AP cluster attempt " << i_trial + 1
                      << " of " << m_p_conv_max_attempts << ".\n"
                      << "\tquantile preference: " << p_q << std::endl;
            b_conv = scby_apcluster(m_samples_to_clusters,
                                    m_clusters_to_samples,
                                    m_exemplars,
                                    m_s,
                                    m_p,
                                    m_timers,
                                    m_maxits,
                                    m_convits,
                                    m_lam,
                                    m_nonoise);
            if (b_conv) {
                // We converged!
                break;
            }
            else {
                // Restore similarities
                m_s = s_prev;
                // Decay quantile preference and update vector
                p_q *= m_p_conv_decay_fact;
                m_p.fill(p_q);
                m_p += p_delta;
            }
        }

        if (b_conv) {
            scby_assert(m_exemplars.n_elem > 0);
            std::cout << "Weighted AP: updating cluster model.\n";
            update_clusters();
            std::cout << "Weighted AP: cluster model updated with " << m_num_exemplars << " exemplars." << std::endl;
        }
        else {
            std::cout << "Weighted AP: stock AP cluster failed.\n";
            m_s = s_prev;
            m_p = p_prev;
            dump_apstate();
        }

        return b_conv;
    }

    /**
     * Removes all exemplars from reservoir that have not had a sample
     * assigned within the decay interval
     * @param tstamp - current timestamp iteration to check for stale exemplars
     */
    void remove_stale_exemplars(const scby_uint tstamp) {
        m_num_exemplars -= m_reservoir.remove_stale_exemplars(tstamp,
                           m_decay_window,
                           m_num_exemplars);
        scby_assert((m_num_exemplars + m_num_unlabeled) == m_reservoir.size());
    }

    /**
     * Updates a previously initialized clustering model after running affinity propagation
     */
    void update_clusters() {
        scby_assert(check_apresults(m_reservoir.size()));
        scby_assert(m_reservoir.size() <= m_reservoir.get_infos().size());
        scby_assert(m_reservoir.get_infos().size() == m_reservoir.get_samples().n_cols);
        scby_assert(m_exemplars.n_elem > 0);
        // Update exemplar infos
        scby_vec_col dist_vec;
        scby_real dist2;
        for (scby_uint i_ex=0; i_ex<m_exemplars.n_elem; ++i_ex) {
            // index into reservoir entry containing exemplar data
            const scby_uint res_index_ex = m_exemplars[i_ex];
            scby_assert_bounds(res_index_ex, 0, m_reservoir.size());
            scbyShwapSampleInfo &info_ex = m_reservoir.get_infos()[res_index_ex];
            scby_assert_bounds(info_ex.index, 0, m_positions_mat.n_cols);

            // process each sample associated to this exemplar
            const scby_uint n_elems = as_scby_uint(m_clusters_to_samples[i_ex].size());
            for (scby_uint i_elem=0; i_elem < n_elems; ++i_elem) {
                // index into reservoir entry containing sample data
                const scby_uint res_index_elem = m_clusters_to_samples[i_ex][i_elem];
                scby_assert_bounds(res_index_elem, 0, m_reservoir.size());
                // skip self calculations
                if (res_index_elem == res_index_ex) {
                    continue;
                }
                const scbyShwapSampleInfo &info_elem = m_reservoir.get_infos()[res_index_elem];
                scby_assert_bounds(info_elem.index, 0, m_positions_mat.n_cols);
                // update multiplicity
                info_ex.n += info_elem.n;
                // find max timestamp
                if (info_elem.touch > info_ex.touch) {
                    info_ex.touch = info_elem.touch;
                }
                // compute distance to exemplar
                SCBY_DIST2(dist2,
                           dist_vec,
                           m_positions_mat.unsafe_col(info_ex.index),
                           m_positions_mat.unsafe_col(info_elem.index));
                scby_assert(dist2 >= as_scby_real(0.0));
                // update sum of squared distances modulated by multiplicity and prior sum
                info_ex.sum_dist2 += ((dist2 * info_elem.n) + info_elem.sum_dist2);
                // update sum of distances modulated by multiplicity
                info_ex.sum_dist += (sqrt(dist2) * info_elem.n);
            }

            // update average distance of all other associated samples to this exemplar
            if (info_ex.n >= as_scby_real(2.0)) {
                info_ex.mean_dist = info_ex.sum_dist / (info_ex.n - as_scby_real(1.0));
            } else if (info_ex.n > as_scby_real(1.0)) {
                info_ex.mean_dist = info_ex.sum_dist / info_ex.n;
            } else {
                info_ex.mean_dist = as_scby_real(0.0);
            }

            // update "standard deviation" of distance to this exemplar
            if (info_ex.n > (as_scby_real(2.0) + SCBY_REAL_EPS)) {
                info_ex.stddev_dist = sqrt(
                                          fabs((info_ex.sum_dist2 - (info_ex.mean_dist * info_ex.mean_dist) * (info_ex.n - as_scby_real(1.0))))
                                          / (info_ex.n - as_scby_real(2.0))
                                      );
            } else {
                info_ex.stddev_dist = as_scby_real(0.0);
            }
        }

        // defragment exemplars
        m_reservoir.defragment(m_exemplars);
        m_num_exemplars = m_exemplars.n_elem;
        m_num_unlabeled = 0;
        scby_assert((m_num_exemplars + m_num_unlabeled) == m_reservoir.size());
    }

    /**
     * Similar to refine_clusters() method in stock affinity propagation.
     * Performs 2-step post-processing to "finalize" exemplars and cluster assignments
     * Warning: invalidates reservoir and m_num_unlabeled
     */
    void refine_clusters() {
        scby_assert(m_num_exemplars > 0);

        std::cout << "Refine clusters: 1st pass finding nearest exemplars.\n";
        find_nearest_exemplar();

        // now, determine final set of exemplars by finding sample closest to all other samples within each cluster
        std::cout << "Refine clusters: recomputing exemplars.\n";
        m_exemplars.set_size(m_num_exemplars);
        m_reservoir.clear();
        m_num_unlabeled = 0; // not needed, but just being consistent
        scby_matrix &exemplar_positions = m_reservoir.get_samples();
        exemplar_positions.set_size(m_positions_mat.n_rows, m_num_exemplars);
        scby_uint min_ex_ix;
        for (scby_uint i_clust=0; i_clust<m_num_exemplars; ++i_clust) {
            const scby_uint cluster_size = as_scby_uint(m_clusters_to_samples[i_clust].size());
            scby_assert(cluster_size > 0);
            // See if we can use distance caching to find exemplar index.
            if (can_alloc_pdist_mat(cluster_size)) {
                min_ex_ix = find_min_ex_ix_cached(
                                i_clust
                                , m_clusters_to_samples
                                , m_positions_mat
                            );
            }
            else {
                // Use memory optimized version (~2 times slower)
                std::cout << "Refine clusters: Cluster size " << cluster_size
                          << " too large for caching.\n"
                          << "Using memory efficient refinement at cluster "
                          << i_clust + 1 << " of " << m_num_exemplars << std::endl;
                min_ex_ix = find_min_ex_ix_nocache(
                                i_clust
                                , m_clusters_to_samples
                                , m_positions_mat
                            );
            }

            // verify an exemplar was found within cluster
            scby_assert(std::find(m_clusters_to_samples[i_clust].begin(),
                                 m_clusters_to_samples[i_clust].end(),
                                 min_ex_ix) != m_clusters_to_samples[i_clust].end());

            // store final exemplar index
            m_exemplars.at(i_clust) = min_ex_ix;

            // copy exemplar position for use in second-pass association
            scby_assert_bounds(i_clust, 0, exemplar_positions.n_cols);
            scby_assert_bounds(min_ex_ix, 0, m_positions_mat.n_cols);
            memcpy(exemplar_positions.colptr(i_clust),
                   m_positions_mat.colptr(min_ex_ix),
                   sizeof(scby_real) * m_positions_mat.n_rows);
        }

        std::cout << "Refine clusters: 2nd pass finding nearest exemplars.\n";
        find_nearest_exemplar();

        // Account for any swizzling that may have occurred during initial AP run.
        std::cout << "Refine clusters: Unswizzling sample mappings.\n";
        unswizzle_results();
    }

    /**
     * Method assumes exemplars are stored in first m_num_exemplar columns of reservoir.
     * Determines exemplar association of all samples based on nearest exemplar
     */
    void find_nearest_exemplar() {
        // reset outputs
        m_samples_to_clusters.set_size(m_positions_mat.n_cols);
        m_clusters_to_samples.clear();
        m_clusters_to_samples.resize(m_num_exemplars);

        // assuming that reservoir has StrAP exemplars in first m_num_exemplar columns
        // find nearest exemplar for each sample
        scby_uint min_col;
        scby_vec_col dist_vec;
        scby_real min_dist2;
        for (scby_uint i_sample=0; i_sample<m_positions_mat.n_cols; ++i_sample) {
            min_dist2 = find_min_dist2(min_col,
                                       dist_vec,
                                       m_positions_mat,
                                       i_sample,
                                       m_reservoir.get_samples(),
                                       m_num_exemplars);
            scby_assert_bounds(min_col, 0, m_num_exemplars);
            scby_assert(min_dist2 >= as_scby_real(0.0));
            m_samples_to_clusters[i_sample] = min_col;
            m_clusters_to_samples[min_col].push_back(i_sample);
        }
    }

    /**
     * If swizzling has occurred during initial AP run, this method will
     * re-map the output data to the original input order.
     */
    void unswizzle_results() {
        // Early out if no swizzling has occurred
        if (!m_swizzled) {
            return;
        }

        // Unswizzle exemplars
        for (scby_uint i_ex = 0; i_ex < m_exemplars.n_elem; ++i_ex) {
            m_exemplars.at(i_ex) = m_swiz_ix_2_init_ix.at(m_exemplars.at(i_ex));
        }

        // Unswizzle clusters to samples
        for (scby_uint i_clust = 0; i_clust < m_clusters_to_samples.size(); ++i_clust) {
            scby_ap_c2s_map_t::value_type &clust = m_clusters_to_samples[i_clust];
            for (scby_uint i_samp = 0; i_samp < clust.size(); ++i_samp) {
                clust[i_samp] = m_swiz_ix_2_init_ix.at(clust[i_samp]);
            }
        }

        // Unswizzle samples to clusters
        {
            scby_index_vec_t tmp_samples_to_clusters;
            tmp_samples_to_clusters.set_size(m_samples_to_clusters.n_elem);
            for (scby_uint i_samp = 0; i_samp < m_samples_to_clusters.n_elem; ++i_samp) {
                tmp_samples_to_clusters.at(m_swiz_ix_2_init_ix.at(i_samp)) = m_samples_to_clusters.at(i_samp);
            }
            m_samples_to_clusters = tmp_samples_to_clusters;
        }

        // Unswizzle positions mat
        if (m_restore_positions_mat) {
            scby_matrix tmp_positions_mat;
            tmp_positions_mat.set_size(m_positions_mat.n_rows, m_positions_mat.n_cols);
            for (scby_uint i_samp = 0; i_samp < m_positions_mat.n_cols; ++i_samp) {
                tmp_positions_mat.col(m_swiz_ix_2_init_ix.at(i_samp)) = m_positions_mat.col(i_samp);
            }
            m_positions_mat = tmp_positions_mat;
        }
    }

    /**
     * Utility called if stock AP fails to converge. Prints to stdout all
     * relevant data points that may help with isolating the convergence
     * issue such as the input similarities matrix and preference vector.
     */
    void dump_apstate() {
        std::cout << "Similarities:\n";
        m_s.print();
        std::cout << std::endl;
        std::cout << "Preferences:\n";
        m_p.print();
        std::cout << std::endl;
        std::cout << "Exemplar infos (id: index, mean_dist, n, stddev_dist, sum_dist, sum_dist2, touch):\n";
        for (scby_uint i_ex = 0; i_ex < m_num_exemplars; ++i_ex) {
            const scbyShwapSampleInfo &ex_info = m_reservoir.get_infos()[i_ex];
            std::cout << i_ex << " : " << ex_info.index << ", " << ex_info.mean_dist
                      << ", " << ex_info.n << ", " << ex_info.stddev_dist
                      << ", " << ex_info.sum_dist << ", " << ex_info.sum_dist2
                      << ", " << ex_info.touch << "\n";
        }
        std::cout << std::endl;
    }

    /**
     * Arguments
     */
    scby_index_vec_t &m_samples_to_clusters;
    scby_ap_c2s_map_t &m_clusters_to_samples;
    scby_index_vec_t &m_exemplars;
    scby_matrix &m_positions_mat;
    scby_ap_timers *m_timers;
    scby_uint m_init_size;
    const scby_real m_ex_assoc_heur_scale;
    const scby_uint m_max_unlabeled_thresh;
    const scby_uint m_max_exemplars_thresh;
    const scby_uint m_decay_window;
    const scby_real m_q;
    const size_t m_maxits;
    const size_t m_convits;
    const scby_real m_lam;
    const bool m_nonoise;
    const scby_real m_p_conv_decay_fact;
    const scby_uint m_p_conv_max_attempts;
    const scby_real m_init_swiz_max_attempts;
    const bool m_restore_positions_mat;

    /**
     * True if swizzling has occurred during initial AP run, false otherwise
     */
    bool m_swizzled;

    /**
     * Mapping from swizzled index to initial index
     */
    scby_index_vec_t m_swiz_ix_2_init_ix;

    /**
     * Buffer for storing similarity values
     */
    scby_matrix m_s;

    /**
     * Buffer for storing preferences
     */
    scby_vec_col m_p;

    /**
     * Stores geometry and meta-data for exemplars and set of unlabeled samples
     */
    scbyShwapReservoir m_reservoir;

    /**
     * Current count of exemplars (stored at front of reservoir)
     */
    scby_uint m_num_exemplars;

    /**
     * Current count of unlabeled items in reservoir (stored after exemplars)
     */
    scby_uint m_num_unlabeled;

    /**
     * Heuristic for associating a sample to a cluster
     */
    assoc_heur_t m_assoc_heur;
};

//****************************************************************************
// SHWAP - Streaming, hierarchical, weighted affinity propagation
//****************************************************************************

/**
 * Streaming, hierarchical, weighted affinity propagation for clustering large
 * data sets. - based on StrAP algorithm.This version internally computes default
 * pairwise similarities and preferences based on input positions_mat and q
 * parameters
 * BEGIN common outputs and inputs:
 * @param samples_to_clusters - Stores cluster assignment for each sample - output
 *  is size n where n is number of samples
 * @param clusters_to_samples - Stores sample assignments for each cluster
 * @param exemplars - stores index of exemplar sample for each cluster - output is
 *  size k where k is number of clusters found
 * @param positions_mat - Matrix of sample coordinates, each sample is assumed to
 *  take a single column. This matrix may be re-ordered during the initial AP
 *  run in order to obtain a convergent ensemble. If the original order must be
 *  preserved, set restore_positions_mat=true
 * @param timers - (optional) Used for timing various operations
 * BEGIN shwap parameters
 * @param init_size - The first init_size elements are clustered using affinity
 *  propagation to find the initial exemplar and cluster assignments
 * @param ex_assoc_heur - The type of heuristic to use for attempting to associate
 *  an unlabeled sample to an existing exemplar
 * @param ex_assoc_heur_scale - The association heuristic cutoff distance is
 *  scaled by this factor->values less than 1.0 make it harder to associate an
 *  unlabeled sample to an exemplar->values greater than 1.0 make it easier to
 *  associate an unlabeled sample to an exemplar
 * @param max_unlabeled_thresh - Threshold for triggering a weighted, local
 *  affinity propagation on samples which failed to be associated to an exemplar
 * @param max_exemplars_thresh - Threshold for triggering a weighted, hierarchical
 *  affinity propagation run on only the exemplars.This is to attempt to merge
 *  exemplars and reduce the total count in order for the local affinity
 *  propagation runs to be performant.
 * @param decay_window - Each exemplar has a window length defined as the number
 *  of samples that have been processed since the last time a sample was
 *  associated to that exemplar.If the window length exceeds the decay value
 *  then the exemplar is removed.Furthermore, the multiplicity weights
 *  associated to that exemplar are reduced as a function of this decay length.
 *  BEGIN classic affinity propagation parameters :
 * @param q - specifies which quantile similarity to assign as the no-prior
 *  preference
 * @param maxits - Maximum number of iterations for each affinity propagation run
 * @param convits - A single run of affinity propagation terminates if the
 *  exemplars have not changed for convits iterations
 * @param lam - Damping factor; should be a value in the range [0.5, 1); higher
 *  values correspond to heavy damping which may be needed if oscillations occur
 * @param nonoise - If false, adds a small amount of noise to similarities to
 *  prevent degenerate cases
 * @param p_conv_decay_fact -  If stock affinity propagation fails to convergence,
 *  we can fiddle with the preferences in a reasonable way such that the
 *  subsequent run will likely converge.Only applies to non - init AP runs.
 * @param p_conv_max_attempts - The maximum number of times to fiddle with the
 *  input preferences in attempt to get affinity propagation to converge before
 *  giving up.Only applies to non - init AP runs.
 * @param init_swiz_max_attempts - The maximum number of times to swizzle the input
 *  samples in an attempt to obtain an ensemble that converges during the initial
 *  affinity propagation run.
 * @param restore_positions_mat - During initialization, the input positions
 *  matrix may be randomly swizzled in order to obtain a convergent ensemble.If
 *  true, the input positions matrix is restored to the original input state;
 *  else, the positions matrix is left modified.
 * @return TRUE if converged, FALSE o/w
 */
bool scby_shwap_pdist2(scby_index_vec_t &samples_to_clusters,
                      scby_ap_c2s_map_t &clusters_to_samples,
                      scby_index_vec_t &exemplars,
                      scby_matrix &positions_mat,
                      scby_ap_timers *timers,
                      const scby_uint init_size,
                      const escby_shwap_ex_assoc_heur ex_assoc_heur,
                      const scby_real ex_assoc_heur_scale,
                      const scby_uint max_unlabeled_thresh,
                      const scby_uint max_exemplars_thresh,
                      const scby_uint decay_window,
                      const scby_real q,
                      const size_t maxits,
                      const size_t convits,
                      const scby_real lam,
                      const bool nonoise,
                      const scby_real p_conv_decay_fact,
                      const scby_uint p_conv_max_attempts,
                      const scby_uint init_swiz_max_attempts,
                      const bool restore_positions_mat) {

#ifdef SHWAP_CLUSTER
#   error SHWAP_CLUSTER already defined!
#endif
#define SHWAP_CLUSTER(assoc_heur_type) \
    scbyShwaperPdist2<assoc_heur_type> shwaper( \
        samples_to_clusters, \
        clusters_to_samples, \
        exemplars, \
        positions_mat, \
        timers, \
        init_size, \
        ex_assoc_heur_scale, \
        max_unlabeled_thresh, \
        max_exemplars_thresh, \
        decay_window, \
        q, \
        maxits, \
        convits, \
        lam, \
        nonoise, \
        p_conv_decay_fact, \
        p_conv_max_attempts, \
        init_swiz_max_attempts, \
        restore_positions_mat); \
    result = shwaper.cluster()

    bool result = false;
    if (ex_assoc_heur == escby_shwap_ex_assoc_heur_nasc_univ_avg_dist2) {
        SHWAP_CLUSTER(scbyShwapExAssocHeurNascUnivAvgDist2);
    } else if (ex_assoc_heur == escby_shwap_ex_assoc_heur_nasc_univ_med_avg_dist2) {
        SHWAP_CLUSTER(scbyShwapExAssocHeurNascUnivMedAvgDist2);
    } else {
        // unrecognized exemplar association heuristic
        scby_assert(false);
    }
    return result;

#undef SHWAP_CLUSTER
}
