/**
 * Utilities for computing pairwise distances
 */
#ifndef SCBYCOMPUTE_PDIST_H
#define SCBYCOMPUTE_PDIST_H

//****************************************************************************
// Includes
//****************************************************************************

#include "scbybuild.h"
#include "scbytypes.h"
#include "scbyparallel_mapper.h"

#include <vector>

//****************************************************************************
// Common utilities for multi-threaded overloads
//****************************************************************************

/**
 * If want to pre-allocate multi-thread scratch buffers for RMSD, pdist2, pdist
 * calculations, then use any overload of this method
 * @param dist_vecs_mt - the output per-thread allocated scratch buffers
 * @param pm - initialized parallel mapper for managing threads
 * @param positions_mat - each sample is a single column
 */
extern void scby_alloc_mt_pdist_buffers(
    std::vector<scby_vec_col> &dist_vecs_mt,
    const scbyParallelMapper &pm,
    const scby_matrix &positions_mat
);

/**
 * If want to pre-allocate multi-thread scratch buffers for RMSD, pdist2, pdist
 * calculations, then use any overload of this method
 * @param dist_vecs_mt - the output per-thread allocated scratch buffers
 * @param n_threads - the number of threads used for parallel distance calculations
 * @param n_elems - the total number of elements (dimensions) defining a single sample
 */
extern void scby_alloc_mt_pdist_buffers(
    std::vector<scby_vec_col> &dist_vecs_mt,
    const scby_uint n_threads,
    const scby_uint n_elems
);

//****************************************************************************
// Single-threaded RMSD implementation
//****************************************************************************

/**
 * Computes root-mean-square distance (RMSD)
 * https://en.wikipedia.org/wiki/Root-mean-square_deviation_of_atomic_positions
 * @param rmsd_mat - output symmetric RMSD matrix
 * @param positions_mat - each sample is a single column
 * @param n_nodes - the number of nodes defining each sample
 */
extern void scby_compute_rmsd(
    scby_sym_matrix &rmsd_mat,
    const scby_matrix &positions_mat,
    const scby_uint n_nodes
);

/**
 * Overload to use memory intensive matrix
 * Computes root-mean-square distance (RMSD)
 * https://en.wikipedia.org/wiki/Root-mean-square_deviation_of_atomic_positions
 * @param rmsd_mat - output symmetric RMSD matrix
 * @param positions_mat - each sample is a single column
 * @param n_nodes - the number of nodes defining each sample
 */
extern void scby_compute_rmsd(
    scby_matrix &rmsd_mat,
    const scby_matrix &positions_mat,
    const scby_uint n_nodes
);

//****************************************************************************
// Multi-threaded (parallel) RMSD implementation
//****************************************************************************

/**
 * multi-threaded version of scby_compute_rmsd
 * uses pre-allocated scratch buffers
 * @param rmsd_mat - output symmetric RMSD matrix
 * @param positions_mat - each sample is a single column
 * @param n_nodes - the number of nodes defining each sample
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 * @param dist_vecs_mt - per thread scratch buffers for distance calculations
 *  -must be allocated via scby_alloc_mt_pdist_buffers(...)
 */
extern void scby_compute_rmsd_mt(
    scby_sym_matrix &rmsd_mat,
    const scby_matrix &positions_mat,
    const scby_uint n_nodes,
    scbyParallelMapper &pm,
    const scby_uint inc_by,
    std::vector<scby_vec_col> &dist_vecs_mt
);

/**
 * overload to use memory intensive matrix
 * multi-threaded version of scby_compute_rmsd
 * uses pre-allocated scratch buffers
 * @param rmsd_mat - output symmetric RMSD matrix
 * @param positions_mat - each sample is a single column
 * @param n_nodes - the number of nodes defining each sample
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 * @param dist_vecs_mt - per thread scratch buffers for distance calculations
 *  -must be allocated via scby_alloc_mt_pdist_buffers(...)
 */
extern void scby_compute_rmsd_mt(
    scby_matrix &rmsd_mat,
    const scby_matrix &positions_mat,
    const scby_uint n_nodes,
    scbyParallelMapper &pm,
    const scby_uint inc_by,
    std::vector<scby_vec_col> &dist_vecs_mt
);

/**
 * multi-threaded version of scby_compute_rmsd
 * @param rmsd_mat - output symmetric RMSD matrix
 * @param positions_mat - each sample is a single column
 * @param n_nodes - the number of nodes defining each sample
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 */
extern void scby_compute_rmsd_mt(
    scby_sym_matrix &rmsd_mat,
    const scby_matrix &positions_mat,
    const scby_uint n_nodes,
    scbyParallelMapper &pm,
    const scby_uint inc_by
);

/**
 * overload to use memory intensive matrix
 * multi-threaded version of scby_compute_rmsd
 * @param rmsd_mat - output symmetric RMSD matrix
 * @param positions_mat - each sample is a single column
 * @param n_nodes - the number of nodes defining each sample
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 */
extern void scby_compute_rmsd_mt(
    scby_matrix &rmsd_mat,
    const scby_matrix &positions_mat,
    const scby_uint n_nodes,
    scbyParallelMapper &pm,
    const scby_uint inc_by
);

//****************************************************************************
// Single-threaded squared distance implementation
//****************************************************************************

/**
 * Computes pairwise squared distances
 * @param pdist2_mat - output symmetric squared distance matrix
 * @param positions_mat - each sample is a single column
 */
extern void scby_compute_pdist2(
    scby_sym_matrix &pdist2_mat,
    const scby_matrix &positions_mat
);

/**
 * Overload to use memory intensive matrix
 * Computes pairwise squared distances
 * @param pdist2_mat - output symmetric squared distance matrix
 * @param positions_mat - each sample is a single column
 */
extern void scby_compute_pdist2(
    scby_matrix &pdist2_mat,
    const scby_matrix &positions_mat
);

//****************************************************************************
// Multi-threaded (parallel) squared distance implementation
//****************************************************************************

/**
 * multi-threaded version of scby_compute_pdist2
 * uses pre-allocated scratch buffers
 * @param pdist2_mat - output symmetric squared distance matrix
 * @param positions_mat - each sample is a single column
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 * @param dist_vecs_mt - per thread scratch buffers for distance calculations
 *  -must be allocated via scby_alloc_mt_pdist_buffers(...)
 */
extern void scby_compute_pdist2_mt(
    scby_sym_matrix &pdist2_mat,
    const scby_matrix &positions_mat,
    scbyParallelMapper &pm,
    const scby_uint inc_by,
    std::vector<scby_vec_col> &dist_vecs_mt
);

/**
 * overload to use memory intensive matrix
 * multi-threaded version of scby_compute_pdist2
 * uses pre-allocated scratch buffers
 * @param pdist2_mat - output symmetric squared distance matrix
 * @param positions_mat - each sample is a single column
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 * @param dist_vecs_mt - per thread scratch buffers for distance calculations
 * -must be allocated via scby_alloc_mt_pdist_buffers(...)
 */
extern void scby_compute_pdist2_mt(
    scby_matrix &pdist2_mat,
    const scby_matrix &positions_mat,
    scbyParallelMapper &pm,
    const scby_uint inc_by,
    std::vector<scby_vec_col> &dist_vecs_mt
);

/**
 * multi-threaded version of scby_compute_pdist2
 * @param pdist2_mat - output symmetric squared distance matrix
 * @param positions_mat - each sample is a single column
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 */
extern void scby_compute_pdist2_mt(
    scby_sym_matrix &pdist2_mat,
    const scby_matrix &positions_mat,
    scbyParallelMapper &pm,
    const scby_uint inc_by
);

/**
 * overload to use memory intensive non-symmetric matrix
 * multi-threaded version of scby_compute_pdist2
 * @param pdist2_mat - output symmetric squared distance matrix
 * @param positions_mat - each sample is a single column
 * @param pm - initialized parallel mapper for managing threads
 * @param inc_by - the size of the work batch
 */
extern void scby_compute_pdist2_mt(
    scby_matrix &pdist2_mat,
    const scby_matrix &positions_mat,
    scbyParallelMapper &pm,
    const scby_uint inc_by
);

#endif // SCBYCOMPUTE_PDIST_H
