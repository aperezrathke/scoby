//****************************************************************************
// scbyrand.h
//****************************************************************************

/**
 * Utility for generating random numbers
 */

#ifndef SCBY_RAND_H
#define SCBY_RAND_H

//****************************************************************************
// Includes
//****************************************************************************

#include "scbybuild.h"
#include "scbyassert.h"
#include "scbytypes.h"

// Make sure to initialize and expose random API from matrix library
#include "scbyarmadillo.h"

#include <random>

//****************************************************************************
// Interface
//****************************************************************************

/**
 * Interface for uniform random number generation
 * @TODO - add functions for generating random vectors and matrices as needed
 * @TODO - allocate one random number generator per thread
 * @TODO - investigate SIMD implementation of Mersenne twister:
 *  http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/SFMT/index.html
 *  (SIMD-oriented Fast Mersenne Twister (SFMT))
 */
class scby_rand
{
public:

    /**
     * Default constructor - initializes RNG engine
     */
    scby_rand() : eng(getSeed())
    {
        // Note: thread_local must be supported in order to have Mersenne
        // twister support within arma
        scby_matrix_utils::arma_rng::set_seed(getSeed());
    }

    /**
     * @return Uniform random number in [0.0, 1.0)
     */
    inline scby_real unif_real()
    {
        return unif_real_dis(eng);
    }

    /**
     * @return Uniform random number in [lower_, upper_)
     */
    inline scby_real unif_real(const scby_real lower_, const scby_real upper_)
    {
        scby_assert(lower_ <= upper_);
        std::uniform_real_distribution<scby_real> dis(lower_, upper_);
        return dis(eng);
    }

    /**
     * @return Integer in [lower_, upper_] inclusive
     */
    template <typename t_int>
    inline t_int unif_int(const t_int lower_, const t_int upper_)
    {
        scby_assert(lower_ <= upper_);
        std::uniform_int_distribution<t_int> dis(lower_, upper_);
        return dis(eng);
    }

private:

    // According to:
    // https://channel9.msdn.com/Events/GoingNative/2013/rand-Considered-Harmful
    // Copying the random number engine is expensive as Mersenne twister has a
    // large internal state. It's "okay" to copy the distribution object, but
    // definitely avoid copying the engine object.

    // It would be nice to forbid (make private) the copy constructor and
    // assignment operator; however, our thread local storage implementation
    // requires them. (It's recommended to have 1 instance per thread). So,
    // for now, we are not forbidding implicit copy and assignment operations.
    typedef std::random_device::result_type seed_type;

    /**
     * @return Seed to initialize RNG
     */
    static seed_type getSeed()
    {
#ifdef SCBY_BUILD_SEED_RAND
        static std::random_device rd;
        const seed_type seed = rd();
#else
        const seed_type seed = 7331;
#endif // SCBY_BUILD_SEED_RAND
        std::cout << "Random seed: " << seed << std::endl;
        return seed;
    }

    /**
     * Typedef the random number engine
     */
    typedef std::mt19937 reng_t;

    /**
     * Engine which generates the random numbers
     */
    reng_t eng;

    /**
     * Uniform distribution on [0,1) interval
     */
    std::uniform_real_distribution<scby_real>  unif_real_dis;
};

#endif // SCBY_RAND_H
