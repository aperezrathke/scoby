//****************************************************************************
// scbyparallel_mapper.h
//****************************************************************************

/**
 * @brief Utility calls the same operation on each element of an array but
 * in parallel among a parameter number of threads. It's like Map-Reduce but
 * without the reduce.
 */

#ifndef SCBYPARALLEL_MAPPER_H
#define SCBYPARALLEL_MAPPER_H

//****************************************************************************
// Includes
//****************************************************************************

#include "scbythread.h"

//****************************************************************************
// scbyMappableWork
//****************************************************************************

/**
 * Interface for an array to be processed in parallel
 */
class scbyMappableWork
{
public:

    /**
     * Virtual destructor
     */
    virtual ~scbyMappableWork()
    {}

    /**
     * Defines what work is to be performed for the parameter range
     * @param ix_start - the starting index to perform work at
     * @param count - the number of work units to perform beginning at
     *  ix_start
     * @param thread_id - a unique calling thread identifier
     */
    virtual scby_bool do_range(
          const scby_uint ix_start
        , const scby_uint count
        , const scby_uint thread_id
    ) = 0;
};

//****************************************************************************
// scbyMapperThread
//****************************************************************************

class scbyParallelMapper;

/**
 * Wrapper around system thread.
 */
class scbyMapperThread
{
public:

    /**
     * Constructs mapper thread
     * @param mapper - owning mapper pool
     * @param thread_id - a unique identifier for this thread
     */
    scbyMapperThread(
          scbyParallelMapper& mapper
        , const scby_uint thread_id
    );

    /**
     * Destructor
     */
    ~scbyMapperThread();

    /**
     * Thread entry point - analogous to process main
     */
    void thread_main();

    /**
     * Our thread identifier
     */
    const scby_uint m_id;

private:

    // Prohibit copy and assignment
    scbyMapperThread(const scbyMapperThread&);
    scbyMapperThread& operator=(const scbyMapperThread&);

    /**
     * @return TRUE if thread should exit, FALSE o/w
     */
    scby_bool should_exit() const;

    /**
     * Waits until signaled to start
     * Also notifies mapper thread when all workers have become idle
     */
    void wait_until_work_ready() const;

    /**
     * Keeps grabbing chunks of work until none is available
     */
    void do_available_work() const;

    /**
     * Parent mapper owning this thread
     */
    scbyParallelMapper& m_mapper;

    /**
     * The actual thread object
     */
    scbyThread_t* mp_sys_thread;
};

//****************************************************************************
// scbyParallelMapper
//****************************************************************************

class scbyParallelMapper
{

public:

    /**
     * Default constructor
     */
    scbyParallelMapper();

    /**
     * Destructor
     */
    ~scbyParallelMapper();

    /**
     * Can only be called once, initializes thread pool
     */
    void init(const scby_uint num_threads);

    /**
     * Address the following issue:
     * http://stackoverflow.com/questions/10915233/stdthreadjoin-hangs-if-called-after-main-exits-when-using-vs2012-rc
     * Basically on windows, if threads are still "alive" before main thread
     * exits, then the program will hang. Therefore, we need to destroy our
     * threads prior to main() exiting.
     */
    void teardown();

    /**
     * Processes work object and blocks calling thread until finished.
     * @param work - the work to process in parallel
     * @param inc_by - the chunk size of each work unit processed by a thread
     */
    void wait_until_work_finished(scbyMappableWork& work, const scby_uint inc_by);

    /**
     * Outputs mapper status
     */
    void log_status() const;

    /**
     * @return number of threads in thread pool
     */
    inline scby_uint num_pool_threads() const {
        return static_cast<scby_uint>(m_pool.size());
    }

private:

    // Prohibit copy and assignment
    scbyParallelMapper(const scbyParallelMapper&);
    scbyParallelMapper& operator=(const scbyParallelMapper&);

    /**
     * @return TRUE if threads should exit, FALSE o/w
     */
    scby_bool exiting() const;

    /**
     * Barrier method - waits until workers threads
     * have blocked on condition variable used for work
     * available notification. This helps avoid race
     * conditions that could otherwise occur if work
     * becomes available before worker threads block
     * (which would cause cond. var::notify_all() to be
     * called before worker thread is waiting on it, which
     * when worker thread does finally call wait, will cause
     * the thread to block instead, which leads to deadlock
     * as main thread is counting on workers to decrement
     * m_num_active_workers to 0.
     */
    void wait_until_all_workers_idle(scbyLock_t& lk);

    /**
     * The work currently being undertaken - client is responsible for
     * managing this memory - the mapper does not delete this work!
     */
    scbyMappableWork* mp_work;

    /**
     * The amount to increment the iteration counter by. Essentially defines
     * the size of a work package a thread performs before attempting to get
     * the next work package.
     */
    scbyAtomicUInt m_inc_by;

    /**
     * An atomic counter - allows atomic operations such as incrementing and
     * decrementing the counter value. This counter is used for iterating over
     * an array of work.
     */
    scbyAtomicUInt m_iter;

    /**
     * Atomic counter used for keeping track of which threads have outstanding
     * work. It is the responsibility of the threads to update this
     * counter when they complete and then notify parent mapper of this event.
     */
    scbyAtomicUInt m_num_active_workers;

    /**
     * Counter to signal that threads should be exiting
     */
    scbyAtomicUInt m_should_exit;

    /**
     * Mutex used for synchronizing access to condition variables
     */
    scbyMutex_t m_mutex;

    /**
     * Used for notifying threads that work is ready to be processed
     */
    scbyConditionVariable_t m_cv_work_begin;

    /**
     * Used for notifying the main thread that all worker threads have finished
     */
    scbyConditionVariable_t m_cv_all_workers_idle;

    /**
     * Our pool of worker threads
     */
    std::vector<scbyMapperThread*> m_pool;

    /**
     * Give threads direct access
     */
    friend class scbyMapperThread;
};

#endif // SCBYPARALLEL_MAPPER_H
