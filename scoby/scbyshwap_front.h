/**
 * Front-facing utilities for streaming, hierarchical, weighted affinity propagation
 *
 * Hierarchical, weighted affinity propagation - a C++ port of the Matlab StrAP algorithm.
 * Uses reservoir size and exemplar count thresholds as criterion for re-running affinity propagation.
 *
 * Zhang, Xiangliang, Cyril Furtlehner, and Michele Sebag. "Data streaming with affinity propagation."
 * In Machine Learning and Knowledge Discovery in Databases, pp. 628-643. Springer Berlin Heidelberg, 2008.
 *
 * Zhang, Xiangliang, Cyril Furtlehner, Cecile Germain-Renaud, and Michele Sebag.
 * "Data stream clustering with affinity propagation." Knowledge and Data Engineering,
 * IEEE Transactions on 26, no. 7 (2014): 1644-1656.
 */
#ifndef SCBYSHWAP_FRONT_H
#define SCBYSHWAP_FRONT_H

//****************************************************************************
// Includes
//****************************************************************************

#include "scbybuild.h"
#include "scbytypes.h"
#include "scbyshwap.h"

//****************************************************************************
// Arguments
//****************************************************************************

/**
 * Enumerated user arguments for use in argv, argc main
 */
enum escby_shwap_pdist2_bin_args {
    escby_shwapc_pd2b_exe_name=0,
    escby_shwapc_pd2b_app_name,
    escby_shwapc_pd2b_in_matrix_path,
    escby_shwapc_pd2b_n_dims,
    escby_shwapc_pd2b_out_samples_to_clusters_path,
    escby_shwapc_pd2b_out_clusters_to_samples_path,
    escby_shwapc_pd2b_out_exemplars_path,
    escby_shwapc_pd2b_min_num_args,
    escby_shwapc_pd2b_init_size=escby_shwapc_pd2b_min_num_args,
    escby_shwapc_pd2b_ex_assoc_heur,
    escby_shwapc_pd2b_ex_assoc_heur_scale,
    escby_shwapc_pd2b_max_unlabeled_thresh,
    escby_shwapc_pd2b_max_exemplars_thresh,
    escby_shwapc_pd2b_decay_window,
    escby_shwapc_pd2b_q,
    escby_shwapc_pd2b_maxits,
    escby_shwapc_pd2b_convits,
    escby_shwapc_pd2b_lam,
    escby_shwapc_pd2b_nonoise,
    escby_shwapc_pd2b_p_conv_decay_fact,
    escby_shwapc_pd2b_p_conv_max_attempts,
    escby_shwapc_pd2b_init_swiz_max_attempts,
    escby_shwapc_pd2b_max_num_args
};

/**
 * Arguments to utility
 */
struct scby_shwap_pdist2_bin_args {
    /**
     * Path to input matrix
     */
    const char* in_matrix_path;
    /**
     * Number of dimensions
     */
    scby_uint n_dims;
    /**
     * Path to write sample id to cluster id mapping in CSV format
     */
    const char* out_samples_to_clusters_path;
    /**
     * Path to write cluster id to set of sample ids in CSV format
     */
    const char* out_clusters_to_samples_path;
    /**
     * Path to write list of exemplar ids
     */
    const char* out_exemplars_path;
    /**
     * The first init_size elements are clustered using affinity propagation to
     * find the initial exemplar and cluster assignments
     */
    scby_uint init_size;
    /**
     * The type of heuristic to use for attempting to associate an unlabeled sample
     * to an existing exemplar
     */
    escby_shwap_ex_assoc_heur ex_assoc_heur;
    /**
     * The association heuristic cutoff distance is scaled by this factor
     *  -> values less than 1.0 make it harder to associate an unlabeled sample to an exemplar
     *  -> values greater than 1.0 make it easier to associate an unlabeled sample to an exemplar
     */
    scby_real ex_assoc_heur_scale;
    /**
     * Threshold for triggering a weighted, local affinity propagation on samples
     * which failed to be associated to an exemplar
     */
    scby_uint max_unlabeled_thresh;
    /**
     * Threshold for triggering a weighted, hierarchical affinity propagation run on only
     * the exemplars. This is to attempt to merge exemplars and reduce the total count in order
     * for the local affinity propagation runs to be performant.
     */
    scby_uint max_exemplars_thresh;
    /**
     * Each exemplar has a window length defined as the number of samples that have been processed since
     * the last time a sample was associated to that exemplar. If the window length exceeds the decay value
     * then the exemplar is removed. Furthermore, the multiplicity weights associated to that exemplar are
     * reduced as a function of this decay length.
     */
    scby_uint decay_window;
    /**
     * Specifies which quantile similarity to assign as the no-prior preference
     */
    scby_real q;
    /**
     * Maximum number of iterations for each affinity propagation run
     */
    size_t maxits;
    /**
     * A single run of affinity propagation terminates if the exemplars have not changed for convits
     * iterations
     */
    size_t convits;
    /**
     * Damping factor; should be a value in the range [0.5, 1); higher values correspond
     * to heavy damping which may be needed if oscillations occur
     */
    scby_real lam;
    /**
     * If false, a small amount of noise is added to similarities to prevent degenerate cases
     */
    bool nonoise;
    /**
     * If a stock affinity propagation run fails to converge, preferences are decayed
     * according to: p_q = p_q * p_conv_decay_fact where p_q is the preference according
     * to the q-th quantile similarity. If picked properly, this will usually allow
     * convergence on the subsequent run. Good values based on testing are 1.05 for
     * slightly less clusters and 0.95 for slightly more clusters. Note, values > 1
     * actually result in "growth" away from zero rather than "decay" towards zero.
     */
    scby_real p_conv_decay_fact;
    /**
     * The maximum number of convergence attempts made by decaying the quantile
     * preference according to p_q = p_q * p_conv_decay. Hence, the preferences
     * are decayed exponentially.
     */
    scby_uint p_conv_max_attempts;
    /**
     * The maximum number of times to swizzle the input samples in an attempt to
     * obtain an ensemble that converges during the initial affinity propagation run.
     */
    scby_uint init_swiz_max_attempts;
};

//****************************************************************************
// Extern(s)
//****************************************************************************

/**
 * Given input samples with coordinates in columns, will perform
 * streaming, hierarchical affinity propagation
 */
extern void scby_shwap_pdist2_bin(const scby_shwap_pdist2_bin_args &args);

#endif // SCBYSHWAP_FRONT_H
