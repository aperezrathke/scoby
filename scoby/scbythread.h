//****************************************************************************
// scbythread.h
//****************************************************************************

/**
 * Our threading specification
 */

#ifndef SCBYTHREAD_H
#define SCBYTHREAD_H

//****************************************************************************
// Includes
//****************************************************************************

#include "scbyassert.h"
#include "scbytypes.h"

#include <atomic>
#include <condition_variable>
#include <thread>
#include <time.h>
#include <vector>

//****************************************************************************
// Defines
//****************************************************************************

// The thread namespace (e.g. - could be std or boost)
#define scbyTh std

// The number of threads to initialize by default
#define SCBY_DEFAULT_NUM_THREADS (std::max<scby_uint>(scbyTh::thread::hardware_concurrency(), 1))

//****************************************************************************
// Typedefs
//****************************************************************************

typedef scbyTh::thread scbyThread_t;
typedef scbyTh::mutex scbyMutex_t;
typedef scbyTh::unique_lock<scbyMutex_t> scbyLock_t;
typedef scbyTh::lock_guard<scbyMutex_t> scbyScopedLock_t;
typedef scbyTh::condition_variable scbyConditionVariable_t;
typedef scbyTh::atomic<scby_uint> scbyAtomicUInt;
typedef scbyTh::atomic<time_t> scbyAtomicTime_t;

#endif // SCBYTHREAD_H
