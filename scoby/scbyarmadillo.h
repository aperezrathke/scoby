//****************************************************************************
// scbyarmadillo.h
//****************************************************************************

/**
 * @brief Armadillo include which incorporates build flags
 */

#ifndef SCBY_ARMADILLO_H
#define SCBY_ARMADILLO_H

#include "scbybuild.h"
#include "scbytypes.h"

/**
 * Check if we don't want armadillo debug information (e.g. during release)
 * Also, define NDEBUG before including this external library just in case it
 * still has internal asserts that aren't disabled by ARMA_NO_DEBUG.
 */
#ifndef SCBY_BUILD_ARMADILLO_DEBUG
#   ifndef ARMA_NO_DEBUG
#       define ARMA_NO_DEBUG // tell armadillo to avoid debug checks
#   endif
#   ifndef NDEBUG // for good measure, disable asserts for this unit
#       define SCBY_NDEBUG_TRANSIENT // this means enable asserts after include
#       define NDEBUG
#   endif // NDEBUG
#   include <armadillo>
#   ifdef SCBY_NDEBUG_TRANSIENT // NDEBUG wasn't defined before, so undef it
#       undef SCBY_NDEBUG_TRANSIENT
#       undef NDEBUG
#   endif // U_NDEBUG_TRANSIENT
#else
#   include <armadillo> // else, lets include extra debug checks
#endif // U_BUILD_ARMADILLO_DEBUG

/**
 * Wrap namespace
 */
#define scby_matrix_utils arma

/**
 * Wrap armadillo specific binary read/write mode
 */
#define scby_matrix_bin_io_mode scby_matrix_utils::arma_binary

/**
 * Matrix size type
 */
typedef scby_matrix_utils::uword sbcy_mat_sz_t;

/**
 * Macro to convert to SCOBY matrix size type
 */
#define as_scby_mat_sz_t(i) static_cast<sbcy_mat_sz_t>(i)

/**
 * Matrix type
 */
typedef scby_matrix_utils::Mat<scby_real> scby_matrix;

/**
 * Real column vector type
 */
typedef scby_matrix_utils::Col<scby_real> scby_vec_col;

/**
 * Wrapper matrix loader which can parse a header row
 * @param outm - reference to scby_matrix that will be read from file
 * @param has_header_row - TRUE if first row is header row,
 *  else if FALSE, then no header is assumed.
 * @param path - character string with path to file
 * @param ft - input file format
 * @return TRUE if matrix read successfully, FALSE o/w
 */
extern bool scby_read_matrix(
      scby_matrix &outm
    , const bool has_header_row
    , const char *path
    , const scby_matrix_utils::file_type ft
);

/**
 * reads a whitespace separated matrix from disk
 * @param outm - reference to scby_matrix that will be read from file
 * @param has_header_row - TRUE if first row is header row,
 *  else if FALSE, then no header is assumed.
 * @param path character string with path to file
 * @return TRUE if matrix successfully loaded into 'outm', FALSE otherwise
 */
extern bool scby_read_matrix_ascii(
      scby_matrix& outm
    , const bool has_header_row
    , const char* path
);

/**
 * reads a csv separated matrix from disk
 * @param outm - reference to scby_matrix that will be read from file
 * @param has_header_row - TRUE if first row is header row,
 *  else if FALSE, then no header is assumed.
 * @param path character string with path to file
 * @return TRUE if matrix successfully loaded into 'outm', FALSE otherwise
 */
extern bool scby_read_matrix_csv(
      scby_matrix &outm
    , const bool has_header_row
    , const char *path
);

/**
 * Writes binary representation of a fast matrix
 * @param outm - the matrix to output
 * @param path - the path to write the matrix to
 * @return TRUE if write successful, FALSE otherwise
 */
extern bool scby_write_matrix_bin(
      const scby_matrix &outm
    , const char *path
);

/**
 * Writes binary representation of a fast matrix
 * @param in_csv_path - path to input csv matrix
 * @param out_bin_path - path to write binary matrix to
 * @param has_header_row - TRUE if first row is header row,
 *  else if FALSE, then no header is assumed.
 * @return TRUE if successful, FALSE otherwise
 */
extern bool scby_convert_matrix_csv_to_bin(
      const char *in_csv_path
    , const char *out_bin_path
    , const bool has_header_row
);

/**
 * Writes csv ascii representation of binary matrix to disk
 * @param in_bin_path - path to write binary matrix to
 * @param out_csv_path - path to output csv matrix
 * @return TRUE if successful, FALSE otherwise
 */
extern bool scby_convert_matrix_bin_to_csv(
      const char *in_bin_path
    , const char *out_csv_path
);

/** 
 * Resizes a coordinates matrix where each column is ordered like so:
 *  Col = x.structA, y.structB, z.structC, ... x.structN, y.structN, z.structN
 *
 *  Each row is ordered like so:
 *  Row 1 = 1st node: x_1A, y_1A, z_1A, x_1B, y_1B, z_1B, ... x_1N, y_1N, z_YN
 *  Row 2 = 2nd node: x_2A, y_2A, z_2A, x_2B, y_2B, z_2B, ... x_2N, y_2N, z_2N
 *  ...
 *  Row k = k-th node: x_kA, y_kA, z_kA, z_kA, ... x_kN, y_kN, z_kN
 *
 * So that a sample takes up only a 1 column instead of n_dims columns.
 * The resulting format for 3-D input would be:
 *  Each column is ordered like so:
 *  Col = structA, structB, structC, ... structN
 *
 *  Each row is ordered like so:
 *  Row 1 = 1st node_x: x_1A, x_1B, ... x_1N
 *  Row 2 = 1st node_y: y_1A, y_1B, ... y_1N
 *  Row 3 = 1st node_z: z_1A, z_1B, ... z_1N
 *  Row 4 = 2nd node_x: x_2A, x_2B, ... x_2N
 *  Row 5 = 2nd node_y: y_2A, y_2B, ... y_2N
 *  Row 6 = 2nd node_y: z_2A, z_2B, ... z_2N
 *
 * The total number of elements is the same but dimensions are now
 * new n_rows = original.n_rows * n_dims
 * new n_cols = original.n_cols / n_dims
 *
 * Therefore, n_cols must be multiple of n_dims!
 * @param mat - the matrix to reshape
 * @param ndims - the number of dimensions each node point occupies
 * @return true if format successful, false o/w
 */
extern bool scby_reshape_mat_col_maj(scby_matrix &mat, const scby_uint n_dims);

/**
 * Row major version of scby_reshape_mat
 * @return true if format successful, false o/w
 */
extern bool scby_reshape_mat_row_maj(scby_matrix &mat, const scby_uint n_dims);

/**
 * Reads binary input matrix and internally resizes such that each sample only occupies a single column
 * @param mat - output matrix - each sample is a single column
 * @param in_matrix_path - path to input matrix
 * @param n_dims - number of base dimensions for each sample
 * @param obs_in_rows - if true, matrix is transposed before being returned
 * @return true if load and format successful, false o/w
 */
extern bool scby_load_mat_bin_and_reshape_col_maj(
      scby_matrix &mat
    , const char *in_matrix_path
    , const scby_uint n_dims
    , const bool obs_in_rows = false
);

// Wrapping multi-line macros in do { ... } while(0) so that they behave
// similar to single-line statements.
// http://www.pixelstech.net/article/1390482950-do-%7B-%7D-while-%280%29-in-macros

/**
 * Macro for computing squared distance between 'a' and 'b'
 * Using macro to guarantee inlining
 * @param res - (scby_real) resulting squared distance value is stored here
 * @param buff - (scby_vec_col) buffer used for transient calculations
 * @param a - column vector to compute distance from b
 * @param b - column vector to compute distance from a
 */
#define SCBY_DIST2(res, buff, a, b) \
    do { \
        buff = (a) - (b); \
        buff %= buff; \
        res = scby_matrix_utils::sum(buff); \
    } while(0)

 /**
  * Macro for computing squared distance between 'a' and 'b' and incrementing
  * res by this amount. Using macro to guarantee inlining
  * @param res - (scby_real) resulting squared distance value is stored here
  * @param buff - (scby_vec_col) buffer used for transient calculations
  * @param a - column vector to compute distance from b
  * @param b - column vector to compute distance from a
  */
#define SCBY_INC_BY_DIST2(res, buff, a, b) \
    do { \
        buff = (a) - (b); \
        buff %= buff; \
        res += scby_matrix_utils::sum(buff); \
    } while(0)

#endif // SCBY_ARMADILLO_H
