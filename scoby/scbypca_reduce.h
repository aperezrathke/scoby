//****************************************************************************
// scbypca_reduce.h
//****************************************************************************

/**
 * @author Alan Perez-Rathke
 * @date December 1st, 2015
 *
 * Utility for reducing dimensionality of an input matrix using principal
 * component analysis:
 *
 * https://en.wikipedia.org/wiki/Principal_component_analysis
 */

#ifndef SCBYPCA_REDUCE_H
#define SCBYPCA_REDUCE_H

//****************************************************************************
// Includes
//****************************************************************************

#include "scbybuild.h"
#include "scbytypes.h"

//****************************************************************************
// Arguments
//****************************************************************************

/**
 * Enumerated user arguments for use in argv, argc main
 */
enum escby_pca_reduce_args {
    escby_pca_reduce_exe_name=0,
    escby_pca_reduce_app_name,
    escby_pca_reduce_perc_keep,
    escby_pca_reduce_obs_in_rows,
    escby_pca_reduce_n_dims,
    escby_pca_reduce_in_matrix_path,
    escby_pca_reduce_out_matrix_path,
    escby_pca_reduce_max_num_args
};

/**
 * Arguments to cropping utility
 */
struct scby_pca_reduce_args_t {
    /**
     * The percentage of variance explained to keep in (0.0, 1.0]
     * = sum(eigenvalues 1 to k) / sum(eigenvalues 1 to n)
     * where k <= n and n is total number of eigenvalues
     */
    scby_real perc_keep;
    /**
     * Each row of input matrix is an observation and each column is a variable,
     * If this is the case, then pass in '1', else pass in '0'
     * (ie. if samples are in rows - pass '1', else pass '0')
     * Note: for performance, we prefer observations in columns as it avoids
     * an extra transpose operation
     */
    bool obs_in_rows;
    /**
     * Number of base dimensions of each observation
     */
    scby_uint n_dims;
    /**
     * Path to input matrix
     */
    const char* in_matrix_path;
    /**
     * Path to write reduced matrix
     */
    const char* out_matrix_path;
};

//****************************************************************************
// Extern(s)
//****************************************************************************

/**
 * Implementation based on MLPack library
 * Note: reduced mat and mat can be passed in as same variable
 * @param reduced_mat - the output dim reduced matrix (column major)
 * @param mat - column major data matrix - samples are in columns, variables are in rows
 *  - matrix will be transformed to PCA space and rows contributing past variance threshold
 *  (as specified by perc_keep) will be removed - this transformed version is stored in trans_mat
 * @param perc_keep - the percent variance to keep in (0,1]
 * @return actual amount of variance retained, which will always be greater than or equal to perc_keep.
 */
extern double scby_pca_reduce(
        scby_matrix &reduced_mat,
        const scby_matrix &mat,
        const double perc_keep);

/**
 * Performs PCA-based dimensionality reduction
 */
extern void scby_pca_reduce_bin(const scby_pca_reduce_args_t &args);

#endif // SCBYPCA_REDUCE_H
