/**
 * @author Alan Perez-Rathke
 * @date August 24, 2015
 */

#include "scbybuild.h"
#include "scbytypes.h"
#include "scbyassert.h"
#include "scbyarmadillo.h"

#include <fstream>

/**
 * Wrapper matrix loader which can parse a header row
 * @param outm - reference to scby_matrix that will be read from file
 * @param has_header_row - TRUE if first row is header row,
 *  else if FALSE, then no header is assumed.
 * @param path - character string with path to file
 * @param ft - input file format
 * @return TRUE if matrix read successfully, FALSE o/w
 */
bool scby_read_matrix(
        scby_matrix &outm
    , const bool has_header_row
    , const char *path
    , const scby_matrix_utils::file_type ft
    )
{
    std::ifstream in;
    in.open(path);

    bool status = false;

    // Early out if invalid stream
    if (!in) {
        goto cleanup;
    }

    // Eat first line if contains header row
    if (has_header_row)
    {
        std::string line;
        if (!std::getline(in, line))
        {
            goto cleanup;
        }

        if (!in) {
            goto cleanup;
        }
    }

    // Feed stream to matrix library
    status = outm.load(in, ft);

cleanup:
    in.close();
    return status;
}

/**
 * reads a whitespace separated matrix from disk
 * @param outm - reference to scby_matrix that will be read from file
 * @param has_header_row - TRUE if first row is header row,
 *  else if FALSE, then no header is assumed.
 * @param path character string with path to file
 * @return TRUE if matrix successfully loaded into 'outm', FALSE otherwise
 */
bool scby_read_matrix_ascii(
      scby_matrix& outm
    , const bool has_header_row
    , const char* path)
{
    return scby_read_matrix(
          outm
        , has_header_row
        , path
        , scby_matrix_utils::raw_ascii
        );
}

/**
 * reads a csv separated matrix from disk
 * @param outm - reference to scby_matrix that will be read from file
 * @param has_header_row - TRUE if first row is header row,
 *  else if FALSE, then no header is assumed.
 * @param path character string with path to file
 * @return TRUE if matrix successfully loaded into 'outm', FALSE otherwise
 */
bool scby_read_matrix_csv(
      scby_matrix& outm
    , const bool has_header_row
    , const char* path)
{
    return scby_read_matrix(
          outm
        , has_header_row
        , path
        , scby_matrix_utils::csv_ascii
        );
}

/**
 * Writes binary representation of a fast matrix
 * @param outm - the matrix to output
 * @param path - the path to write the matrix to
 * @return TRUE if write successful, FALSE otherwise
 */
bool scby_write_matrix_bin(
      const scby_matrix& outm
    , const char* path
)
{
    if (outm.n_elem <= 0)
    {
        std::cout << "Cannot write empty matrix\n";
        return false;
    }

    if (path == NULL)
    {
        std::cout << "Cannot write matrix to invalid file name\n";
        return false;
    }

    std::cout << "Writing " << outm.n_rows << " x " << outm.n_cols << " fast matrix to: " << path << std::endl;

    return outm.save(path, scby_matrix_bin_io_mode);
}

/**
 * Writes binary representation of a fast matrix
 * @param in_csv_path - path to input csv matrix
 * @param out_bin_path - path to write binary matrix to
 * @param has_header_row - TRUE if first row is header row,
 *  else if FALSE, then no header is assumed.
 * @return TRUE if successful, FALSE otherwise
 */
bool scby_convert_matrix_csv_to_bin(
      const char* in_csv_path
    , const char* out_bin_path
    , const bool has_header_row
)
{
    scby_matrix outm;

    std::cout << "scby_convert_matrix_csv_to_bin:\n";
    std::cout << "\tin_csv_path: " << in_csv_path << std::endl;
    std::cout << "\tout_bin_path: " << out_bin_path << std::endl;
    std::cout << "\thas_header_row: " << (has_header_row ? "true" : "false") << std::endl;

    std::cout << "Reading " << in_csv_path << std::endl;
    if (!scby_read_matrix_csv(
                  outm
                , has_header_row
                , in_csv_path
            ))
    {
        std::cout << "Unable to read: " << in_csv_path << std::endl;
        return false;
    }

    return scby_write_matrix_bin(outm, out_bin_path);
}

/**
 * Writes csv ascii representation of binary matrix to disk
 * @param in_bin_path - path to write binary matrix to
 * @param out_csv_path - path to output csv matrix
 * @return TRUE if successful, FALSE otherwise
 */
bool scby_convert_matrix_bin_to_csv(
      const char* in_bin_path
    , const char* out_csv_path
)
{
    scby_matrix outm;

    std::cout << "scby_convert_matrix_bin_to_csv:\n";
    std::cout << "\tin_bin_path: " << in_bin_path << std::endl;
    std::cout << "\tout_csv_path: " << out_csv_path << std::endl;

    std::cout << "Reading " << in_bin_path << std::endl;
    const bool status = outm.load(in_bin_path);
    if (!status) {
        std::cout << "Error: unable to load matrix at " << in_bin_path << std::endl;
        return false;
    }

    std::cout << "Writing " << outm.n_rows << " x " << outm.n_cols << " matrix to " << out_csv_path << std::endl;
    outm.save(out_csv_path, scby_matrix_utils::csv_ascii);
    return true;
}

/** Resizes a coordinates matrix where
 *  Each column is ordered like so:
 *  Col = x.structA, y.structB, z.structC, ... x.structN, y.structN, z.structN
 *
 *  Each row is ordered like so:
 *  Row 1 = 1st node: x_1A, y_1A, z_1A, x_1B, y_1B, z_1B, ... x_1N, y_1N, z_YN
 *  Row 2 = 2nd node: x_2A, y_2A, z_2A, x_2B, y_2B, z_2B, ... x_2N, y_2N, z_2N
 *  ...
 *  Row k = k-th node: x_kA, y_kA, z_kA, z_kA, ... x_kN, y_kN, z_kN
 *
 * So that a sample takes up only a 1 column instead of n_dims columns.
 * The resulting format for 3-D input would be:
 *  Each column is ordered like so:
 *  Col = structA, structB, structC, ... structN
 *
 *  Each row is ordered like so:
 *  Row 1 = 1st node_x: x_1A, x_1B, ... x_1N
 *  Row 2 = 1st node_y: y_1A, y_1B, ... y_1N
 *  Row 3 = 1st node_z: z_1A, z_1B, ... z_1N
 *  Row 4 = 2nd node_x: x_2A, x_2B, ... x_2N
 *  Row 5 = 2nd node_y: y_2A, y_2B, ... y_2N
 *  Row 6 = 2nd node_y: z_2A, z_2B, ... z_2N
 *
 * The total number of elements is the same but dimensions are now
 * new n_rows = original.n_rows * n_dims
 * new n_cols = original.n_cols / n_dims
 *
 * Therefore, n_cols must be multiple of n_dims!
 * @param mat - the matrix to reshape
 * @param ndims - the number of dimensions each node point occupies
 * @return true if format successful, false o/w
 */
bool scby_reshape_mat_col_maj(scby_matrix& mat, const scby_uint n_dims) {
    if (n_dims > 1) {
        // resize matrix so that each structure is represented by a single column instead of ndim columns
        // note: this only works because armadillo library stores columns consecutively in memory
        if (mat.n_cols % n_dims != 0) {
            std::cout << "Error: attempting to reshape matrix with columns not multiple of dimensions.\n";
            return false;
        }
#ifdef SCBY_BUILD_ENABLE_ASSERT
        scby_matrix orig_mat = mat;
#endif // SCBY_BUILD_ENABLE_ASSERT
        mat.set_size(mat.n_rows * n_dims, mat.n_cols / n_dims);
#ifdef SCBY_BUILD_ENABLE_ASSERT
        // Verify memory was preserved
        scby_assert(orig_mat.n_elem == mat.n_elem);
        scby_assert(0 == memcmp(orig_mat.memptr(), mat.memptr(), orig_mat.n_elem * sizeof(scby_real)));
#endif // SCBY_BUILD_ENABLE_ASSERT
    }
    return true;
}

/**
 * Row major version of scby_reshape_coords
 * @return true if format successful, false o/w
 */
bool scby_reshape_mat_row_maj(scby_matrix& mat, const scby_uint n_dims) {
    // early out if n_dims is <= 1
    if (n_dims > 1) {
        // Compaction utility expects observations in columns, so we must transpose matrix
        scby_matrix trans_mat = scby_matrix_utils::trans(mat);
        if (!scby_reshape_mat_col_maj(trans_mat, n_dims)) {
            return false;
        }
        // And now transpose again to return to row-major format
        // Note, this transpose is not inverse of original transpose as matrix now
        // has different dimensions
        mat = scby_matrix_utils::trans(trans_mat);
    }
    return true;
}

/**
 * Reads binary input matrix and internally resizes such that each sample only occupies a single column
 * @param mat - output matrix - each sample is a single column
 * @param in_matrix_path - path to input matrix
 * @param n_dims - number of base dimensions for each sample
 * @param obs_in_rows - if true, matrix is transposed before being returned
 * @return true if load and format successful, false o/w
 */
bool scby_load_mat_bin_and_reshape_col_maj(
        scby_matrix &mat,
        const char *in_matrix_path,
        const scby_uint n_dims,
        const bool obs_in_rows) {

    // Read from disk
    std::cout << "Reading input coordinates matrix: " << ((in_matrix_path != NULL) ? in_matrix_path : "") << std::endl;
    const bool result = (in_matrix_path != NULL)
                        ? mat.load(in_matrix_path, scby_matrix_bin_io_mode) : false;
    if (!result) {
        std::cout << "Failed to load matrix\n";
        return false;
    }
    std::cout << "Finished reading matrix of size " << mat.n_rows << " x " << mat.n_cols << std::endl;
    // Reformat so that each sample only takes a single column or row
    if (obs_in_rows) {
        std::cout << "Transposing from row major to column major\n";
        mat = scby_matrix_utils::trans(mat);
    }
    if (!scby_reshape_mat_col_maj(mat, n_dims)) {
        std::cout << "Failed to format matrix.\n";
        return false;
    }
    std::cout << "Reformatted matrix to " << mat.n_rows << " x " << mat.n_cols << std::endl;
    return true;
}
