/**
 * Streaming, hierarchical, weighted affinity propagation - a C++ port of the Matlab StrAP algorithm.
 * Uses reservoir size and exemplar count thresholds as criterion for re-running affinity propagation.
 *
 * Zhang, Xiangliang, Cyril Furtlehner, and Michele Sebag. "Data streaming with affinity propagation."
 * In Machine Learning and Knowledge Discovery in Databases, pp. 628-643. Springer Berlin Heidelberg, 2008.
 *
 * Zhang, Xiangliang, Cyril Furtlehner, Cecile Germain-Renaud, and Michele Sebag.
 * "Data stream clustering with affinity propagation." Knowledge and Data Engineering,
 * IEEE Transactions on 26, no. 7 (2014): 1644-1656.
 */
#ifndef SCBYSHWAP_H
#define SCBYSHWAP_H

//****************************************************************************
// Includes
//****************************************************************************

#include "scbybuild.h"
#include "scbytypes.h"
#include "scbyapcluster.h"

//****************************************************************************
// Exemplar association heuristics
//****************************************************************************

/**
 * Heuristics for associating an unlabeled sample to an exemplar. If heuristic fails
 * to associate sample, it is added to the unlabeled sample reservoir for processing
 * via local affinity propagation.
 * @TODO - Add heuristics based on local max pdist2 and local avg pdist2
 */
enum escby_shwap_ex_assoc_heur {
    /**
     * Associate an unlabeled sample to an exemplar if the squared distance to the
     * nearest exemplar is less than or equal to the average distance of all samples to their respective exemplars
     */
    escby_shwap_ex_assoc_heur_nasc_univ_avg_dist2 = 0,

    /**
     * Associate an unlabeled sample to an exemplar if the squared distance to the nearest exemplar is less than or
     * equal to the median of the average distance of samples to their respective exemplars
     */
    escby_shwap_ex_assoc_heur_nasc_univ_med_avg_dist2,

    /**
     * The number of association heuristics available
     */
    escby_shwap_ex_assoc_heur_num
};

//****************************************************************************
// Default SHWAP parameter values
//****************************************************************************

/**
 * To have consistent defaults for all declarations of cluster method, defer
 * to these macro values
 */

/**
 * Default initialization size:
 * The first init_size elements are clustered using affinity propagation to
 * find the initial exemplar and cluster assignments
 */
#define SCBY_SHWAP_DEF_INIT_SIZE 2000

/**
 * Default heuristic for attempting to associate an unlabeled sample to an existing exemplar
 */
#define SCBY_SHWAP_DEF_EX_ASSOC_HEUR escby_shwap_ex_assoc_heur_nasc_univ_med_avg_dist2

/**
 * Default scale factor for association cutoff (cutoff distance is scaled by this factor)
 */
#define SCBY_SHWAP_DEF_EX_ASSOC_HEUR_SCALE as_scby_real(0.25)

/**
 * Default threshold for triggering a weighted, local affinity propagation (ap) on samples which failed
 * to be associated to an exemplar. If the number of unlabeled samples reaches this count, ap is triggered.
 */
#define SCBY_SHWAP_DEF_MAX_UNLABELED_THRESH 1500

/**
 * Default threshold for triggering a weighted, hierarchical affinity propagation run on only
 * the exemplars. This is to attempt to merge exemplars and reduce the total count in order
 * for the local affinity propagation runs to be performant.
 */
#define SCBY_SHWAP_DEF_MAX_EXEMPLARS_THRESH 1250

/**
 * Default decay window:
 * Each exemplar has a window length defined as the number of samples that have been processed since
 * the last time a sample was associated to that exemplar. If the window length exceeds the decay value
 * then the exemplar is removed. Furthermore, the multiplicity weights associated to that exemplar are
 * reduced as a function of this decay length.
 */
#define SCBY_SHWAP_DEF_DECAY_WINDOW 10000

/**
 * Default preference convergence factor:
 * Basically, if stock affinity propagation fails to convergence, we can fiddle with the preferences
 * in a reasonable way such that the subsequent run will likely converge. Only applies to non-init
 * AP runs.
 */
#define SCBY_SHWAP_DEF_P_CONV_DECAY_FACT as_scby_real(1.05)

/**
 * Default preference convergence max attempts
 * The maximum number of times to fiddle with the input preferences in attempt to get affinity
 * propagation to converge before giving up. Only applies to non-init AP runs.
 */
#define SCBY_SHWAP_DEF_P_CONV_MAX_ATTEMPTS 5

/**
 * Default initial stock AP cluster max attempts for convergence
 * The maximum number of times to swizzle the input samples in an attempt to obtain an
 * ensemble that converges during the initial affinity propagation run.
 */
#define SCBY_SHWAP_DEF_INIT_SWIZ_MAX_ATTEMPTS 5

/**
 * Default restore positions matrix behavior
 * During initialization, the input positions matrix may be randomly swizzled
 * in order to obtain a convergent ensemble. If true, the input positions
 * matrix is restored to the original input state; else, the positions matrix
 * is left modified.
 */
#define SCBY_SHWAP_DEF_RESTORE_POSITIONS_MAT false

//****************************************************************************
// SHWAP - Streaming, hierarchical, weighted affinity propagation
//****************************************************************************

/**
 * Streaming, hierarchical, weighted affinity propagation for clustering large
 * data sets. - based on StrAP algorithm.This version internally computes default
 * pairwise similarities and preferences based on input positions_mat and q
 * parameters
 * BEGIN common outputs and inputs:
 * @param samples_to_clusters - Stores cluster assignment for each sample - output
 *  is size n where n is number of samples
 * @param clusters_to_samples - Stores sample assignments for each cluster
 * @param exemplars - stores index of exemplar sample for each cluster - output is
 *  size k where k is number of clusters found
 * @param positions_mat - Matrix of sample coordinates, each sample is assumed to
 *  take a single column. This matrix may be re-ordered during the initial AP
 *  run in order to obtain a convergent ensemble. If the original order must be
 *  preserved, set restore_positions_mat=true
 * @param timers - (optional) Used for timing various operations
 * BEGIN shwap parameters
 * @param init_size - The first init_size elements are clustered using affinity
 *  propagation to find the initial exemplar and cluster assignments
 * @param ex_assoc_heur - The type of heuristic to use for attempting to associate
 *  an unlabeled sample to an existing exemplar
 * @param ex_assoc_heur_scale - The association heuristic cutoff distance is
 *  scaled by this factor->values less than 1.0 make it harder to associate an
 *  unlabeled sample to an exemplar->values greater than 1.0 make it easier to
 *  associate an unlabeled sample to an exemplar
 * @param max_unlabeled_thresh - Threshold for triggering a weighted, local
 *  affinity propagation on samples which failed to be associated to an exemplar
 * @param max_exemplars_thresh - Threshold for triggering a weighted, hierarchical
 *  affinity propagation run on only the exemplars.This is to attempt to merge
 *  exemplars and reduce the total count in order for the local affinity
 *  propagation runs to be performant.
 * @param decay_window - Each exemplar has a window length defined as the number
 *  of samples that have been processed since the last time a sample was
 *  associated to that exemplar.If the window length exceeds the decay value
 *  then the exemplar is removed.Furthermore, the multiplicity weights
 *  associated to that exemplar are reduced as a function of this decay length.
 *  BEGIN classic affinity propagation parameters :
 * @param q - specifies which quantile similarity to assign as the no-prior
 *  preference
 * @param maxits - Maximum number of iterations for each affinity propagation run
 * @param convits - A single run of affinity propagation terminates if the
 *  exemplars have not changed for convits iterations
 * @param lam - Damping factor; should be a value in the range [0.5, 1); higher
 *  values correspond to heavy damping which may be needed if oscillations occur
 * @param nonoise - If false, adds a small amount of noise to similarities to
 *  prevent degenerate cases
 * @param p_conv_decay_fact -  If stock affinity propagation fails to convergence,
 *  we can fiddle with the preferences in a reasonable way such that the
 *  subsequent run will likely converge.Only applies to non - init AP runs.
 * @param p_conv_max_attempts - The maximum number of times to fiddle with the
 *  input preferences in attempt to get affinity propagation to converge before
 *  giving up.Only applies to non - init AP runs.
 * @param init_swiz_max_attempts - The maximum number of times to swizzle the input
 *  samples in an attempt to obtain an ensemble that converges during the initial
 *  affinity propagation run.
 * @param restore_positions_mat - During initialization, the input positions
 *  matrix may be randomly swizzled in order to obtain a convergent ensemble.If
 *  true, the input positions matrix is restored to the original input state;
 *  else, the positions matrix is left modified.
 * @return TRUE if converged, FALSE o/w
 */
extern bool scby_shwap_pdist2(scby_index_vec_t &samples_to_clusters,
                              scby_ap_c2s_map_t &clusters_to_samples,
                              scby_index_vec_t &exemplars,
                              scby_matrix &positions_mat,
                              scby_ap_timers *timers=NULL,
                              const scby_uint init_size=SCBY_SHWAP_DEF_INIT_SIZE,
                              const escby_shwap_ex_assoc_heur ex_assoc_heur=SCBY_SHWAP_DEF_EX_ASSOC_HEUR,
                              const scby_real ex_assoc_heur_scale=SCBY_SHWAP_DEF_EX_ASSOC_HEUR_SCALE,
                              const scby_uint max_unlabeled_thresh=SCBY_SHWAP_DEF_MAX_UNLABELED_THRESH,
                              const scby_uint max_exemplars_thresh=SCBY_SHWAP_DEF_MAX_EXEMPLARS_THRESH,
                              const scby_uint decay_window=SCBY_SHWAP_DEF_DECAY_WINDOW,
                              const scby_real q=SCBY_AP_DEF_PREF_QUANTILE,
                              const size_t maxits=SCBY_AP_DEF_MAXITS,
                              const size_t convits=SCBY_AP_DEF_CONVITS,
                              const scby_real lam=SCBY_AP_DEF_LAM,
                              const bool nonoise=SCBY_AP_DEF_NONOISE,
                              const scby_real p_conv_decay_fact=SCBY_SHWAP_DEF_P_CONV_DECAY_FACT,
                              const scby_uint p_conv_max_attempts=SCBY_SHWAP_DEF_P_CONV_MAX_ATTEMPTS,
                              const scby_uint init_swiz_max_attempts =SCBY_SHWAP_DEF_INIT_SWIZ_MAX_ATTEMPTS,
                              const bool restore_positions_mat=SCBY_SHWAP_DEF_RESTORE_POSITIONS_MAT);

#endif // SCBYSHWAP_H
