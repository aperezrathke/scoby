/**
 * @author Alan Perez-Rathke
 * @date August 24, 2015
 */

//****************************************************************************
// Includes
//****************************************************************************

#include "scbybuild.h"
#include "scbyassert.h"
#include "scbycompute_pdist.h"
#include "scbycompute_pdist_front.h"
#include "scbysymmatrix_io.h"

//****************************************************************************
// Helper namespace
//****************************************************************************

/**
 * Common utilities for RMSD computation
 */
namespace {

    /**
     * Core RMSD data
     */
    struct scbyRMSDCore {
        scby_matrix positions_mat;
        scby_sym_matrix rmsd_mat;
        scby_uint n_nodes;
    };

    /**
     * Initializes RMSD computation state
     */
    bool rmsd_preamble(scbyRMSDCore &core,
                       // Input user parameters
                       const char *in_bin_path,
                       const unsigned int n_dims,
                       const char *out_bin_path,
                       const char *out_csv_path) {

        // verify input path is not null
        scby_assert(NULL != in_bin_path);

        // verify at least 1 output path is not null
        scby_assert((NULL != out_bin_path) || (NULL != out_bin_path));

        std::cout << "Core parameters:\n";
        std::cout << "\tin_bin_path: " << in_bin_path << std::endl;
        std::cout << "\tndim: " << n_dims << std::endl;
        std::cout << "\tout_bin_path: " << ((out_bin_path != NULL) ? out_bin_path : "null") << std::endl;
        std::cout << "\tout_csv_path: " << ((out_csv_path != NULL) ? out_csv_path : "null") << std::endl;

        // verify at least 1 dimension
        if (n_dims < 1) {
            std::cout << "Invalid format: must have at least 1 dimension\n";
            return false;
        }

        // load node positions
        std::cout << "Loading and formatting positions matrix\n";
        scby_matrix &positions_mat = core.positions_mat;
        if (!scby_load_mat_bin_and_reshape_col_maj(positions_mat, in_bin_path, n_dims)) {
            return false;
        }

        // verify have at least 1 node
        scby_assert(positions_mat.n_rows % n_dims == 0);
        const unsigned int n_nodes = core.n_nodes = positions_mat.n_rows / n_dims;
        if (n_nodes < 1) {
            std::cout << "Invalid format: number of nodes must be greater than or equal to 1\n";
            return false;
        }

        // verify have at least 2 structures
        const unsigned int n_structs = positions_mat.n_cols;
        if (n_structs < 2) {
            std::cout << "Invalid format: number of nodes must be greater than or equal to 2\n";
            return false;
        }

        // initialization successful
        return true;
    }

    /**
     * writes RMSD results to disk
     */
    void rmsd_write_data(const scby_sym_matrix &rmsd_mat,
                         const char *out_bin_path,
                         const char *out_csv_path) {
        // write binary matrix
        if (out_bin_path != NULL) {
            std::cout << "Writing binary RMSD matrix to: " << out_bin_path << std::endl;
            scby_sym_matrix_write_bin(rmsd_mat, out_bin_path);
        }

        // write csv matrix
        if (out_csv_path != NULL) {
            std::cout << "Writing ASCII CSV RMSD matrix to: " << out_csv_path << std::endl;
            scby_sym_matrix_write_csv(rmsd_mat, out_csv_path);
        }
        std::cout << "Finished writing RMSD data." << std::endl;
    }

} // end of helper namespace

//****************************************************************************
// Single-threaded (serial) RMSD implementation
//****************************************************************************

/**
 * @param in_bin_path - The input bin path. Each column represents the
 * coordinate of the structure nodes at a single dimension. Each row
 *  represents the positions of the i-th node. For example, assume N
 *  structures (e.g., N protein structures):
 *
 * Each column is ordered like so:
 *  Col = x.structA, y.structB, z.structC, ... x.structN, y.structN, z.structN
 *
 * Each row is ordered like so:
 *  Row 1 = 1st node: x_1A, y_1A, z_1A, x_1B, y_1B, z_1B, ... x_1N, y_1N, z_YN
 *  Row 2 = 2nd node: x_2A, y_2A, z_2A, x_2B, y_2B, z_2B, ... x_2N, y_2N, z_2N
 *      ...
 *  Row k = k-th node: x_kA, y_kA, z_kA, z_kA, ... x_kN, y_kN, z_kN
 *
 * @param n_dims - the number of dimensions for each position (e.g. 2-D or 3-D),
 *  must be a positive integer. Dimensions are assumed to be consecutive. So,
 *  for a 2-D representation, input columns and rows (for file specified by
 *  in_csv_path) are:
 *
 *  Col = x.structA, y.structB, ... x.structN, y.structN
 *  Row = 1st node: x_1A, y_1A, ... x_1N, y_1N
 *      ...
 *  Row k = k-th node: x_kA, y_kA, ... x_kN, y_1N
 *
 * @param out_bin_path - output path for binary matrix for use with clustering.
 *  if null, then will not be output.
 * @param out_csv_path - OPTIONAL output path to plain-text csv file of RMSD
 *  distances. If non-null, output will be lower symmetric.
 */
void scby_compute_rmsd_front(const char *in_bin_path,
                             const unsigned int n_dims,
                             const char *out_bin_path,
                             const char *out_csv_path) {

    // initialize core data
    scbyRMSDCore core;
    if (!rmsd_preamble(core, in_bin_path, n_dims, out_bin_path, out_csv_path)) {
        return;
    }

    std::cout << "Computing RMSD" << std::endl;
    scby_compute_rmsd(core.rmsd_mat,
                      core.positions_mat,
                      core.n_nodes);
    std::cout << "Finished RMSD computations." << std::endl;

    rmsd_write_data(core.rmsd_mat, out_bin_path, out_csv_path);
}

//****************************************************************************
// Multi-threaded (parallel) RMSD implementation
//****************************************************************************

/**
 * multi-threaded version of scby_compute_rmsd
 * @param n_threads - the number of threads to allocate
 * @param inc_by - the size of the work batch
 * @param in_bin_path - see scby_compute_rmsd
 * @param n_dims - see scby_compute_rmsd
 * @param out_bin_path - see scby_compute_rmsd
 * @param out_csv_path - see scby_compute_rmsd
 */
void scby_compute_rmsd_mt_front(
    const unsigned int n_threads,
    const unsigned int inc_by,
    const char *in_bin_path,
    const unsigned int n_dims,
    const char *out_bin_path,
    const char *out_csv_path) {

    std::cout << "Multi-thread parameters:\n";
    std::cout << "\tn_threads: " << n_threads << std::endl;
    std::cout << "\tinc_by: " << inc_by << std::endl;

    if (n_threads < 1) {
        std::cout << "Error: thread count must be greater than 0.\n";
        return;
    }

    if (inc_by < 1) {
        std::cout << "Error: thread batch work increment must be greater than 0.\n";
        return;
    }

    // initialize core data
    scbyRMSDCore core;
    if (!rmsd_preamble(core, in_bin_path, n_dims, out_bin_path, out_csv_path)) {
        return;
    }

    // scoping so that we teardown threads as soon as they are unneeded
    {
        // init parallel mapper
        scbyParallelMapper pm;
        pm.init(n_threads);

        // perform RMSD calculations
        std::cout << "Computing RMSD" << std::endl;
        scby_compute_rmsd_mt(core.rmsd_mat,
                             core.positions_mat,
                             core.n_nodes,
                             pm,
                             inc_by);
        std::cout << "Finished RMSD computations." << std::endl;
    }

    // write results to disk
    rmsd_write_data(core.rmsd_mat, out_bin_path, out_csv_path);
}
