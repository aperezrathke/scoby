/**
 * @author Alan Perez-Rathke
 * @date December 1st, 2015
 *
 * Simple utility to crop ends of a matrix.
 * Useful for generating manageable test data
 */

#ifndef SCBYCROP_H
#define SCBYCROP_H

//****************************************************************************
// Includes
//****************************************************************************

#include "scbybuild.h"
#include "scbytypes.h"

//****************************************************************************
// Arguments
//****************************************************************************

// Enumerated user arguments for use in argv, argc main
enum escby_crop_args {
    escby_crop_exe_name=0,
    escby_crop_app_name,
    escby_crop_num_keep_rows,
    escby_crop_num_keep_cols,
    escby_crop_in_matrix_path,
    escby_crop_out_matrix_path,
    escby_crop_max_num_args
};

// Arguments to cropping utility
struct scby_crop_args_t {
    // The max number of rows to keep
    // if <= 0, then all rows are kept
    int num_keep_rows;
    // The max number of columns to keep
    // if <= 0, then all columns are kept
    int num_keep_cols;
    // Path to input matrix
    const char* in_matrix_path;
    // Path to write cropped matrix
    const char* out_matrix_path;
};

//****************************************************************************
// Extern(s)
//****************************************************************************

// Reduces size of input matrix - useful for generating test data
extern void scby_crop_bin(const scby_crop_args_t &args);

#endif // SCBYCROP_H
