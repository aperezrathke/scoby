/**
 * @author Alan Perez-Rathke
 * @date July 25, 2014
 */

#ifndef SCBYKDIST_H
#define SCBYKDIST_H

#include "scbybuild.h"
#include "scbyassert.h"
#include "scbytypes.h"
#include <vector>
#include <algorithm>

// used for finding epsilon
// from ester et al, section 4.2:
// http://www.it.uu.se/edu/course/homepage/infoutv/ht11/literature_material/KDD-96.final.frame.pdf
template <typename t_matrix>
void scby_get_sorted_k_dist(std::vector<scby_real>& out_sorted_k_dist, const t_matrix& dist_matrix, const size_t k = 4) {

    const typename t_matrix::size_type num_nodes = dist_matrix.size1();
    scby_assert(k <= num_nodes);

    out_sorted_k_dist.resize(num_nodes, 0.0);

    std::vector<scby_real> k_buffer(k);
    scby_real tmp = 0.0;

    for (typename t_matrix::size_type irow = 0; irow < num_nodes; ++irow) {

        // initialize k_buffer with first k elements
        for (size_t icol = 0; icol < k; ++icol) {
            k_buffer[icol] = dist_matrix(irow, icol);
        }

        // compare against remaining (num_nodes - k) elememnts
        for (size_t icol = k; icol < num_nodes; ++icol) {
            scby_real d = dist_matrix(irow, icol);
            for (size_t ik = 0; ik < k; ++ik) {
                if (k_buffer[ik] > d) {
                    tmp = k_buffer[ik];
                    k_buffer[ik] = d;
                    d = tmp;
                }
            }
        }

        out_sorted_k_dist[irow] = *(std::max_element(k_buffer.begin(), k_buffer.end()));

    } // end iteration over rows of matrix

    // sort the distances
    std::sort(out_sorted_k_dist.begin(), out_sorted_k_dist.end(), std::greater<scby_real>());
}

#endif // SCBYKDIST_H
