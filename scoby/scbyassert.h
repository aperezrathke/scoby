/**
 * @author Alan Perez-Rathke
 * @date July 25, 2014
 */

#ifndef SCBYASSERT_H
#define SCBYASSERT_H

#include "scbybuild.h"

// Make sure that NDEBUG is defined if asserts are not enabled
// C++ standard specifies to define NDEBUG if assertions are to be turned off
#ifndef SCBY_BUILD_ENABLE_ASSERT
#   ifndef NDEBUG
#       define NDEBUG
#   endif // NDEBUG
#endif // SCBY_BUILD_ENABLE_ASSERT

// Add compiler messages on MSVC and GCC compilers to let us know that
// assertions are enabled.
#if !defined(NDEBUG) || defined(SCBY_BUILD_ENABLE_ASSERT)
#   ifdef SCBY_BUILD_COMPILER_MSVC
#       pragma message("Assertions enabled.")
#   elif defined(SCBY_BUILD_COMPILER_GCC)
#       pragma message "Assertions enabled."
#   endif // compiler check
#endif // !defined(NDEBUG) || defined(SCBY_BUILD_ENABLE_ASSERT)

#include <assert.h>

// This part is ugly, but, at the end of the day, to toggle assertions,
// just define SCBY_BUILD_ENABLE_ASSERT within scbybuild.h or through command line
// and this part will override (if need be) to give the desired behavior.
//
// We want to route to the std assert implementation in the following
// conditions:
// - Asserts are disabled via undefined SCBY_BUILD_ENABLE_ASSERT directive
// - or standard NDEBUG preprocessor directive is not defined and asserts are enabled
#if (!defined(SCBY_BUILD_ENABLE_ASSERT) || (defined(SCBY_BUILD_ENABLE_ASSERT) && !defined(NDEBUG)))
// Defer to std assert implementation
#   define scby_assert assert
#else
// Else, in CMake build system, it's hard to modify the NDEBUG flag which is
// always defined in release builds. So if we really want asserts in release
// mode, we need to 'override' by routing to our own version.
inline void scby_assert(const bool expr)
{
    if (!expr)
    {
        // Call compiler/platform specific break routine
        // note: asm int 3 is inline assembly to call interrupt 3 - usually meant to
        // programmatically add a breakpoint (or trap to the debugger)
#ifdef SCBY_BUILD_COMPILER_MSVC
        // https://msdn.microsoft.com/en-us/library/45yd4tzz.aspx
        // see also __debugbreak() intrinsic:
        // https://msdn.microsoft.com/en-us/library/f408b4et.aspx
#ifdef _WIN64
        // inline assembly is not supported for 64-bit architectures
        __debugbreak();
#else
        __asm int 3;
#endif // _WIN64
#elif defined(SCBY_BUILD_COMPILER_GCC)
        // http://www.ibiblio.org/gferg/ldp/GCC-Inline-Assembly-HOWTO.html
        __asm__("int $3");
#elif defined(SCBY_BUILD_COMPILER_ICC)
        // intel compiler supports both microsoft and gcc style asm tags
        // but recommends gcc/gnu-style
        // https://software.intel.com/en-us/node/513428
        __asm__("int $3");
#else
#       error Unrecognized platform
# endif // compiler check
    }
}
#endif // override assert

#ifdef SCBY_BUILD_ENABLE_ASSERT
#   define scby_verify scby_assert
#else
#   define scby_verify( expr_ ) expr_
#endif

/**
 * Bounds checking that element i is within [l, u)
 */
#define scby_assert_bounds(i_, l_, u_) scby_assert( ((i_) >= (l_)) && ((i_) < (u_)) )

#endif // SCBYASSERT_H
