/**
 * C++ port of affinity propagation utility within APCluster R package
 *
 * Frey, Brendan J., and Delbert Dueck. "Clustering by passing messages between data points."
 * science 315, no. 5814 (2007): 972-976.
 *
 * Bodenhofer, Ulrich, Andreas Kothmeier, and Sepp Hochreiter.
 * "APCluster: an R package for affinity propagation clustering." Bioinformatics 27, no. 17 (2011): 2463-2464.
 */

//****************************************************************************
// Includes
//****************************************************************************

#include "scbybuild.h"
#include "scbyassert.h"
#include "scbytypes.h"
#include "scbyapcluster.h"
#include "scbycompute_pdist.h"

#include <algorithm>
#include <vector>

//****************************************************************************
// Utilities
//****************************************************************************

namespace {

    /**
     * Determines assignment of clusters to each sample
     * @param sample_to_cluster - output vector mapping sample id to cluster id
     * @param clusters_to_samples - output mapping of cluster identifiers to set of samples belonging to it
     * @param exemplar_s - exemplar similarities - each column is similarity of all samples to that exemplar
     */
    void assign_clusters(scby_index_vec_t &samples_to_clusters,
                         scby_ap_c2s_map_t &clusters_to_samples,
                         const scby_matrix &exemplar_s) {
        samples_to_clusters.set_size(exemplar_s.n_rows);
        clusters_to_samples.clear();
        clusters_to_samples.resize(exemplar_s.n_cols);
        for (size_t i=0; i<exemplar_s.n_rows; ++i) {
            exemplar_s.row(i).max(samples_to_clusters.at(i));
            scby_assert_bounds(samples_to_clusters[i], 0, exemplar_s.n_cols);
            clusters_to_samples[samples_to_clusters[i]].push_back(i);
        }
    }

    /**
     * Allocates a new contiguous view of the matrix by selecting the columns specified
     * by parameter index vector into a single contiguous matrix
     * @param m_view - output matrix of size m.n_row x indices.size()
     *  - will contiguously contain the columns specified by parameter indices vector
     * @param indices - an integral vector type specifying which column indices to keep
     */
    void select_cols(scby_matrix &m_view, const scby_matrix &m, const scby_index_vec_t &indices) {
        const size_t n_cols = indices.size();
        const size_t col_bytes = sizeof(scby_real) * m.n_rows;
        scby_assert(col_bytes > 0);
        m_view.set_size(m.n_rows, n_cols);
        for (size_t i=0; i<n_cols; ++i) {
            scby_assert_bounds(indices[i], 0, m.n_cols);
            memcpy(
                m_view.colptr(i)
                , m.colptr(indices[i])
                , col_bytes
            );
        }
    }

    /**
     * Verifies assumption that all values are finite and less than SCBY_REAL_MAX in the off-diagonal
     */
    bool check_s(const scby_matrix &s) {
        bool result = true;
        size_t i=0, j=0;
        for (; i<s.n_rows; ++i) {
            for (; j<s.n_cols; ++j) {
                result &= std::isfinite(s(i,j));
                scby_assert(result);
                result &= s(i,j) < SCBY_REAL_MAX;
                scby_assert(result);
                if (!result) {
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Verifies assumption that exemplar is assigned to its own cluster
     */
    bool check_c(const scby_index_vec_t &samples_to_clusters, const scby_index_vec_t &I) {
        bool result = true;
        size_t i;
        // Bounds check cluster assignments
        for (i=0; i<samples_to_clusters.size(); ++i) {
            result &= ((samples_to_clusters[i] >= 0) && (samples_to_clusters[i] < I.size()));
            scby_assert(result);
            if (!result) {
                break;
            }
        }

        if (result) {
            // Bounds check exemplar assignments
            for (i = 0; i<I.size(); ++i) {
                result &= ((I[i] >= 0) && (I[i] < samples_to_clusters.size()));
                scby_assert(result);
                if (!result) {
                    break;
                }
                // Check exemplar made it to its own cluster
                result &= samples_to_clusters[I[i]] == i;
                scby_assert(result);
                if (!result) {
                    break;
                }
            }
        }

        return result;
    }

    /**
     * Code based on APCluster Rcpp source
     * @param s - a similarity matrix - typically taken to be negative of the
     *  Euclidean squared distance between data points. preferences will be stored
     *  along diagonal and matrix also may be modified by Gaussian noise
     * @param p - vector of preferences for each input sample. higher preferences means sample
     *  is more likely to be found as an exemplar. if no prior preference, science paper recommends
     *  to simply use the median similarity value
     * @param maxits - maximum number of iterations
     * @param convits - the algorithm terminates if the examplars have not changed for convits
     *  iterations
     * @param lam - damping factor; should be a value in the range [0.5, 1); higher values correspond
     *  to heavy damping which may be needed if oscillations occur
     * @param nonoise - adds a small amount of noise to s to prevent degenerate cases
     * @param I - Nx1 column vector - stores index of exemplar for K-th cluster
     * @param A - NxN real matrix for availability messages
     * @param R - NxN real matrix for responsibility messages
     * @pram K - output number of clusters found
     * @return TRUE if converged, FALSE o/w
     */
    bool find_exemplars(scby_matrix &s,
                        const scby_vec_col &p,
                        const size_t maxits,
                        const size_t convits,
                        const scby_real lam,
                        const bool nonoise,
                        scby_index_vec_t &I,
                        scby_matrix &A,
                        scby_matrix &R,
                        size_t &K) {
        // assert symmetric similarity matrix
        scby_assert(s.n_rows == s.n_cols);
        // assert we have at enough elements for clustering
        scby_assert(s.n_elem > 2);
        // assert damping factor is within expected range
        scby_assert(lam >= as_scby_real(0.5) && lam <= as_scby_real(0.9));
        // assert preference vector has expected size
        scby_assert(p.n_elem == s.n_rows);
        // assert we have non-pathological iteration limits
        scby_assert(maxits > 1);
        scby_assert(convits > 1);

        // number of samples
        const size_t N = s.n_rows;

        // in case user did not remove degeneracies from the input similarities,
        // avoid degenerate solutions by adding a small amount of noise to the
        // input similarities

        if (!nonoise)
        {
            // NxN matrix of standard Gaussian noise
            const scby_matrix randomMat(N, N, scby_matrix_utils::fill::randn);
            s += (SCBY_REAL_EPS * s + std::numeric_limits<scby_real>::min() * as_scby_real(100.0)) * randomMat;
        }

        // set preferences along diagonal
        s.diag() = p;

        // I stores index of k-th cluster
        I.zeros(N);

        // availability messages
        A.zeros(N,N);

        // responsibility messages
        R.zeros(N,N);

        scby_matrix_utils::Mat<int> e(N, convits, scby_matrix_utils::fill::zeros);
        scby_matrix_utils::Col<int> se(N, scby_matrix_utils::fill::zeros);
        scby_vec_col Rp(N);

        bool dn = false, unconverged = false;

        // K - stores total number of clusters
        size_t i = 0, j, ii;

        while (!dn) {
            // first, compute responsibilities

            for (ii = 0; ii < N; ++ii) {
                scby_real max1 = -SCBY_REAL_MAX, max2 = -SCBY_REAL_MAX, avsim;
                size_t yMax;

                for (j = 0; j < N; ++j) // determine second-largest element of AS
                {
                    avsim = A.at(ii, j) + s.at(ii, j);

                    if (avsim > max1) {
                        max2 = max1;
                        max1 = avsim;
                        yMax = j;
                    }
                    else if (avsim > max2)
                        max2 = avsim;
                }

                for (j = 0; j < N; ++j) // perform update
                {
                    scby_real oldVal = R(ii, j);
                    scby_real newVal = (as_scby_real(1.0) - lam) * (s(ii, j) - (j == yMax ? max2 : max1)) + lam * oldVal;
                    R.at(ii, j) = (newVal > SCBY_REAL_MAX ? SCBY_REAL_MAX : newVal);
                }
            }

            // secondly, compute availabilities

            for (ii = 0; ii < N; ++ii) {
                scby_real auxsum = 0;

                for (j = 0; j < N; ++j) {
                    if (R.at(j, ii) < 0 && j != ii)
                        Rp[j] = 0;
                    else
                        Rp[j] = R.at(j, ii);

                    auxsum += Rp[j];
                }

                for (j = 0; j < N; ++j) {
                    scby_real oldVal = A.at(j, ii);
                    scby_real newVal = auxsum - Rp[j];

                    if (newVal > 0 && j != ii)
                        newVal = 0;

                    A.at(j, ii) = (as_scby_real(1.0) - lam) * newVal + lam * oldVal;
                }
            }

            // determine exemplars and check for convergence

            unconverged = false;

            K = 0;

            for (ii = 0; ii < N; ++ii) {
                int ex = (A.at(ii, ii) + R.at(ii, ii) > 0 ? 1 : 0);
                se[ii] = se[ii] - e.at(ii, i % convits) + ex;
                if (se[ii] > 0 && se[ii] < convits)
                    unconverged = true;
                e.at(ii, i % convits) = ex;
                // store index of exemplar for current cluster
                if (ex) {
                    I[K] = ii;
                }
                K += ex;
            }

            if (i >= (convits - 1) || i >= (maxits - 1))
                dn = ((!unconverged && K > 0) || (i >= (maxits - 1)));

            ++i;
        }

        return !unconverged;
    }

    /**
     * Utility for refining exemplar and cluster assignments based on similarity
     * Refinement procedure based on post-processing within APCluster R package
     * @param samples_to_clusters - stores cluster assignment for each sample
     *  - output is size n where n is number of samples
     * @param clusters_to_samples - stores sample assignments for each cluster
     * @param exemplars - stores index of exemplar sample for each cluster
     *  - output is size k where k is number of clusters found
     * @param s - a similarity matrix - typically taken to be negative of the
     *  Euclidean squared distance between data points. preferences will be stored
     *  along diagonal and matrix also may be modified by Gaussian noise
     * @param p - vector of preferences for each input sample. higher preferences means sample
     *  is more likely to be found as an exemplar. if no prior preference, science paper recommends
     *  to simply use the median similarity value
     * @param I - stores indices of exemplars (i-th index is sample index for i-th cluster)
     * @param K - the number of clusters/exemplars founds by affinity propagation
     */
    void refine_clusters(scby_index_vec_t &samples_to_clusters,
                         scby_ap_c2s_map_t &clusters_to_samples,
                         scby_index_vec_t &exemplars,
                         scby_matrix &s,
                         const scby_vec_col &p,
                         scby_matrix_utils::Col<scby_matrix_utils::uword> &I,
                         const size_t K) {
        scby_assert(K > 0);
        scby_assert_bounds(K, 0, s.n_rows);

        // Hacky, make sure exemplars are assigned to their own cluster
        // by assigning an "infinite" similarity
        scby_assert(check_s(s));
        s.diag().fill(SCBY_REAL_MAX);

        // Crop initial exemplar indices to number of clusters
        I.resize(K);

        // Extract columns of exemplars from similarity matrix
        scby_matrix exemplar_similarities;
        select_cols(exemplar_similarities, s, I);

        // For each sample, find most similar exemplar
        // This is the initial cluster assignment for each sample
        assign_clusters(samples_to_clusters,
                        clusters_to_samples,
                        exemplar_similarities);
        scby_assert(check_c(samples_to_clusters, I));
        scby_assert(clusters_to_samples.size() == K);

        // reset preferences along diagonal
        s.diag() = p;

        // Refine exemplars within each cluster by finding sample
        // most similar to everything else
        exemplars.set_size(K);
        for (size_t i_clust=0; i_clust<K; ++i_clust) {
            scby_real max_total_sim = -SCBY_REAL_MAX;
            const size_t n_elem = clusters_to_samples[i_clust].size();
            scby_assert(n_elem > 0);
            for (size_t i_elem=0; i_elem<n_elem; ++i_elem) {
                const size_t i_ix = clusters_to_samples[i_clust][i_elem];
                scby_real total_sim = as_scby_real(0.0);
                for (size_t j_elem=0; j_elem<n_elem; ++j_elem) {
                    const size_t j_ix = clusters_to_samples[i_clust][j_elem];
                    total_sim += s.at(i_ix, j_ix);
                    scby_assert(std::isfinite(total_sim));
                }
                if (total_sim > max_total_sim) {
                    max_total_sim = total_sim;
                    exemplars.at(i_clust) = i_ix;
                }
            }
            scby_assert_bounds(exemplars[i_clust], 0, s.n_cols);
        }

        // Reassign clusters based on refined exemplar similarity
        s.diag().fill(SCBY_REAL_MAX);
        select_cols(exemplar_similarities, s, exemplars);
        assign_clusters(samples_to_clusters,
                        clusters_to_samples,
                        exemplar_similarities);
        scby_assert(exemplars.n_elem == clusters_to_samples.size());
    }

} // end of helper namespace

//****************************************************************************
// AP timers
//****************************************************************************

/**
 * Total time accrued
 */
std::clock_t scby_ap_timers::accrued_timers[e_scby_ap_timers_num] = {0};

/**
 * If valid pointer, begins timing
 */
void scby_ap_timers::begin_capture(scby_ap_timers *t, const escby_ap_timers et) {
    scby_assert_bounds(et, 0, e_scby_ap_timers_num);
    if (t != NULL) {
        t->transient_timers[et] = std::clock();
    }
}

/**
 * If valid pointer, ends timing
 */
void scby_ap_timers::end_capture(scby_ap_timers *t, const escby_ap_timers et) {
    scby_assert_bounds(et, 0, e_scby_ap_timers_num);
    if (t != NULL) {
        t->transient_timers[et] = std::clock() - t->transient_timers[et];
        accrued_timers[et] += t->transient_timers[et];
    }
}

/**
 * @return total number of seconds between timer capture calls
 */
scby_real scby_ap_timers::get_timer_secs(const escby_ap_timers et) {
    scby_assert_bounds(et, 0, e_scby_ap_timers_num);
    return accrued_timers[et] / ((scby_real)CLOCKS_PER_SEC);
}

/**
 * outputs times taken by tracked operations
 */
void scby_ap_timers::report() {
    const char * timer_names[e_scby_ap_timers_num] = {
        "ap_timer_pdist",
        "ap_timer_find_exemplars",
        "ap_timer_refine_clusters",
        "ap_timer_all"
    };
    std::cout << "Affinity propagation times:\n";
    for (size_t i=0; i<e_scby_ap_timers_num; ++i) {
        std::cout << "\t" << timer_names[i] << " : " << get_timer_secs((escby_ap_timers)i) << " s\n";
    }
}

//****************************************************************************
// scby_apcluster
//****************************************************************************

// Implementation detail: the noise initialization reference code (matlab, Rcpp, etc)
// expects a full-sized matrix and after initialization s(i,j) is almost certainly not symmetric
// hence, *not* using a memory efficient symmetric matrix (e.g. one which only stores upper tri)

/**
 * @param samples_to_clusters - stores cluster assignment for each sample
 *  - output is size n where n is number of samples
 * @param clusters_to_samples - stores sample assignments for each cluster
 * @param exemplars - stores index of exemplar sample for each cluster
 *  - output is size k where k is number of clusters found
 * @param s - a similarity matrix - typically taken to be negative of the
 *  Euclidean squared distance between data points. preferences will be stored
 *  along diagonal and matrix also may be modified by Gaussian noise
 * @param p - vector of preferences for each input sample. higher preferences means sample
 *  is more likely to be found as an exemplar. if no prior preference, science paper recommends
 *  to simply use the median similarity value
 * @param timers - (optional) used for timing various operations
 * @param maxits - maximum number of iterations
 * @param convits - the algorithm terminates if the exemplars have not changed for convits
 *  iterations
 * @param lam - damping factor; should be a value in the range [0.5, 1); higher values correspond
 *  to heavy damping which may be needed if oscillations occur
 * @param nonoise - if false, adds a small amount of noise to similarities to prevent degenerate cases
 * @return TRUE if converged, FALSE o/w
 */
bool scby_apcluster(scby_index_vec_t &samples_to_clusters,
                    scby_ap_c2s_map_t &clusters_to_samples,
                    scby_index_vec_t &exemplars,
                    scby_matrix &s,
                    const scby_vec_col &p,
                    scby_ap_timers *timers,
                    const size_t maxits,
                    const size_t convits,
                    const scby_real lam,
                    const bool nonoise) {

    scby_index_vec_t I;
    scby_matrix A;
    scby_matrix R;

    size_t K;

    samples_to_clusters.clear();
    clusters_to_samples.clear();
    exemplars.clear();

    // determine exemplars
    scby_ap_timers::begin_capture(timers, e_scby_ap_timer_find_exemplars);
    const bool converged = find_exemplars(
                               s,
                               p,
                               maxits,
                               convits,
                               lam,
                               nonoise,
                               I,
                               A,
                               R,
                               K);
    scby_ap_timers::end_capture(timers, e_scby_ap_timer_find_exemplars);

    // determine cluster assignments
    scby_ap_timers::begin_capture(timers, e_scby_ap_timer_refine_clusters);
    if (K > 0) {
        refine_clusters(samples_to_clusters, clusters_to_samples, exemplars, s, p, I, K);
    }
    scby_ap_timers::end_capture(timers, e_scby_ap_timer_refine_clusters);

    return converged;
}

/**
 * This stock version internally computes default pairwise similarities and preferences
 * based on input positions_mat and q parameters
 * @param samples_to_clusters - stores cluster assignment for each sample
 *  - output is size n where n is number of samples
 * @param clusters_to_samples - stores sample assignments for each cluster
 * @param exemplars - stores index of exemplar sample for each cluster
 *  - output is size k where k is number of clusters found
 * @param positions_mat - matrix of sample coordinates, each sample is assumed to take a single column
 * @param timers - (optional) used for timing various operations
 * @param q - specifies which quantile similarity to assign as the no-prior preference
 * @param maxits - maximum number of iterations
 * @param convits - the algorithm terminates if the exemplars have not changed for convits
 *  iterations
 * @param lam - damping factor; should be a value in the range [0.5, 1); higher values correspond
 *  to heavy damping which may be needed if oscillations occur
 * @param nonoise - if false, adds a small amount of noise to similarities to prevent degenerate cases
 * @return TRUE if converged, FALSE o/w
 */
bool scby_apcluster_pdist2(scby_index_vec_t &samples_to_clusters,
                          scby_ap_c2s_map_t &clusters_to_samples,
                          scby_index_vec_t &exemplars,
                          const scby_matrix &positions_mat,
                          scby_ap_timers *timers,
                          const scby_real q,
                          const size_t maxits,
                          const size_t convits,
                          const scby_real lam,
                          const bool nonoise) {
    scby_assert(positions_mat.n_cols >= 3);

    // compute pairwise similarities as the negative euclidean squared distance
    scby_ap_timers::begin_capture(timers, e_scby_ap_timer_pdist);
    scby_matrix s;
    scby_compute_pdist2(s, positions_mat);
    s *= as_scby_real(-1.0);
    scby_ap_timers::end_capture(timers, e_scby_ap_timer_pdist);

    // initialize preferences based on quantile similarity
    scby_vec_col p;
    scby_apcluster_util_qs_prefs_lower_tri(p, s, q);

    // perform affinity propagation
    return scby_apcluster(samples_to_clusters,
                          clusters_to_samples,
                          exemplars,
                          s,
                          p,
                          timers,
                          maxits,
                          convits,
                          lam,
                          nonoise);
}

/**
 * Utility for determining preferences based on the q'th quantile similarity score
 * present within the lower triangle of the input similarity matrix
 * @param p - output vector of preferences based on the similarity score
 *  at the q-th quantile - each element is set to same value
 * @param s - initialized symmetric pairwise similarity matrix
 * @param q - specifies the quantile of the similarity score in [0, 1.0],
 *  the score corresponding to this quantile is replicated to each element of p
 */
void scby_apcluster_util_qs_prefs_lower_tri(
        scby_vec_col &p,
        const scby_matrix &s,
        const scby_real q) {
    scby_assert(s.n_rows >= 3);
    scby_assert(s.n_rows == s.n_cols);
    scby_assert(s.n_elem == (s.n_rows * s.n_cols));
    scby_assert(q >= as_scby_real(0.0));
    scby_assert(q <= as_scby_real(1.0));

    // determine number of elements in lower triangle
    const scby_uint n_lower_tri_elem = (s.n_elem - s.n_rows) / 2;
    scby_assert((n_lower_tri_elem + n_lower_tri_elem + s.n_rows) == s.n_elem);
    scby_assert(n_lower_tri_elem > 0);

    // linearize lower tri (exclude diagonal)
    scby_uint i_elem=0;
    std::vector<scby_real> lower_tri(n_lower_tri_elem);
    for (scby_uint i_col=0; i_col<s.n_cols; ++i_col) {
        for (scby_uint i_row=(i_col+1); i_row<s.n_rows; ++i_row) {
            scby_assert_bounds(i_elem, 0, lower_tri.size());
            lower_tri[i_elem++] = s.at(i_row, i_col);
        }
    }
    scby_assert(i_elem == lower_tri.size());

    // find element corresponding to q-th quantile
    const scby_uint q_index = as_scby_uint((as_scby_real(n_lower_tri_elem-1) * q));
    scby_assert_bounds(q_index, 0, lower_tri.size());
    std::nth_element(lower_tri.begin(), lower_tri.begin() + q_index, lower_tri.end());
    const scby_real q_sim = lower_tri[q_index];

    // allocate preferences vector
    p.set_size(s.n_rows);

    // replicate q-th quantile similarity to each element of preferences vector
    p.fill(q_sim);
}

/**
 * Utility for determining preferences based on the q'th quantile similarity score
 * based on both the upper and lower tri of the similarity matrix. Note, the
 * diagonal is excluded as the input preferences are to be placed along it.
 * @param p - output vector of preferences based on the similarity score
 *  at the q-th quantile - each element is set to same value
 * @param s - initialized pairwise similarity matrix - square, can be non-symmetric
 * @param q - specifies the quantile of the similarity score in [0, 1.0],
 *  the score corresponding to this quantile is replicated to each element of p
 */
void scby_apcluster_util_qs_prefs_no_diag(
        scby_vec_col &p,
        const scby_matrix &s,
        const scby_real q) {
    scby_assert(s.n_rows >= 3);
    scby_assert(s.n_rows == s.n_cols);
    scby_assert(s.n_elem == (s.n_rows * s.n_cols));
    scby_assert(q >= as_scby_real(0.0));
    scby_assert(q <= as_scby_real(1.0));

    // determine number of elements sans diagonal
    const scby_uint n_elem = s.n_elem - s.n_rows;
    scby_assert(n_elem > 0);

    // linearize s matrix (exclude diagonal)
    scby_uint i_elem=0;
    std::vector<scby_real> s_lin(n_elem);
    for (scby_uint i_col=0; i_col<s.n_cols; ++i_col) {
        for (scby_uint i_row=(i_col+1); i_row<s.n_rows; ++i_row) {
            scby_assert_bounds(i_elem, 0, s_lin.size());
            s_lin[i_elem++] = s.at(i_row, i_col);
            scby_assert_bounds(i_elem, 0, s_lin.size());
            s_lin[i_elem++] = s.at(i_col, i_row);
        }
    }
    scby_assert(i_elem == s_lin.size());

    // find element corresponding to q-th quantile
    const scby_uint q_index = as_scby_uint((as_scby_real(n_elem - 1) * q));
    scby_assert_bounds(q_index, 0, s_lin.size());
    std::nth_element(s_lin.begin(), s_lin.begin() + q_index, s_lin.end());
    const scby_real q_sim = s_lin[q_index];

    // allocate preferences vector
    p.set_size(s.n_rows);

    // replicate q-th quantile similarity to each element of preferences vector
    p.fill(q_sim);
}
