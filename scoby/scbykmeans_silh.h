//****************************************************************************
// scbykmeans_silh.h
//
// Uses method of silhouettes to determine the number of "natural" clusters
// https://en.wikipedia.org/wiki/Silhouette_(clustering)
// Peter J. Rousseeuw (1987). "Silhouettes: a Graphical Aid to the Interpretation and Validation of Cluster Analysis".
// Computational and Applied Mathematics 20: 53–65. doi:10.1016/0377-0427(87)90125-7
//****************************************************************************

#ifndef SCBYKMEANS_SILH_H
#define SCBYKMEANS_SILH_H

//****************************************************************************
// Includes
//****************************************************************************

#include "scbybuild.h"
#include "scbytypes.h"

//****************************************************************************
// Arguments
//****************************************************************************

// Enumerated user arguments for use in argv, argc main
enum escby_kmeans_silh_args {
    escby_kmeans_exe_name=0,
    escby_kmeans_app_name,
    escby_kmeans_silh_min_k,
    escby_kmeans_silh_max_k,
    escby_kmeans_silh_k_inc,
    escby_kmeans_silh_k_report_interv,
    escby_kmeans_silh_n_rand_restarts,
    escby_kmeans_silh_n_dims,
    escby_kmeans_silh_delta,
    escby_kmeans_silh_max_iters_per_run,
    escby_kmeans_silh_in_bin_coords_path,
    escby_kmeans_silh_out_clusters_path,
    escby_kmeans_silh_min_num_args,
    // All arguments below are optional
    escby_kmeans_silh_out_centroids_path = escby_kmeans_silh_min_num_args,
    escby_kmeans_silh_out_cluster_counts_path,
    escby_kmeans_silh_out_silh_scores_path,
    escby_kmeans_silh_out_silh_k_scores_path,
    escby_kmeans_silh_max_num_args
};

// Arguments to silhouette based k-means clusterer
struct scby_kmeans_silh_args_t {
    // the minimum k-cluster value to attempt
    scby_uint min_k;
    // the maximum k-cluster value to attempt (>= min_k)
    scby_uint max_k;
    // the interval between subsequent k-cluster attempts
    scby_uint k_inc;
    // the interval for reporting top cluster status (heartbeat)
    scby_uint k_report_interv;
    // k-means finds local minima sensitive to initial starting centroids.
    // to alleviate this, for a given k, the algorithm can be rerun from
    // different random initial conditions. The best clustering will be kept.
    scby_uint n_rand_restarts;
    // number of consecutive columns defining a single sample
    scby_uint n_dims;
    // for single k-value run, if all centroids change by less than
    // or equal to this amount, then algorithm stops
    scby_real delta;
    // the maximum number of iterations for a single run of k means
    scby_uint max_iters_per_run;
    // path to binary coords matrix
    const char* in_bin_coords_path;
    // path to write cluster assignments for each structure
    const char* out_clusters_path;
    // optional output path of centroids
    const char* out_centroids_path;
    // optional output path of counts for each cluster
    const char* out_cluster_counts_path;
    // optional output path of silh. scores for each sample
    const char* out_silh_scores_path;
    // optional output path to silh. scores for each k-value attempted
    const char* out_silh_k_scores_path;
};

//****************************************************************************
// Extern(s)
//****************************************************************************

// Searches for a cluster count between [min_k and max_k] of the parameter
// data. Uses silhouette method to pick k-value with largest average
// silhouette score.
extern void scby_k_means_silh(const scby_kmeans_silh_args_t &args);

#endif // SCBYKMEANS_SILH_H
